﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;

namespace TasksOfCompany
{
    public class LineNotify
    {
        public int lineNotify(string msg, string token, ref string ErrMsg)
        {
            int Err = 0;
            try
            {
                var request = (HttpWebRequest)WebRequest.Create("https://notify-api.line.me/api/notify");

                var postData = string.Format("message={0}", msg);
                var data = Encoding.UTF8.GetBytes(postData);

                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";
                request.ContentLength = data.Length;
                request.Headers.Add("Authorization", "Bearer " + token);
                
                using (var stream = request.GetRequestStream()) stream.Write(data, 0, data.Length);
                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
                 
            }
            catch (Exception ex)
            {
                ErrMsg = ex.Message;
                ++Err;
            }
            return Err;
        }

        public int LineNotify_GetUser(ref string ErrMsg,string Token)
        {
            int TranErr = 0;

            try
            {
                var User_request = (HttpWebRequest)WebRequest.Create("https://api.line.me/v2/bot/group/{groupId}/members/ids");
                User_request.Headers.Add("Authorization", "Bearer " + Token);
            }
            catch (Exception ex)
            {
                ErrMsg = ex.Message;
            }

            return TranErr;
        }

    }
}
