﻿using ControlCenter;
using DBUtil;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid.Views.BandedGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Calendar.v3;
using Google.Apis.Calendar.v3.Data;
using Google.Apis.Services;
using ModuleUtil;
using PopUpQueryGrid;
using QERPUtill;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace TasksOfCompany
{
    public partial class FormTasksOfCompany : QEB.Form
    {
        public FormTasksOfCompany()
        {
            InitializeComponent();
        }
        #region Calendar
        private string ApplicationName = "Google Calendar API .NET Quickstart";
        #endregion
        #region Valiable
        private Mode ModeSelect = Mode.View;
        private TabSelected ModeTabSelect = TabSelected.TabBug;
        private ModeCopyPress ModeCopyOrNormal = ModeCopyPress.Normal;

        public string LoginInfo = string.Empty;
        public PopMessageLoading PopMessageLoadingObj;
        private DBSimple DBSimpleObj;
        private DBTransaction DBTransObj;
        #endregion
        #region Table
        private DataTable TBTasksOfCompany;
        private DataTable TBEmployeeMaster;
        private DataTable TBCustomerMaster;
        private DataTable TBTasksOfCompanyCopy;
        private DataTable TBCompanyConfigPlan;
        private DataTable TBEmployee;
        #endregion
        private XtraPopUpQueryGrid PopUpCustomerCode;
        public Dictionary<string, string> EmailDic;
        public enum Mode
        {
            View,
            Edit
        }
        public enum TabSelected
        {
            TabTest,
            TabPlaREQ,
            TabBug
        }
        public enum ModeCopyPress
        {
            CopyPress,
            Normal
        }
        private void FormTasksOfCompany_Load(object sender, EventArgs e)
        {
            this.QEBSetup(this.LoginInfo);
            this.DBSimpleObj = new DBSimple(this.QEBConnection);
            this.DBTransObj = new DBTransaction(this.QEBConnection);

            ModuleUtil.ModuleProgram TitleProgram = new ModuleProgram(this.QEBConnectionQEB, this.QEBCenterInfo.Company);
            this.Text = TitleProgram.GetTitleProgram("Tasks Of Company", this.QEBCenterInfo.Server, this.QEBCenterInfo.Company, this.QEBCenterInfo.Username);
            this.Text = this.Text + " [" + ProductVersion + "]";
            this.InitTable();
            this.InitValue();
            this.InitControl();
            this.InitEvent();

            this.LoadData();
            this.CheckEnableControl(Mode.View);
            this.PopMessageLoadingObj.CloseLoadingProgram();
            this.WindowState = FormWindowState.Maximized;
        }
        private void LoadData()
        {
            string ErrMsg = string.Empty;
            int ErrCode = 0;
            int TranErr = 0;

            string Query = @"
SELECT * , RowNumber AS NoItem
FROM TaskOfCompany
{0}";
            string Search = string.Empty;

            DateTime DateSearchTo = DateTime.Now;
            DateTime DateSearchFrom = DateTime.Now;

            string DateSearchFromtxt = string.Empty;
            string DateSearchTotxt = string.Empty;

            #region Date Search
            if (DateTime.TryParse(dateSearchTo.Text, out DateSearchTo))
            {
                DateSearchTotxt = DateSearchTo.Month + "/" + DateSearchTo.Day + "/" + DateSearchTo.Year;
            }
            if (DateTime.TryParse(dateSearchFrom.Text, out DateSearchFrom))
            {
                DateSearchFromtxt = DateSearchFrom.Month + "/" + DateSearchFrom.Day + "/" + DateSearchFrom.Year;
            }

            Search = "WHERE DocumentType NOT IN ('OT')";
            if (this.txtSearch.Text != string.Empty)
            {
                if (lueSerch.EditValue.ToString() == FILED_TasksOfCompany.COL01_RowNumber)
                {
                    int RowNumber = -1;
                    Int32.TryParse(txtSearch.Text, out RowNumber);
                    Search += " AND " + lueSerch.EditValue + @" = " + RowNumber;
                }
                else
                    Search += " AND " + lueSerch.EditValue + @" LIKE N'" + txtSearch.Text + "%'";
            }
            if (this.dateSearchFrom.Text != string.Empty || this.dateSearchTo.Text != string.Empty)
            {
                Search += " AND ";
                if (DateSearchFromtxt == string.Empty && DateSearchTotxt != string.Empty)
                {
                    Search += lueDateSearch.EditValue + " <= '" + DateSearchTotxt + "'";
                }
                else if (DateSearchFromtxt != string.Empty && DateSearchTotxt == string.Empty)
                {
                    Search += lueDateSearch.EditValue + " >= '" + DateSearchFromtxt + "'";
                }
                else if (DateSearchFromtxt != string.Empty && DateSearchTotxt != string.Empty)
                {
                    Search += lueDateSearch.EditValue + " <= '" + DateSearchTotxt + @"' AND 
" + lueDateSearch.EditValue + " >= '" + DateSearchFromtxt + "'";
                }
            }
            #endregion
            string valueStatus = this.SearchStatus_CheckCombobox.EditValue.ToString();
            if (!String.IsNullOrEmpty(valueStatus))
            {
                valueStatus = valueStatus.Replace(", ", "','");
                Search += String.Format(" AND (ISNULL(TransactionStatus, 'N') IN('{0}') OR ISNULL(TestStatus, '') IN('{0}'))", valueStatus);
            }
            Query = string.Format(Query, Search);
            TranErr += DBSimpleObj.FillData(this.TBTasksOfCompany, Query, ref ErrCode, ref ErrMsg);
            

            if (this.TBTasksOfCompany.Rows.Count > 0)
            {
                foreach (DataRow RowTask in this.TBTasksOfCompany.Rows)
                {
                    string caseID = ReceiveValue.StringReceive("RowNumber", RowTask);

                    string DocumentTypeRequiment = ReceiveValue.StringReceive(FILED_TasksOfCompany.COL06_DocumentTypeRequiment, RowTask);
                    string DocumentType = ReceiveValue.StringReceive(FILED_TasksOfCompany.COL05_DocumentType, RowTask);
                    bool CheckAppreovedTest = ReceiveValue.BoolReceive(FILED_TasksOfCompany.COL39_ApprovedTest, RowTask, false);
                    if (DocumentType == VALUE_Status.DocumenttType_Test)
                    {
                        if (CheckAppreovedTest)
                        {
                            RowTask.BeginEdit();
                            RowTask["ColorStatus"] = VALUE_Status.Color_Green;
                            RowTask.EndEdit();
                        }
                    }
                    else
                    {
                        DateTime ToDay = DateTime.Now.Date;
                        DateTime? DueDate = null;
                        if (RowTask[FILED_TasksOfCompany.COL13_DueDate] != DBNull.Value)
                            DueDate = ((DateTime)RowTask[FILED_TasksOfCompany.COL13_DueDate]).Date;

                        DateTime? TestDueDate = null;
                        if (RowTask["TestDueDate"] != DBNull.Value)
                            TestDueDate = ((DateTime)RowTask["TestDueDate"]).Date;

                        string color = VALUE_Status.Color_Black;
                        if (DueDate.HasValue || TestDueDate.HasValue)
                        {
                            string StatusWork = ReceiveValue.StringReceive(FILED_TasksOfCompany.COL15_TransactionStatus, RowTask);
                            string TestStatus = ReceiveValue.StringReceive("TestStatus", RowTask);
                            if (StatusWork == VALUE_Status.TransactionStatus_Finish
                                   && (TestStatus == "N" || TestStatus == "P")
                                   && TestDueDate.HasValue && TestDueDate <= ToDay)
                                color = VALUE_Status.Color_LightPink;
                            else if (StatusWork != VALUE_Status.TransactionStatus_Finish
                              && DueDate.HasValue && DueDate <= ToDay)
                                color = VALUE_Status.Color_Red;
                            else if (StatusWork != VALUE_Status.TransactionStatus_Finish
                             && DueDate.HasValue && DueDate <= ToDay.AddDays(1))
                                color = VALUE_Status.Color_Yellow;
                        }
                        RowTask.BeginEdit();
                        RowTask["ColorStatus"] = color;
                        RowTask.EndEdit();
                    }
                }
            }
            if (TranErr != 0)
            {
                MessageBox.Show(ErrMsg);
            }
        }
        private void InitTable()
        {
            this.QEBds = new DataSet();
            TableDefCreateStore TBStorApp = new TableDefCreateStore();
            TBStorApp.Add(new TableCreatedDef(TableName.TasksOfCompany, new string[] { }, true, false));
            TBStorApp.Add(new TableCreatedDef(TableName.CustomerMaster, new string[] { }, true, false));
            TBStorApp.Add(new TableCreatedDef(TableName.EmployeeMaster, new string[] { }, true, false));
            TBStorApp.Add(new TableCreatedDef(TableName.CompanyConfigPlan, new string[] { }, true, false));
            this.DBSimpleObj.CreateArrayDataTable(this.QEBds, ref TBStorApp);

            this.TBEmployeeMaster = this.QEBds.Tables[TableName.EmployeeMaster];
            this.TBTasksOfCompany = this.QEBds.Tables[TableName.TasksOfCompany];
            this.TBCompanyConfigPlan = this.QEBds.Tables[TableName.CompanyConfigPlan];
            this.TBCustomerMaster = this.QEBds.Tables[TableName.CustomerMaster];
            this.TBTasksOfCompanyCopy = this.TBTasksOfCompany.Clone();

            this.TBTasksOfCompany.Columns.Add("StatusRow", typeof(String));
            this.TBTasksOfCompany.Columns.Add("ColorStatus", typeof(String));
            this.TBTasksOfCompany.Columns.Add("BackColorStatus", typeof(String));
            this.TBTasksOfCompany.Columns.Add("StatusUpdate", typeof(String));
            this.TBTasksOfCompany.Columns.Add("NoItem", typeof(String));

            this.TBEmployee = new DataTable();
            this.TBEmployee.Columns.Add("Code");
            this.TBEmployee.Columns.Add("NickName");
            this.TBEmployee.Columns.Add("NameThai");
            this.TBEmployee.Columns.Add("Title");
            this.TBEmployee.Columns.Add("RecStatus", typeof(int));
        }
        private void InitEvent()
        {
            this.btnSearch.KeyDown += new KeyEventHandler(EnterKeyDown);
            this.txtSearch.KeyDown += new KeyEventHandler(EnterKeyDown);
            this.dateSearchFrom.KeyDown += new KeyEventHandler(EnterKeyDown);
            this.dateSearchTo.KeyDown += new KeyEventHandler(EnterKeyDown);

            this.btnSearch.Click += new EventHandler(btnSearch_Click);

            this.StripMenuAdd.Click += new EventHandler(StripMenuAdd_Click);
            this.StripMenuCancel.Click += new EventHandler(StripMenuCancel_Click);
            this.StripMenuReCancel.Click += new EventHandler(StripMenuReCancel_Click);

            this.btnbarEdit.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(Edit_Event);
            this.barEdit.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(Edit_Event);
            this.barSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(Save_Event);
            this.BarUndo.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(Undo_ItemClick);
            this.barPlan.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(barPlan_ItemClick);
            this.barLineNotify.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(barLineNotify_ItemClick);
            this.barMail.ItemClick += BarMail_ItemClick;
            this.barExport.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(barExport_ItemClick);
            this.StripMenuCopy.Click += StripMenuCopy_Click;
            this.barConfigPlan.ItemClick += BarConfigPlan_ItemClick;


            this.TBTasksOfCompany.ColumnChanged += new DataColumnChangeEventHandler(TBTasksOfCompany_ColumnChanged);
            this.advBandedGridTaskOfCompany.ShowingEditor += new CancelEventHandler(advBandedGridTaskOfCompany_ShowingEditor);
            this.advBandedGridPlanREQ.ShowingEditor += new CancelEventHandler(advBandedGridTaskOfCompany_ShowingEditor);
            this.advBandedGridTest.ShowingEditor += new CancelEventHandler(advBandedGridTaskOfCompany_ShowingEditor);
            this.advBandedGridTaskOfCompany.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(advBandedGridTaskOfCompany_RowCellStyle);
            this.advBandedGridPlanREQ.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(advBandedGridTaskOfCompany_RowCellStyle);
            this.advBandedGridTest.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(advBandedGridTaskOfCompany_RowCellStyle);

            this.repositoryCustomerCode.ButtonPressed += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(repositoryCustomerCode_ButtonPressed);
            this.repositoryItemTabTestCustomerCode.ButtonPressed += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(repositoryCustomerCode_ButtonPressed);
            this.repositoryItemTabPlanREQCustomerCode.ButtonPressed += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(repositoryCustomerCode_ButtonPressed);
            this.repository_DocumentType.EditValueChanged += new EventHandler(repository_DocumentType_EditValueChanged);

            this.advBandedGridTaskOfCompany.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(advBandedGrid_FocusedRowChanged);
            this.advBandedGridPlanREQ.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(advBandedGrid_FocusedRowChanged);
            this.advBandedGridTest.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(advBandedGrid_FocusedRowChanged);
            this.TabControl.SelectedPageChanged += TabControl_SelectedPageChanged;

            this.advBandedGridTest.CustomRowFilter += advBandedGridTaskOfCompany_CustomRowFilter;
            this.advBandedGridPlanREQ.CustomRowFilter += advBandedGridTaskOfCompany_CustomRowFilter;
            this.advBandedGridTaskOfCompany.CustomRowFilter += advBandedGridTaskOfCompany_CustomRowFilter;

            this.advBandedGridTaskOfCompany.CustomRowCellEditForEditing += CustomRowCellEditForEditing;
            this.advBandedGridPlanREQ.CustomRowCellEditForEditing += CustomRowCellEditForEditing;
            this.advBandedGridTest.CustomRowCellEditForEditing += CustomRowCellEditForEditing;
        }
        private void InitValue()
        {
            #region Add Email ]
            this.EmailDic = new Dictionary<string, string>();
            this.EmailDic.Add("กิ่ง", "thitika@quadtech.co.th");
            this.EmailDic.Add("แบงค์", "thodsaporn@quadtech.co.th");
            this.EmailDic.Add("เดือน", "pawadee@quadtech.co.th");
            this.EmailDic.Add("เจน", "sirinart@quadtech.co.th");
            this.EmailDic.Add("ไก่", "natasak@quadtech.co.th");
            this.EmailDic.Add("นุท", "chompoonoot@quadtech.co.th");
            this.EmailDic.Add("เบล", "benchawan@quadtech.co.th");
            this.EmailDic.Add("โจ้", "ekawut@quadtech.co.th");
            this.EmailDic.Add("ตูน", "narisara@quadtech.co.th");
            this.EmailDic.Add("ปิง", "kongsit@quadtech.co.th");
            this.EmailDic.Add("หนุ่ย", "kampon@quadtech.co.th");
            this.EmailDic.Add("ออย", "surachest@quadtech.co.th");
            this.EmailDic.Add("เตอร์", "sirin@quadtech.co.th");
            this.EmailDic.Add("บิ้ว", "natchapat@quadtech.co.th");
            this.EmailDic.Add("บุญ", "boonsuk@quadtech.co.th");
            this.EmailDic.Add("แพร", "phatchareeporn@quadtech.co.th");
            this.EmailDic.Add("เคน", "athid@quadtech.co.th");
            this.EmailDic.Add("ปุ่น", "chiratkit@quadtech.co.th");
            #endregion
        }
        private void InitControl()
        {
            #region Columns DocumentType ประเภทรายการ
            string[,] ArrayDocumentType = new string[,]
            {
              {VALUE_Status.DocumenttType_Note,"Note"},
              {VALUE_Status.DocumenttType_Bug,"Bug"},
              {VALUE_Status.DocumenttType_ToTalk,"To Talk"},
            };
            DataView ViewDocumentType = this.GetDataView(ArrayDocumentType);
            this.SetupColLookUpEdit(ViewDocumentType, "Code", "Desc", this.repository_DocumentType);
            #endregion

            #region DocumentTypePlanREQ
            string[,] ArrayDocumentTypePlanREQ = new string[,]
            {
              {VALUE_Status.DocumenttType_Requiment,"Requirment"},
              {VALUE_Status.DocumenttType_Plan,"Plan"},
            };
            DataView ViewArrayDocumentTypePlanREQ = this.GetDataView(ArrayDocumentTypePlanREQ);
            this.SetupColLookUpEdit(ViewArrayDocumentTypePlanREQ, "Code", "Desc", this.repositoryItemTabPlanREQDocumentType);
            //repositoryItemTabPlanREQDocumentType
            #endregion

            #region Columns DocumentTypeRequiment ปรเภท Requiment
            string[,] ArrayDocumentTypeRequiment = new string[,]
            {
              {VALUE_Status.DocumentTypeRequiment_Form,"Form"},
              {VALUE_Status.DocumentTypeRequiment_Report,"Report"},
              {VALUE_Status.DocumentTypeRequiment_Program,"Program"},
              {VALUE_Status.DocumentTypeRequiment_Config,"Config/File,FormatNo"},
              {VALUE_Status.DocumentTypeRequiment_Template,"Template"},
              {VALUE_Status.DocumentTypeRequiment_Trainning,"Tranning"},
              {VALUE_Status.DocumentTypeRequiment_OpenHouse,"OpenHouse"},
              {VALUE_Status.DocumentTypeRequiment_Setup,"Setup"}
            };
            DataView ViewDocumentTypeRequiment = this.GetDataView(ArrayDocumentTypeRequiment);
            this.SetupColLookUpEdit(ViewDocumentTypeRequiment, "Code", "Desc", this.repositoryDocumentTypeRequiment);
            this.SetupColLookUpEdit(ViewDocumentTypeRequiment, "Code", "Desc", this.repositoryItemTabPlanREQDocumentTypeRequiment);
            #endregion

            #region TypeSupervisor แผนกผู้รับผิดชอบ
            string[,] ArrayTypeSupervisor = new string[,]
            {
                {"P","Programer"},
                {"S","Support"},
                {"SM","Sales"},
                {"GM","General Manager"},
            };
            DataView ViewTypeSupervisor = this.GetDataView(ArrayTypeSupervisor);
            this.SetupColLookUpEdit(ViewTypeSupervisor, "Code", "Desc", this.repository_TypeSupervisor);
            this.SetupColLookUpEdit(ViewTypeSupervisor, "Code", "Desc", this.repositoryTabPlanREQTypeSupervisor);
            #endregion

            #region TransactionStatus
            string[,] ArrayTransactionStatus = new string[,]
            {
                {VALUE_Status.TransactionStatus_Finish,"เสร็จ"},
                {"FWT","เสร็จ(ไม่ต้องทดสอบ)"},
                {VALUE_Status.TransactionStatus_Processing,"กำลังดำเนินการ"},
                {VALUE_Status.TransactionStatus_Wait,"รอลูกค้า"},
                {VALUE_Status.TransactionStatus_Non,"ยัง"},
            };
            DataView ViewTransactionStatus = this.GetDataView(ArrayTransactionStatus);
            this.SetupColLookUpEdit(ViewTransactionStatus, "Code", "Desc", this.repository_TransactionStatus);
            this.SetupColLookUpEdit(ViewTransactionStatus, "Code", "Desc", this.repositoryItemTabPlanREQTransactionStatus);

            string[,] ArrayTransactionStatusTest = new string[,]
            {
                {"",""},
                {VALUE_Status.TransactionStatus_Finish,"ผ่าน"},
                {"R","ไม่ผ่าน"},
                {VALUE_Status.TransactionStatus_Processing,"กำลังดำเนินการ"},
                {VALUE_Status.TransactionStatus_Non,"ยัง"},           
            };
            DataView ViewTransactionStatusTest = this.GetDataView(ArrayTransactionStatusTest);
            this.SetupColLookUpEdit(ViewTransactionStatusTest, "Code", "Desc", this.repositoryItemTestStatus);
            #endregion

            #region Search Control
            string[,] ArraySearch = new string[,]
            {
              {FILED_TasksOfCompany.COL01_RowNumber,"Case ID"},
              {FILED_TasksOfCompany.COL03_CustomerCode,"รหัสลูกค้า"},
              {FILED_TasksOfCompany.COL04_CustomerName,"ชื่อลุกค้า"},
              {FILED_TasksOfCompany.COL05_DocumentType,"ประเภทเอกสาร"},
              {FILED_TasksOfCompany.COL06_DocumentTypeRequiment,"ประเภท Requiment"},
              {FILED_TasksOfCompany.COL08_ProgramName,"โปรแกรม"},
              {FILED_TasksOfCompany.COL11_TypeSupervisor,"ประเภทผู้รับผิดชอบ"},
            };
            DataView ViewSearch = this.GetDataView(ArraySearch);
            this.SetupLookupEdit(ViewSearch, "Code", "Desc", this.lueSerch);
            this.lueSerch.EditValue = FILED_TasksOfCompany.COL01_RowNumber;

            string[,] ArrayDateSearch = new string[,]
            {
                {FILED_TasksOfCompany.COL02_TransactionDate,"วันที่"},
                {FILED_TasksOfCompany.COL13_DueDate,"วันที่กำหนดส่ง"},
                {FILED_TasksOfCompany.COL14_DueDateTrue,"วันที่ส่งจริง"},
            };
            DataView ViewDateSearch = this.GetDataView(ArrayDateSearch);
            this.SetupLookupEdit(ViewDateSearch, "Code", "Desc", this.lueDateSearch);
            this.lueDateSearch.EditValue = FILED_TasksOfCompany.COL02_TransactionDate;

            #endregion

            #region TabSupport
            DataView ViewCustomerTabSupport = new DataView(this.TBTasksOfCompany
                , String.Format("{0} = '{1}' OR {0} = '{2}' OR {0} = '{3}'"
                    , FILED_TasksOfCompany.COL05_DocumentType
                    , VALUE_Status.DocumenttType_Bug
                    , VALUE_Status.DocumenttType_Note
                    , VALUE_Status.DocumenttType_ToTalk)
                , "RowNumber ASC,TransactionDate ASC", DataViewRowState.CurrentRows);
            this.gridControlBackLog.DataSource = ViewCustomerTabSupport;
            #endregion

            #region TabPlan
            DataView ViewCustomerTabPlanREQ = new DataView(this.TBTasksOfCompany
                    , String.Format("{0} = '{1}' OR {0} = '{2}'"
                    , FILED_TasksOfCompany.COL05_DocumentType
                    , VALUE_Status.DocumenttType_Plan
                    , VALUE_Status.DocumenttType_Requiment)
                , "TransactionDate ASC", DataViewRowState.CurrentRows);
            this.gridControlPlanREQ.DataSource = ViewCustomerTabPlanREQ;
            #endregion

            #region TabTest
            DataView ViewCustomerTabTest = new DataView(this.TBTasksOfCompany
                    , String.Format("{0} = '{1}'"
                    , FILED_TasksOfCompany.COL05_DocumentType
                    , VALUE_Status.DocumenttType_Test)
                , "TransactionDate ASC", DataViewRowState.CurrentRows);
            this.gridControlTest.DataSource = ViewCustomerTabTest;

            string[,] ArrayTabTest_StatusTest = new string[,]
            {
                    {"Y","เสร็จ"},
                    {"N","ยัง"},
            };
            DataView ViewArrayTabTest_StatusTest = this.GetDataView(ArrayTabTest_StatusTest);
            this.SetupColLookUpEdit(ViewArrayTabTest_StatusTest, "Code", "Desc", this.repositoryItemStatusTest);
            string[,] ArrayTabTest_SupportTest = new string[,]
            {
                    {"Y","วาง"},
                    {"N","ไม่วาง"},
            };
            DataView ViewArrayTabTest_SupportTest = this.GetDataView(ArrayTabTest_SupportTest);
            this.SetupColLookUpEdit(ViewArrayTabTest_SupportTest, "Code", "Desc", this.repositoryItemTabTestSupportTest);
            #endregion

            #region PopUpCustomerCode
            this.PopUpCustomerCode = new XtraPopUpQueryGrid(this.QEBCenterInfo);
            this.PopUpCustomerCode.LoadPopUpFm(false, "ค้นหาลูกค้า", new XtraGridHeader.ColumnDefForXtraGridHeader[]
            {
                new XtraGridHeader.ColumnDefForXtraGridHeader(FILED_CustomerMaster.Code,"รหัส",75),
                new XtraGridHeader.ColumnDefForXtraGridHeader(FILED_CustomerMaster.NameThai,"คำอธิบาย(ไทย)",120),
                new XtraGridHeader.ColumnDefForXtraGridHeader(FILED_CustomerMaster.NickName,"ชื่อเล่น",120)
            }, this.TBCustomerMaster, FILED_CustomerMaster.Code, FILED_CustomerMaster.Code);
            this.PopUpCustomerCode.EnablePopUp(true, string.Empty);
            this.PopUpCustomerCode.SelectPopUpFm(this.TBCustomerMaster.TableName);
            #endregion

            this.contextMenuStripGrit.Opening += new CancelEventHandler(contextMenuStripGrit_Opening);
            this.TabControl.SelectedTabPage = this.TabBacklog;

            this.repositoryCustomerCode.MaxLength = 20;

            string QueryEmployee =
@"SELECT EM.Code, EM.Categories1 AS NickName, EM.NameThai , EM.Categories2 AS Title , EM.RecStatus
FROM EmployeeMaster EM
WHERE ISNULL(EM.Categories1,'') <> ''";
            this.DBSimpleObj.FillData(this.TBEmployee, QueryEmployee);
            this.TBEmployee.Rows.Add("Support", "Support", "Support", "S", 0);
            this.TBEmployee.Rows.Add("Support-Test", "Support-Test", "Support-Test", "S", 0);
            this.TBEmployee.Rows.Add("Support-Template", "Support-Template", "Support - Template", "S", 0);
            this.TBEmployee.Rows.Add("Sales", "Sales", "Sales", "SM", 0);
            this.repositoryItemEmployee_TabTest.DataSource = new DataView(this.TBEmployee, "", "NickName ASC", DataViewRowState.CurrentRows);
            this.repositoryItemEmployee_TabTest.DisplayMember = "NickName";
            this.repositoryItemEmployee_TabTest.ValueMember = "Code";

            this.repositoryItemEmployee.DataSource = new DataView(this.TBEmployee, "", "NickName ASC", DataViewRowState.CurrentRows);
            this.repositoryItemEmployee.DisplayMember = "NickName";
            this.repositoryItemEmployee.ValueMember = "Code";

            this.repositoryItemEmployee_Plan.DataSource = new DataView(this.TBEmployee, "", "NickName ASC", DataViewRowState.CurrentRows);
            this.repositoryItemEmployee_Plan.DisplayMember = "NickName";
            this.repositoryItemEmployee_Plan.ValueMember = "Code";

            DataTable TBWorkStatus = new DataTable();
            TBWorkStatus.Columns.Add("Code");
            TBWorkStatus.Columns.Add("Desc");
            TBWorkStatus.Rows.Add("N", "ยังไม่เสร็จ");
            TBWorkStatus.Rows.Add("W", "รอลูกค้า");
            TBWorkStatus.Rows.Add("P", "กำลังดำเนินการ");
            TBWorkStatus.Rows.Add("F", "เสร็จ");

            this.SearchStatus_CheckCombobox.Properties.DataSource = TBWorkStatus;
            this.SearchStatus_CheckCombobox.Properties.DisplayMember = "Desc";
            this.SearchStatus_CheckCombobox.Properties.ValueMember = "Code";
            this.SearchStatus_CheckCombobox.SetEditValue("N, W, P");
        }
        private void advBandedGrid_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            DataRow RowFocus = null;
            if (ModeTabSelect == TabSelected.TabBug)
                RowFocus = this.advBandedGridTaskOfCompany.GetDataRow(e.FocusedRowHandle);
            if (ModeTabSelect == TabSelected.TabPlaREQ)
                RowFocus = this.advBandedGridPlanREQ.GetDataRow(e.FocusedRowHandle);
            if (ModeTabSelect == TabSelected.TabTest)
                RowFocus = this.advBandedGridTest.GetDataRow(e.FocusedRowHandle);

            if (RowFocus == null)
                return;
            string DocumentType = ReceiveValue.StringReceive(FILED_TasksOfCompany.COL05_DocumentType, RowFocus);
            string CustomerCode = ReceiveValue.StringReceive(FILED_TasksOfCompany.COL03_CustomerCode, RowFocus);
            if (ModeSelect == Mode.Edit)
            {
                if (CustomerCode != string.Empty)
                {
                    if (DocumentType == VALUE_Status.DocumenttType_Plan)
                    {
                        this.CheckEnableButtonPlan(true);
                    }
                    else
                    {
                        this.CheckEnableButtonPlan(false);
                    }
                }
                else
                {
                    this.CheckEnableButtonPlan(false);
                }
            }
        }
        private void repository_DocumentType_EditValueChanged(object sender, EventArgs e)
        {
            DevExpress.XtraEditors.LookUpEdit DocumentType = sender as DevExpress.XtraEditors.LookUpEdit;
            if (DocumentType == null)
                return;
            DataRow RowTask = null;
            if (ModeTabSelect == TabSelected.TabBug)
                RowTask = this.advBandedGridTaskOfCompany.GetFocusedDataRow();
            if (ModeTabSelect == TabSelected.TabPlaREQ)
                RowTask = this.advBandedGridPlanREQ.GetFocusedDataRow();
            if (ModeTabSelect == TabSelected.TabTest)
                RowTask = this.advBandedGridTest.GetFocusedDataRow();

            string CustomerCode = ReceiveValue.StringReceive(FILED_TasksOfCompany.COL03_CustomerCode, RowTask);

            if (ModeSelect == Mode.Edit)
            {
                if (CustomerCode != string.Empty)
                {
                    if (DocumentType.EditValue.ToString() == VALUE_Status.DocumenttType_Plan)
                    {

                        this.CheckEnableButtonPlan(true);
                    }
                    else
                    {
                        this.CheckEnableButtonPlan(false);
                    }
                }
                else
                {
                    this.CheckEnableButtonPlan(false);
                }

            }
        }
        private void SetupColLookUpEdit(DataView view, string Code, string Desc, RepositoryItemLookUpEdit ctr)
        {
            ctr.DataSource = view;
            ctr.ValueMember = Code;
            ctr.DisplayMember = Desc;
            ctr.NullText = string.Empty;
            ctr.ShowHeader = false;
            ctr.ShowFooter = false;
            ctr.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFit;
            ctr.PopupFormMinSize = new System.Drawing.Size(10, 10);
            ctr.DropDownRows = view.Count > 10 ? 10 : view.Count;
        }
        private void SetupLookupEdit(DataView view, string Code, string Desc, DevExpress.XtraEditors.LookUpEdit ctr)
        {
            ctr.Properties.DataSource = view;
            ctr.Properties.ValueMember = Code;
            ctr.Properties.DisplayMember = Desc;
            ctr.Properties.NullText = string.Empty;
            ctr.Properties.ShowHeader = false;
            ctr.Properties.ShowFooter = false;
            ctr.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFit;
            ctr.Properties.PopupFormMinSize = new System.Drawing.Size(10, 10);
            ctr.Properties.DropDownRows = view.Count > 10 ? 10 : view.Count;
        }
        public DataView GetDataView(string[,] Str)
        {
            DataTable TB = new DataTable();
            TB.Columns.Add("Code", System.Type.GetType("System.String"));
            TB.Columns.Add("Desc", System.Type.GetType("System.String"));
            TB.Columns.Add("NoLine", System.Type.GetType("System.Int32"));
            for (int i = 0; i < Str.GetLength(0); i++)
            {
                DataRow dr = TB.NewRow();
                dr["NoLine"] = i + 1;
                dr["Code"] = Str[i, 0];
                dr["Desc"] = Str[i, 1];
                TB.Rows.Add(dr);
            }
            return new DataView(TB, "", "NoLine ASC", DataViewRowState.CurrentRows);
        }
        private void advBandedGridTaskOfCompany_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            if (this.TBTasksOfCompany.Rows.Count > 0)
            {
                DataRow RowFocus = null;
                if (ModeTabSelect == TabSelected.TabBug)
                    RowFocus = this.advBandedGridTaskOfCompany.GetDataRow(e.RowHandle);
                if (ModeTabSelect == TabSelected.TabPlaREQ)
                    RowFocus = this.advBandedGridPlanREQ.GetDataRow(e.RowHandle);
                if (ModeTabSelect == TabSelected.TabTest)
                    RowFocus = this.advBandedGridTest.GetDataRow(e.RowHandle);
                if (RowFocus != null)
                {
                    string StatusColor = ModuleUtil.ReceiveValue.StringReceive("ColorStatus", RowFocus);
                    if (StatusColor == VALUE_Status.Color_Red)
                    {
                        e.Appearance.BackColor = Color.FromArgb(204, 102, 102);
                        e.Appearance.ForeColor = Color.White;
                    }
                    else if (StatusColor == VALUE_Status.Color_Delete)
                    {
                        e.Appearance.ForeColor = Color.Red;
                    }
                    else if (StatusColor == VALUE_Status.Color_Yellow)
                    {
                        e.Appearance.BackColor = Color.FromArgb(255, 255, 204);
                        e.Appearance.ForeColor = Color.Black;
                    }
                    else if (StatusColor == VALUE_Status.Color_Green)
                    {
                        e.Appearance.BackColor = Color.FromArgb(179, 255, 204);
                    }
                    else if (StatusColor == VALUE_Status.Color_Blue)
                    {
                        e.Appearance.BackColor = Color.LightSkyBlue;
                    }
                    else if (StatusColor == VALUE_Status.Color_LightPink)
                    {
                        e.Appearance.BackColor = Color.LightPink;
                        e.Appearance.ForeColor = Color.Black;
                    }
                    else if (StatusColor == VALUE_Status.Color_Black)
                    {
                        e.Appearance.ForeColor = Color.Black;
                    }
                }

            }
        }
        private void repositoryCustomerCode_ButtonPressed(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            DevExpress.XtraEditors.ButtonEdit btnCode = sender as DevExpress.XtraEditors.ButtonEdit;
            if (btnCode == null)
                return;
            this.PopUpCustomerCode.ShowForm();
            if (this.PopUpCustomerCode.StrReturn != string.Empty)
                btnCode.EditValue = this.PopUpCustomerCode.StrReturn;
            this.advBandedGridTaskOfCompany.FocusedColumn = this.Col_CustomerName;
        }
        private void advBandedGridTaskOfCompany_ShowingEditor(object sender, CancelEventArgs e)
        {
            string FocusColumns = string.Empty;
            if (ModeTabSelect == TabSelected.TabBug)
                FocusColumns = this.advBandedGridTaskOfCompany.FocusedColumn.FieldName;
            if (ModeTabSelect == TabSelected.TabPlaREQ)
                FocusColumns = this.advBandedGridPlanREQ.FocusedColumn.FieldName;
            if (ModeTabSelect == TabSelected.TabTest)
                FocusColumns = this.advBandedGridTest.FocusedColumn.FieldName;

            if (FocusColumns == string.Empty)
                return;
            if (ModeSelect == Mode.View)
            {
                e.Cancel = true;
                if (FocusColumns == FILED_TasksOfCompany.COL09_TransactionDescription)
                {
                    e.Cancel = false;
                    this.repositoryTransactionDescription.ReadOnly = true;
                    this.repositoryItemTabPlanREQTransactionDescription.ReadOnly = true;
                    this.repositoryItemTransactionDescriptionTest.ReadOnly = true;
                }
                if (FocusColumns == FILED_TasksOfCompany.COL16_TransactionConClude)
                {
                    e.Cancel = false;
                    this.repositoryTransactionConClude.ReadOnly = true;
                    this.repositoryItemTabPlanREQTransactionConClude.ReadOnly = true;
                    this.repositoryItemTestTransactionConClude.ReadOnly = true;
                }
                if (FocusColumns == FILED_TasksOfCompany.COL35_OpenFileVersion)
                {
                    e.Cancel = false;
                    this.repositoryItemTabTestOpenFileVersion.ReadOnly = true;
                }
            }
            else
            {
                e.Cancel = false;
                if (FocusColumns == FILED_TasksOfCompany.COL09_TransactionDescription)
                {
                    e.Cancel = false;
                    this.repositoryTransactionDescription.ReadOnly = false;
                    this.repositoryItemTabPlanREQTransactionDescription.ReadOnly = false;
                    this.repositoryItemTransactionDescriptionTest.ReadOnly = false;
                }
                if (FocusColumns == FILED_TasksOfCompany.COL16_TransactionConClude)
                {
                    e.Cancel = false;
                    this.repositoryTransactionConClude.ReadOnly = false;
                    this.repositoryItemTabPlanREQTransactionConClude.ReadOnly = false;
                    this.repositoryItemTestTransactionConClude.ReadOnly = false;
                }
                if (FocusColumns == FILED_TasksOfCompany.COL22_DueDateBigin)
                {
                    e.Cancel = true;
                }
                if (FocusColumns == FILED_TasksOfCompany.COL35_OpenFileVersion)
                {
                    e.Cancel = false;
                    this.repositoryItemTabTestOpenFileVersion.ReadOnly = false;
                }
            }
        }
        private void btnSearch_Click(object sender, EventArgs e)
        {
            this.LoadData();
        }
        private void Undo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DialogResult ShowChoais = MessageBox.Show("คุณต้องการยกเลิกแก้ไข ?", "Save", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            if (ShowChoais == DialogResult.Yes)
            {
                this.ModeSelect = Mode.View;
                this.CheckEnableControl(Mode.View);
                this.LoadData();
            }
        }
        private void Edit_Event(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.LoadData();
            this.ModeSelect = Mode.Edit;
            this.CheckEnableControl(Mode.Edit);
        }
        private void Save_Event(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DialogResult ShowChoais = MessageBox.Show("คุณต้องการบันทึกรายการหรือไม่?", "Save", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            if (ShowChoais == DialogResult.Yes)
            {
                this.panelControl1.Focus();
                string ErrMsg = string.Empty;
                string ErrLog = string.Empty;
                int ErrCode = 0;
                int TranErr = 0;

                //DataTable TBTasksOfCompanyBackUp = TBTasksOfCompany.Clone();
                //foreach (DataRow RowBackUp in this.TBTasksOfCompany.Rows)
                //    TBTasksOfCompanyBackUp.ImportRow(RowBackUp);

                DataTable TBTasksOfCompanyUpdate = TBTasksOfCompany.Clone();
                if (TranErr == 0)
                {
                    #region SetAdd Or Edit
                    foreach (DataRow RowTask in this.TBTasksOfCompany.AsEnumerable().Where(x => !String.IsNullOrEmpty(x.Field<String>("StatusRow"))).ToArray())
                    {
                        string StatusRow = ReceiveValue.StringReceive("StatusRow", RowTask);
                        string StatusUpdate = ReceiveValue.StringReceive("StatusUpdate", RowTask);
                        bool Approved = ReceiveValue.BoolReceive(FILED_TasksOfCompany.COL39_ApprovedTest, RowTask, false);
                        string DocumentType = ReceiveValue.StringReceive(FILED_TasksOfCompany.COL05_DocumentType, RowTask);
                        string StatusTest = ReceiveValue.StringReceive(FILED_TasksOfCompany.COL40_StatusTest, RowTask);
                        if (StatusRow != string.Empty)
                        {
                            if (StatusRow == "N")
                            {
                                RowTask.BeginEdit();
                                RowTask[FILED_TasksOfCompany.COL22_DueDateBigin] = RowTask[FILED_TasksOfCompany.COL13_DueDate];
                                RowTask.EndEdit();
                                
                            }
                            if (StatusUpdate == "E")
                            {
                                RowTask.BeginEdit();
                                RowTask[FILED_TasksOfCompany.COL19_LastUpdated] = DateTime.Now.Date;
                                RowTask[FILED_TasksOfCompany.COL20_UpdatedUser] = this.QEBCenterInfo.Username;
                                RowTask.EndEdit();
                            }
                            TBTasksOfCompanyUpdate.ImportRow(RowTask);
                        }
                        if (DocumentType == VALUE_Status.DocumenttType_Test)
                        {
                            if (StatusTest == string.Empty)
                            {
                                if (!Approved)
                                {
                                    RowTask.BeginEdit();
                                    RowTask[FILED_TasksOfCompany.COL40_StatusTest] = "N";
                                    RowTask.EndEdit();
                                }
                                else
                                {
                                    RowTask.BeginEdit();
                                    RowTask[FILED_TasksOfCompany.COL40_StatusTest] = "Y";
                                    RowTask.EndEdit();
                                }
                            }
                            else
                            {
                                if (Approved && StatusTest == "N")
                                {
                                    RowTask.BeginEdit();
                                    RowTask[FILED_TasksOfCompany.COL40_StatusTest] = "Y";
                                    RowTask.EndEdit();
                                }
                            }
                        }
                    }
                    #endregion
                    //UpdateCalendarRow();
                    #region Set Delete
                    //foreach (DataRow RowTask in this.TBTasksOfCompany.Rows)
                    //{
                    //    string StatusRow = ReceiveValue.StringReceive("StatusRow", RowTask);
                    //    if (StatusRow == VALUE_Status.DELETE)
                    //    {
                    //        RowTask.Delete();
                    //    }
                    //}
                    #endregion
                    if (TranErr == 0)
                    {
                        foreach (DataRow RowTaskUpdate in TBTasksOfCompanyUpdate.Select())
                        {
                            int StatusErr = 0;
                            string StatusRow = ReceiveValue.StringReceive("StatusRow", RowTaskUpdate);
                            #region Set Delete
                            if (StatusRow == VALUE_Status.DELETE)
                            {
                                RowTaskUpdate.Delete();
                            }
                            #endregion
                            StatusErr += this.DBTransObj.StartTransaction(ref ErrMsg);
                            if (StatusErr != 0)
                                ErrMsg += "DBTransObj StartTransaction Error";
                            if (StatusErr == 0)
                            {
                                DataTable TBRowUpdate = TBTasksOfCompanyUpdate.Clone();
                                TBRowUpdate.ImportRow(RowTaskUpdate);
                                StatusErr += this.DBTransObj.OleDBUpdate(TBRowUpdate, ref ErrCode, ref ErrMsg);
                                if (StatusErr != 0)
                                {
                                    this.DBTransObj.TransRollBack();
                                    ErrMsg += String.Format(@"รายการที่ {0} เรื่อง {1} ไม่สามารถบันทึกได้เนื่องจากมีคนบันทึกไปแล้ว"
                                         , ReceiveValue.StringReceive(FILED_TasksOfCompany.COL03_CustomerCode, RowTaskUpdate)
                                         , ReceiveValue.StringReceive(FILED_TasksOfCompany.COL09_TransactionDescription, RowTaskUpdate)) + @"
";
                                    this.UpdateLogFileError(ErrMsg);
                                }
                                else
                                {
                                    this.DBTransObj.TransCommit();
                                }
                            }
                        }
                    }
                    if (TranErr == 0)
                    {
                        if (ErrMsg != string.Empty)
                        {
                            MessageBox.Show(ErrMsg);
                        }
                        MessageBox.Show("บันทึกเรียบร้อย", "Finish", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        this.ModeSelect = Mode.View;
                        this.CheckEnableControl(Mode.View);
                        this.LoadData();

                        //initToCalendar();
                        //NotifyCalendarUnfinish();
                    }
                }
                //else
                //{
                //    //this.BackUpDataTable();
                //    MessageBox.Show(ErrMsg);
                //}
            }
        }
        #region Calendar
        private void UpdateLogFileError(string MsgLog)
        {
            #region Update Log
            StringBuilder CreateLog = new StringBuilder();
            CreateLog.Append(String.Format(@"
Err : 001 : LineNotify
{0} 
{1}
-----------------------------------------------", DateTime.Now.ToString(), MsgLog));
            File.AppendAllText(System.Reflection.Assembly.GetEntryAssembly().Location + "Error.txt", CreateLog.ToString());
            CreateLog.Clear();
            #endregion
        }
        private void NotifyCalendarUnfinish()
        {
            string ErrMsg = string.Empty;
            int ErrCode = 0;
            int TranErr = 0;

            DataTable TBCalendar = new DataTable();

            string Query = @"select 
TC.SupervisorName,
EM.Email
,TC.TransactionDescription
,TC.AssigerNamem
,TC.DueDate
,TC.CustomerName
, TC.TransactionStatus
,(select Email from EmployeeMaster where SuffixThai = TC.AssigerName) as EmailAssign
 from EmployeeMaster EM
inner join  TaskOfCompany TC on TC.SupervisorName  like '%'+EM.SuffixThai+'%'
where  TC.TransactionStatus!='F'and TC.TransactionStatus!='W' and TC.DueDate<CONVERT(date, getdate())";

            TranErr += DBSimpleObj.FillData(TBCalendar, Query, ref ErrCode, ref ErrMsg);

            string email = "", assign = "", transactionDescription = "", customerName = "", emailAssign = "";
            DateTime duedate = DateTime.Now;
            foreach (DataRow dr in TBCalendar.Rows)
            {

                email = ReceiveValue.StringReceive("Email", dr);
                if (email == "") emailAssign = "Example@gmail.com";
                emailAssign = ReceiveValue.StringReceive("EmailAssign", dr);
                if (emailAssign == "") emailAssign = "Example@gmail.com";
                duedate = ReceiveValue.DateReceive("DueDate", dr, DateTime.Now);
                assign = ReceiveValue.StringReceive("AssigerName", dr);
                transactionDescription = ReceiveValue.StringReceive("TransactionDescription", dr);
                customerName = ReceiveValue.StringReceive("CustomerName", dr);
                AddToCalendar(email, emailAssign, duedate, assign, transactionDescription, customerName);

            }
        }
        private void initToCalendar()
        {
            string ErrMsg = string.Empty;
            int ErrCode = 0;
            int TranErr = 0;

            DataTable TBCalendar = new DataTable();

            string Query = @"select 
TC.SupervisorName,
EM.Email
,TC.TransactionDescription
,TC.AssigerName
,TC.DueDate
,TC.CustomerName
,(select Email from EmployeeMaster where SuffixThai = TC.AssigerName) as EmailAssign
 from EmployeeMaster EM
inner join  TaskOfCompany TC on TC.SupervisorName  like '%'+EM.SuffixThai+'%'
where TC.CalendarYesNo!='Y' AND TC.TransactionStatus!='F' ";

            TranErr += DBSimpleObj.FillData(TBCalendar, Query, ref ErrCode, ref ErrMsg);

            string email = "", assign = "", transactionDescription = "", customerName = "", emailAssign = "";
            DateTime duedate = DateTime.Now;

            foreach (DataRow dr in TBCalendar.Rows)
            {

                email = ReceiveValue.StringReceive("Email", dr);
                if (email == "") emailAssign = "Example@gmail.com";
                emailAssign = ReceiveValue.StringReceive("EmailAssign", dr);
                if (emailAssign == "") emailAssign = "Example@gmail.com";
                duedate = ReceiveValue.DateReceive("DueDate", dr, DateTime.Now);
                assign = ReceiveValue.StringReceive("AssigerName", dr);
                transactionDescription = ReceiveValue.StringReceive("TransactionDescription", dr);
                customerName = ReceiveValue.StringReceive("CustomerName", dr);
                AddToCalendar(email, emailAssign, duedate, assign, transactionDescription, customerName);

            }
            UpdateCalendar();

        }
        private void AddToCalendar(string email, string emailAssign, DateTime duedateFrom, string assign, string transactionDescription, string customerName)
        {
            TimeSpan from = new TimeSpan(8, 00, 0);
            duedateFrom = duedateFrom + from;

            DateTime duedateTo = duedateFrom;
            TimeSpan to = new TimeSpan(17, 00, 0);
            duedateTo = duedateTo.Date + to;



            UserCredential credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                new ClientSecrets
                {
                    ClientId = "385997726082-bogjgncp2i0atiqdj6mrf5190k6lg08l.apps.googleusercontent.com",
                    ClientSecret = "HFMf6yqLTCRcplSwFRnlUxhI",
                },
                new[] { CalendarService.Scope.Calendar },
                "user",
                CancellationToken.None).Result;


            var service = new CalendarService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = ApplicationName,
            });
            Event newEvent = new Event()
            {
                Summary = transactionDescription + " ของบริษัท" + customerName,
                Location = "Quadra Technology",//"800 Howard St., San Francisco, CA 94103",
                Description = " มอบหมายโดย" + assign,//"A chance to hear more about Google's developer products.",
                Start = new EventDateTime()
                {
                    DateTime = duedateFrom,//DateTime.Parse("2019-01-23T09:00:00-07:00"),
                    TimeZone = "Asia/Bangkok",
                },
                End = new EventDateTime()
                {
                    DateTime = duedateTo, //DateTime.Parse("2019-01-24T17:00:00-07:00"),
                    TimeZone = "Asia/Bangkok",//"America/Los_Angeles",
                },
                Recurrence = new String[] { "RRULE:FREQ=DAILY;COUNT=1" },
                Attendees = new EventAttendee[] {

                    new EventAttendee() { Email = email },
                    new EventAttendee() { Email = emailAssign }
                },
                Reminders = new Event.RemindersData()
                {
                    UseDefault = false,
                    Overrides = new EventReminder[] {
                    new EventReminder() { Method = "email", Minutes = 24 * 60 },
                    new EventReminder() { Method = "sms", Minutes = 10 },
                    }
                }
            };

            String calendarId = "primary";
            EventsResource.InsertRequest request = service.Events.Insert(newEvent, calendarId);
            try
            {
                Event createdEvent = request.Execute();
                Console.WriteLine("Event created: {0}", createdEvent.HtmlLink);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                Console.WriteLine(ex.Message);
            }
        }
        private void UpdateCalendar()
        {
            string ErrMsg = string.Empty;
            int ErrCode = 0;
            int TranErr = 0;

            TranErr = this.DBTransObj.StartTransaction(ref ErrMsg);
            foreach (DataRow dr in TBTasksOfCompany.Select("CalendarYesNo <> 'Y'"))
            {
                dr.BeginEdit();
                dr["CalendarYesNo"] = 'Y';
                dr.EndEdit();
            }
            if (TranErr == 0)
            {
                TranErr += this.DBTransObj.OleDBUpdate(this.TBTasksOfCompany, ref ErrCode, ref ErrMsg);
                if (TranErr == 0)
                {
                    this.DBTransObj.TransCommit();
                }
                else
                {
                    this.DBTransObj.TransRollBack();
                    MessageBox.Show(ErrMsg);
                }
            }
            else
            {
                MessageBox.Show(ErrMsg);
            }


        }
        #endregion
        private void barExport_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            SaveFileDialog SaveFileBug = new SaveFileDialog();
            SaveFileDialog SaveFilePlan = new SaveFileDialog();
            SaveFileDialog SaveFileTest = new SaveFileDialog();
            SaveFileBug.FileName = "BugExcel.xls";


            SaveFilePlan.FileName = "PlanExcel.xls";
            SaveFileTest.FileName = "TestExcel.xls";

            SaveFileBug.InitialDirectory = System.Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            SaveFileBug.DefaultExt = ".xls";
            SaveFileBug.RestoreDirectory = true;

            SaveFilePlan.InitialDirectory = System.Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            SaveFilePlan.DefaultExt = ".xls";
            SaveFilePlan.RestoreDirectory = true;

            SaveFileTest.InitialDirectory = System.Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            SaveFileTest.DefaultExt = ".xls";
            SaveFileTest.RestoreDirectory = true;


            if (SaveFileBug.ShowDialog() == DialogResult.OK)
            {
                this.gridControlBackLog.ExportToXls(SaveFileBug.FileName);


                string FileNameGet = string.Empty;
                if (SaveFileBug.FileName.Contains("\\"))
                {
                    string FileNameReplace = SaveFileBug.FileName.Replace("\\", "❤");
                    string[] ArrayFileName = FileNameReplace.Split('❤');
                    string FileNameTrue = ArrayFileName[ArrayFileName.Length - 1];
                    FileNameGet = SaveFileBug.FileName.Replace(FileNameTrue, "");
                }
                this.gridControlPlanREQ.ExportToXls(FileNameGet + SaveFilePlan.FileName);
                this.gridControlTest.ExportToXls(FileNameGet + SaveFileTest.FileName);
            }

        }
        private void barLineNotify_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DialogResult MessageConfurm = MessageBox.Show("คุณต้องการแจ้งเตือน งานค้างสำหรับวันนี้ และในวันพรุ่งนี้?", "แจ้งเตือนงานค้าง", MessageBoxButtons.YesNo);
            if (MessageConfurm == DialogResult.Yes)
            {
                int TransErr = 0;
                string ErrMsg = string.Empty;
                TransErr += this.CallLineNotify(ref ErrMsg);
                if (TransErr == 0)
                {
                    MessageBox.Show("แจ้งเตือนเรียบร้อยแล้วจ้าาาา !!!");
                }
                else
                {
                    MessageBox.Show("อุ๊ย!...ผิดพลาด " + ErrMsg + " แจ้งปิงด่วน");
                    StringBuilder CreateLog = new StringBuilder();
                    CreateLog.Append(String.Format(@"
Err : 001 : LineNotify
{0} 
{1}
-----------------------------------------------", DateTime.Now.ToString(), ErrMsg));
                    File.AppendAllText(System.Reflection.Assembly.GetEntryAssembly().Location + "Error.txt", CreateLog.ToString());
                    CreateLog.Clear();
                }
            }
        }
        private void barPlan_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (this.ModeSelect == Mode.View)
            {
                return;
            }
            else
            {
                DataRow RowTask = null;
                if (ModeTabSelect != TabSelected.TabPlaREQ)
                    return;

                RowTask = this.advBandedGridPlanREQ.GetFocusedDataRow();
                if (RowTask == null)
                    return;

                string CustomerCode = ReceiveValue.StringReceive(FILED_TasksOfCompany.COL03_CustomerCode, RowTask);

                if (CustomerCode != string.Empty)
                {
                    string Query = String.Format(@"SELECT * FROM {0} WHERE {1} IS NOT NULL ORDER BY {1}"
                                        , TableName.CompanyConfigPlan
                                        , FIELD_CompanyConfigPlan.RowNumber);
                    DataTable TBCompanyPlan = new DataTable();
                    DBSimpleObj.FillData(TBCompanyPlan, Query);
                    if (TBCompanyPlan.Rows.Count > 0)
                    {
                        foreach (DataRow RowPlan in TBCompanyPlan.Rows)
                        {
                            string TransactionDesction = ReceiveValue.StringReceive(FIELD_CompanyConfigPlan.TransactionDescription, RowPlan);
                            DataRow RowNew = this.TBTasksOfCompany.NewRow();
                            RowNew[FILED_TasksOfCompany.COL02_TransactionDate] = DateTime.Now.Date;
                            RowNew[FILED_TasksOfCompany.COL05_DocumentType] = VALUE_Status.DocumenttType_Plan;
                            RowNew[FILED_TasksOfCompany.COL11_TypeSupervisor] = "S";
                            RowNew[FILED_TasksOfCompany.COL06_DocumentTypeRequiment] = RowPlan[FIELD_CompanyConfigPlan.DocumentTypeRequiment];
                            RowNew[FILED_TasksOfCompany.COL03_CustomerCode] = CustomerCode;
                            RowNew[FILED_TasksOfCompany.COL07_StatusCustomer] = "ปกติ";
                            RowNew[FILED_TasksOfCompany.COL15_TransactionStatus] = VALUE_Status.TransactionStatus_Non;
                            RowNew[FILED_TasksOfCompany.COL09_TransactionDescription] = TransactionDesction;
                            RowNew["StatusRow"] = VALUE_Status.NEW;
                            RowNew[FILED_TasksOfCompany.COL18_RecStatus] = 0;
                            RowNew[FILED_TasksOfCompany.COL19_LastUpdated] = DateTime.Now.Date;
                            RowNew[FILED_TasksOfCompany.COL20_UpdatedUser] = this.QEBCenterInfo.Username;
                            this.TBTasksOfCompany.Rows.Add(RowNew);
                        }
                    }
                    else
                    {
                        MessageBox.Show("กรุณา set ConfigPlan");
                    }
                    #region Old
                    /*
                    #region Array Values
                    string[] ArrayTransactionDescription = new string[] 
                    {
                        "ระบบซื้อ (PR,PO,PI)",
                        "ระบบขาย (QT,SO,SI,DO)",
                        "ระบบสินค้าคงคลัง (IAM,IAT,IAD,IAW,RD,PK)",
                        "ระบบบัญชี (CP,CS,JE,CB,ใบวางบิล,ใบเสร็จรับเงิน)",
                        "ระบบผลิต (WO)"
                    };
                    string[] ArrayTransactionFrom = new string[]
                   {
                        "ฟอร์มฟรี 1"
                        ,"ฟอร์มฟรี 2"
                        ,"ฟอร์มฟรี 3"
                        ,"ฟอร์มฟรี 4"
                        ,"ฟอร์มฟรี 5"
                        ,"ฟอร์มฟรี 6"
                        ,"ฟอร์มฟรี 7"
                        ,"ฟอร์มฟรี 8"
                        ,"ฟอร์มฟรี 9"
                        ,"ฟอร์มฟรี 10"
                   };
                    string[] ArrayTransactionType = new string[] 
                    { 
                        VALUE_Status.DocumentTypeRequiment_OpenHouse,
                        VALUE_Status.DocumentTypeRequiment_Template,
                        VALUE_Status.DocumentTypeRequiment_Trainning,
                        VALUE_Status.DocumentTypeRequiment_Setup,
                        VALUE_Status.DocumentTypeRequiment_Config,
                        VALUE_Status.DocumentTypeRequiment_Form
                    };
                    string[] ArrayTransactionDescriptionOther = new string[]
                    {
                        "Send Template",
                        "Brief Requirements",
                    };
                    #endregion

                    string[] ArrayDocumentTypeRequiment = new string[]
                    { 
                        "OpenHouse",
                        "Tranning",
                        "Setup",
                        "Config/File,FormatNo"
                    };
                    int CountArray = 0;
                    */
                    #region Old Plan
                    /*
                    foreach (string TransactionType in ArrayTransactionType)
                    {
                        if (TransactionType == VALUE_Status.DocumentTypeRequiment_Template)
                        {
                            #region Template

                            foreach (string TransactionDesOther in ArrayTransactionDescriptionOther)
                            {
                                DataRow RowNew = this.TBTasksOfCompany.NewRow();
                                RowNew[FILED_TasksOfCompany.COL01_RowNumber] = TBTasksOfCompany.AsEnumerable().Max(r => r.Field<int>(FILED_TasksOfCompany.COL01_RowNumber)) + 1;
                                RowNew[FILED_TasksOfCompany.COL02_TransactionDate] = DateTime.Now;
                                RowNew[FILED_TasksOfCompany.COL03_CustomerCode] = CustomerCode;
                                RowNew[FILED_TasksOfCompany.COL05_DocumentType] = VALUE_Status.DocumenttType_Plan;
                                RowNew[FILED_TasksOfCompany.COL06_DocumentTypeRequiment] = TransactionType;
                                RowNew[FILED_TasksOfCompany.COL07_StatusCustomer] = "ปกติ";
                                RowNew[FILED_TasksOfCompany.COL18_RecStatus] = 0;
                                RowNew[FILED_TasksOfCompany.COL19_LastUpdated] = DateTime.Now;
                                RowNew[FILED_TasksOfCompany.COL20_UpdatedUser] = this.QEBCenterInfo.Username;
                                RowNew[FILED_TasksOfCompany.COL15_TransactionStatus] = VALUE_Status.TransactionStatus_Non;
                                RowNew[FILED_TasksOfCompany.COL09_TransactionDescription] = "Template " + TransactionDesOther;
                                RowNew["StatusRow"] = "N";
                                this.TBTasksOfCompany.Rows.Add(RowNew);
                            }

                            foreach (string TrnsactionDes in ArrayTransactionDescription)
                            {
                                DataRow RowNew = this.TBTasksOfCompany.NewRow();
                                RowNew[FILED_TasksOfCompany.COL01_RowNumber] = TBTasksOfCompany.AsEnumerable().Max(r => r.Field<int>(FILED_TasksOfCompany.COL01_RowNumber)) + 1;
                                RowNew[FILED_TasksOfCompany.COL02_TransactionDate] = DateTime.Now;
                                RowNew[FILED_TasksOfCompany.COL03_CustomerCode] = CustomerCode;
                                RowNew[FILED_TasksOfCompany.COL05_DocumentType] = VALUE_Status.DocumenttType_Plan;
                                RowNew[FILED_TasksOfCompany.COL06_DocumentTypeRequiment] = TransactionType;
                                RowNew[FILED_TasksOfCompany.COL07_StatusCustomer] = "ปกติ";
                                RowNew[FILED_TasksOfCompany.COL18_RecStatus] = 0;
                                RowNew[FILED_TasksOfCompany.COL19_LastUpdated] = DateTime.Now;
                                RowNew[FILED_TasksOfCompany.COL20_UpdatedUser] = this.QEBCenterInfo.Username;
                                RowNew[FILED_TasksOfCompany.COL15_TransactionStatus] = VALUE_Status.TransactionStatus_Non;
                                RowNew[FILED_TasksOfCompany.COL09_TransactionDescription] = "Template " + TrnsactionDes;
                                RowNew["StatusRow"] = "N";
                                this.TBTasksOfCompany.Rows.Add(RowNew);
                            }

                            #endregion
                        }
                        else if (TransactionType == VALUE_Status.DocumentTypeRequiment_Form)
                        {
                            #region Form
                            foreach (string TrnsactionDesFrom in ArrayTransactionFrom)
                            {
                                DataRow RowNew = this.TBTasksOfCompany.NewRow();
                                RowNew[FILED_TasksOfCompany.COL01_RowNumber] = TBTasksOfCompany.AsEnumerable().Max(r => r.Field<int>(FILED_TasksOfCompany.COL01_RowNumber)) + 1;
                                RowNew[FILED_TasksOfCompany.COL02_TransactionDate] = DateTime.Now;
                                RowNew[FILED_TasksOfCompany.COL03_CustomerCode] = CustomerCode;
                                RowNew[FILED_TasksOfCompany.COL05_DocumentType] = VALUE_Status.DocumenttType_Plan;
                                RowNew[FILED_TasksOfCompany.COL06_DocumentTypeRequiment] = TransactionType;
                                RowNew[FILED_TasksOfCompany.COL12_SupervisorName] = "โจ้";
                                RowNew[FILED_TasksOfCompany.COL07_StatusCustomer] = "ปกติ";
                                RowNew[FILED_TasksOfCompany.COL18_RecStatus] = 0;
                                RowNew[FILED_TasksOfCompany.COL19_LastUpdated] = DateTime.Now;
                                RowNew[FILED_TasksOfCompany.COL20_UpdatedUser] = this.QEBCenterInfo.Username;
                                RowNew[FILED_TasksOfCompany.COL15_TransactionStatus] = VALUE_Status.TransactionStatus_Non;
                                RowNew[FILED_TasksOfCompany.COL09_TransactionDescription] = "Form " + TrnsactionDesFrom;
                                RowNew["StatusRow"] = "N";
                                this.TBTasksOfCompany.Rows.Add(RowNew);
                            }
                            #endregion
                        }
                        else
                        {
                            #region Defual
                            DataRow RowNew = this.TBTasksOfCompany.NewRow();
                            RowNew[FILED_TasksOfCompany.COL01_RowNumber] = TBTasksOfCompany.AsEnumerable().Max(r => r.Field<int>(FILED_TasksOfCompany.COL01_RowNumber)) + 1;
                            RowNew[FILED_TasksOfCompany.COL02_TransactionDate] = DateTime.Now;
                            RowNew[FILED_TasksOfCompany.COL03_CustomerCode] = CustomerCode;
                            RowNew[FILED_TasksOfCompany.COL05_DocumentType] = VALUE_Status.DocumenttType_Plan;
                            RowNew[FILED_TasksOfCompany.COL06_DocumentTypeRequiment] = TransactionType;
                            RowNew[FILED_TasksOfCompany.COL07_StatusCustomer] = "ปกติ";
                            RowNew[FILED_TasksOfCompany.COL18_RecStatus] = 0;
                            RowNew[FILED_TasksOfCompany.COL19_LastUpdated] = DateTime.Now;
                            RowNew[FILED_TasksOfCompany.COL20_UpdatedUser] = this.QEBCenterInfo.Username;
                            RowNew[FILED_TasksOfCompany.COL15_TransactionStatus] = VALUE_Status.TransactionStatus_Non;
                            RowNew[FILED_TasksOfCompany.COL09_TransactionDescription] = ArrayDocumentTypeRequiment[CountArray].ToString();
                            RowNew["StatusRow"] = "N";
                            this.TBTasksOfCompany.Rows.Add(RowNew);
                            #endregion
                            CountArray++;
                        }
                    }
                    */
                    #endregion
                    #endregion
                }
            }
        }
        private void BarMail_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DialogResult MessageConfurm = MessageBox.Show("คุณต้องการแจ้งเตือน ผ่าน Mail งานค้างสำหรับวันนี้ และในวันพรุ่งนี้?", "แจ้งเตือนงานค้าง", MessageBoxButtons.YesNo);
            if (MessageConfurm == DialogResult.Yes)
            {
                string ErrMsg = string.Empty;
                int TransErr = 0;
                TransErr = this.CallMail(ref ErrMsg);
                if (TransErr > 0)
                {
                    MessageBox.Show(ErrMsg);
                }
                else
                {
                    MessageBox.Show("Email Send successfully");
                }
            }
        }
        #region Sent mail
        private int CallMail(ref string ErrMsg)
        {
            int TransErr = 0;
            int ErrCode = 0;
            DataTable TBTasksOfCompanyMail = new DataTable();
            DateTime DateFirst = DateTime.Now;
            DateTime DateSecond = DateFirst.AddDays(2);
            string DateFirstxt = DateFirst.Month + "-" + DateFirst.Day + "-" + DateFirst.Year;
            if (DateFirst.DayOfWeek.ToString() == "Friday")
            {
                DateSecond = DateFirst.AddDays(4);
            }
            string DateSecondtxt = DateSecond.Month + "-" + DateSecond.Day + "-" + DateSecond.Year;

            #region Query_Mail
            string Query_Mail = String.Format(@"
SELECT * FROM
(
	SELECT
    1 AS OrderNo
	,ISNULL(STUFF((
			SELECT ','+ EM.NickName
			FROM TaskOfCompany S
			OUTER APPLY (SELECT [Data] AS 'ResponsibleMan' FROM dbo.Split(S.ResponsibleMan,',')) X
			LEFT JOIN 
			(
				SELECT EM.Code, EM.Categories1 AS NickName, EM.NameThai , EM.Title , EM.RecStatus
				FROM EmployeeMaster EM
				WHERE ISNULL(EM.Categories1,'') <> ''
			) EM
			ON EM.Code = x.ResponsibleMan
			WHERE S.RowNumber = T.RowNumber
			AND EM.Code IS NOT NULL
			ORDER BY EM.NameThai
			FOR XML PATH ('')
	), 1, 1, ''),'Support') AS SupervisorName
	, CASE WHEN T.DueDate < GETDATE() THEN CONVERT(DATE,GETDATE()) ELSE CONVERT(DATE,DueDate) END AS 'Date'
	, T.TransactionDescription
	, ISNULL(T.CustomerName,'') AS CustomerName
    , CASE WHEN T.DocumentTypeRequiment = 'T' AND T.DueDate IS NULL THEN 1 ELSE 0 END AS IsTemplate
    , CASE WHEN T.DueDate < GETDATE() THEN CONVERT(DATE,GETDATE()) ELSE CONVERT(DATE,DueDate) END AS DueDate
    , CONVERT(DATE,T.DueDateBigin) AS DueDateBegin
	FROM TaskOfCompany T
	WHERE T.RecStatus = 0
	AND T.DocumentType NOT IN ('K','OT')
	AND CONVERT(DATE,T.DueDate) < '{0}'
	AND ISNULL(T.TransactionStatus,'') IN('N','T','W','P')
	
	UNION ALL
	SELECT
    1 AS OrderNo
    ,ISNULL(STUFF((
			SELECT ','+ EM.NickName
			FROM TaskOfCompany S
			OUTER APPLY (SELECT [Data] AS 'ResponsibleMan' FROM dbo.Split(S.Tester,',')) X
			LEFT JOIN 
			(
				SELECT EM.Code, EM.Categories1 AS NickName, EM.NameThai , EM.Title , EM.RecStatus
				FROM EmployeeMaster EM
				WHERE ISNULL(EM.Categories1,'') <> ''
			) EM
			ON EM.Code = x.ResponsibleMan
			WHERE S.RowNumber = T.RowNumber
			AND EM.Code IS NOT NULL
			ORDER BY EM.NameThai
			FOR XML PATH ('')
	), 1, 1, ''),'Support-Test') AS SupervisorName
	, CASE WHEN T.TestDueDate < GETDATE() THEN CONVERT(DATE,GETDATE()) ELSE CONVERT(DATE,TestDueDate) END AS 'Date'
	, T.TransactionDescription
	, ISNULL(T.CustomerName,'') AS CustomerName
    , CASE WHEN T.DocumentTypeRequiment = 'T' AND T.DueDate IS NULL THEN 1 ELSE 0 END AS IsTemplate
    , CASE WHEN T.TestDueDate < GETDATE() THEN CONVERT(DATE,GETDATE()) ELSE CONVERT(DATE,TestDueDate) END AS DueDate
    , NULL AS DueDateBegin
	FROM TaskOfCompany T
	WHERE T.RecStatus = 0
	AND T.DocumentType IN('B')
	AND ISNULL(T.TestStatus,'') IN('N','P') 
	AND CONVERT(DATE,T.TestDueDate) < '{0}'

    UNION ALL
    SELECT 
    1 AS OrderNo
	, N'พี่นุท' AS SupervisorName
	, CASE WHEN T.TransactionDate < GETDATE() THEN CONVERT(DATE,GETDATE()) ELSE CONVERT(DATE,TransactionDate) END AS [Date]
	, 'รออนุมัติ Test '+CONVERT(NVARCHAR,COUNT(''))+' รายการ' AS TransactionDescription
	, 'Test' AS CustomerName
    , CASE WHEN T.DocumentTypeRequiment = 'T' AND T.DueDate IS NULL THEN 1 ELSE 0 END AS IsTemplate
    , CASE WHEN T.TransactionDate < GETDATE() THEN CONVERT(DATE,GETDATE()) ELSE CONVERT(DATE,TransactionDate) END AS DueDate
    , NULL AS DueDateBegin
	FROM TaskOfCompany T
	WHERE T.RecStatus = 0
	AND T.DocumentType = 'T'
	AND CONVERT(DATE,T.TransactionDate) < '{0}'
    AND ((ISNULL(T.StatusTest,'N') = 'Y' AND ISNULL(T.ApprovedTest,0) = 0)  OR (ISNULL(T.StatusTest,'N') = 'N'))
    AND (ISNULL(T.TransactionDescription,'') <> '' AND ISNULL(T.CustomerName,'') <> '')
    GROUP BY CASE WHEN T.TransactionDate < GETDATE() THEN CONVERT(DATE,GETDATE()) ELSE CONVERT(DATE,TransactionDate) END
	,CASE WHEN T.DocumentTypeRequiment = 'T' AND T.DueDate IS NULL THEN 1 ELSE 0 END

	UNION ALL
    SELECT 
    2 AS OrderNo
    , N'To Talk ที่นัดกับลูกค้าไว้' AS SupervisorName
	, CASE WHEN T.DueDate < GETDATE() THEN CONVERT(DATE,GETDATE()) ELSE CONVERT(DATE,DueDate) END AS 'Date'
	, T.TransactionDescription
	, ISNULL(T.CustomerName,'') AS CustomerName
    , CASE WHEN T.DocumentTypeRequiment = 'T' AND T.DueDate IS NULL THEN 1 ELSE 0 END AS IsTemplate
    , CASE WHEN T.DueDate < GETDATE() THEN CONVERT(DATE,GETDATE()) ELSE CONVERT(DATE,DueDate) END AS DueDate
    , CONVERT(DATE,T.DueDateBigin) AS DueDateBegin
	FROM TaskOfCompany T
	WHERE T.RecStatus = 0
	AND T.DocumentType = 'K'
	AND CONVERT(DATE,T.DueDate) < '{0}'
	AND ISNULL(T.TransactionStatus,'') IN('N','T','W','P')
) T
WHERE T.IsTemplate = 0
GROUP BY T.SupervisorName,T.Date,T.TransactionDescription,T.CustomerName,T.IsTemplate,T.DueDate,T.DueDateBegin,T.OrderNo
ORDER BY T.OrderNo,T.SupervisorName,T.DueDate,T.CustomerName
", DateSecondtxt);
            #endregion
            TransErr += this.DBSimpleObj.FillData(TBTasksOfCompanyMail, Query_Mail, ref ErrCode, ref ErrMsg);
            if (TransErr == 0 && TBTasksOfCompanyMail.Rows.Count > 0)
            {
                DataTable SentMail = this.SplitMailText(this.InitTableMail(TBTasksOfCompanyMail));
                if (SentMail.Rows.Count > 0)
                {
                    foreach (DataRow Rowmail in SentMail.Rows)
                    {
                        string Name = ReceiveValue.StringReceive("Name", Rowmail).Replace("พี่", "");
                        string NameCheck = "หนุ่ย,ปิง,เตอร์,บิ้ว,ตูน";
                        if (!NameCheck.Contains(Name))
                        {
                            string Email = ReceiveValue.StringReceive("Email", Rowmail);
                            string Description = ReceiveValue.StringReceive("Description", Rowmail);
                            string Date = DateTime.Now.Date.ToString("dd MMM yyyy");
                            string FormatBody = String.Format(@"<html>
<head>
<title>แจ้งเตือนงานค้าง {0}</title>
</head>
<body>
<div style = ""background-color : white
; border : 3px solid
; padding : 30"">
<h1 style=""border-bottom : 3px solid; border-color : red"">
	{1}
</h1>
<h3>{2}</h3>
</div>
</body>
</html>", Name + " วันที่ " + Date, Name + " วันที่ " + Date, Description);
                            this.SendEmail("ตามงานค้าง วันที่ " + Date, FormatBody, Email);
                        }
                    }
                }
            }
            return TransErr;
        }
        private DataTable InitTableMail(DataTable TableMail)
        {
            DataTable TableMailClone = TableMail.Clone();
            foreach (DataRow RowMail in TableMail.Rows)
            {
                string NameSent = ReceiveValue.StringReceive("SupervisorName", RowMail).Trim().Replace(" ", "");
                if (NameSent.Contains(","))
                {
                    string[] NameSplit = NameSent.Split(',');
                    foreach (string Arraytxt in NameSplit)
                    {
                        if (Arraytxt != string.Empty)
                        {
                            RowMail["SupervisorName"] = Arraytxt;
                            TableMailClone.ImportRow(RowMail);
                        }
                    }
                }
                else
                {
                    TableMailClone.ImportRow(RowMail);
                }
            }
            return TableMailClone;
        }
        private DataTable SplitMailText(DataTable TBTask)
        {
            DataTable TBMail = new DataTable();
            TBMail.Columns.Add("Name", typeof(String));
            TBMail.Columns.Add("Description", typeof(String));
            TBMail.Columns.Add("Email", typeof(String));

            string[] ArrayColumns = { FILED_TasksOfCompany.COL12_SupervisorName };
            DataTable TBName = TBTask.DefaultView.ToTable(true, ArrayColumns);
            foreach (DataRow RowName in TBName.Rows)
            {
                string NameGet = ReceiveValue.StringReceive(FILED_TasksOfCompany.COL12_SupervisorName, RowName).Trim();
                string FormatSearch = String.Format("{0} = '{1}'", FILED_TasksOfCompany.COL12_SupervisorName, NameGet);
                string FormatSort = String.Format("{0},{1} DESC", FILED_TasksOfCompany.COL12_SupervisorName, "Date");

                string Email = string.Empty;
                string ItemAdd = string.Empty;
                string Date = string.Empty;

                foreach (DataRow RowTask in TBTask.Select(FormatSearch, FormatSort))
                {
                    string Company = ReceiveValue.StringReceive(FILED_TasksOfCompany.COL04_CustomerName, RowTask);
                    string Description = ReceiveValue.StringReceive(FILED_TasksOfCompany.COL09_TransactionDescription, RowTask);
                    string DueDateTrue = string.Empty;
                    DateTime datebegin = DateTime.Now.Date;
                    if (ReceiveValue.StringReceive("DueDateBigin", RowTask) != string.Empty)
                        if (DateTime.TryParse(ReceiveValue.StringReceive("DueDateBigin", RowTask), out datebegin))
                            DueDateTrue = " [กำหนดแรก " + datebegin.ToString("dd/MM/yyyy") + "]";

                    string ComDesc = String.Format("&emsp;-[{0}] {1} [{2}]<br>", Company, Description.Trim(), DueDateTrue);
                    string DateRun = ReceiveValue.StringReceive("Date", RowTask);
                    if (DateRun != Date)
                    {
                        Date = DateRun;
                        ItemAdd += "<br>" + String.Format("{0} : <br>", Date);
                    }
                    ItemAdd += ComDesc;
                }
                Email = this.EmailDic.FirstOrDefault(x => x.Key == NameGet.Replace("พี่", "")).Value;
                if (Email != null)
                {
                    DataRow NewRow = TBMail.NewRow();
                    NewRow["Name"] = NameGet;
                    NewRow["Description"] = ItemAdd;
                    NewRow["Email"] = Email;
                    TBMail.Rows.Add(NewRow);
                }
            }
            return TBMail;
        }
        private void SendEmail(string Topic, string emailBody, string EmailTo)
        {
            MailMessage m = new MailMessage();
            SmtpClient sc = new SmtpClient();
            try
            {
                m.From = new MailAddress("info@quadtech.co.th");
                m.To.Add(EmailTo);
                m.Subject = Topic;
                m.IsBodyHtml = true;
                m.Body = emailBody;

                //sc.Host = "smtp.gmail.com";
                sc.Host = "inetmail.cloud";
                sc.Port = 25;
                sc.Credentials = new System.Net.NetworkCredential("info@quadtech.co.th", "Windows2008");

                sc.EnableSsl = true;
                sc.Send(m);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        #endregion
        #region LineNotify
        private int CallLineNotify(ref string ErrMsg)
        {
            int TranErr = 0;
            int ErrCode = 0;

            DataTable TBTasksOfCompanyLine = new DataTable();
            DateTime DateFirst = DateTime.Now;
            DateTime DateSecond = DateFirst.AddDays(2);
            string DateFirstxt = DateFirst.Month + "-" + DateFirst.Day + "-" + DateFirst.Year;
            if (DateFirst.DayOfWeek.ToString() == "Friday")
                DateSecond = DateFirst.AddDays(4);
            string DateSecondtxt = DateSecond.Month + "-" + DateSecond.Day + "-" + DateSecond.Year;

            // ของ บริษัท
            string Token = "7iEemVHe824139nBuS1SM84thiX7mo9eoEKCFIPwACo";
            // ของ ส่วนตัว
            //string Token = "r1LFrexFoZA1AzGwxBvvclZgCxT3sduiqwbO5RxXow1";

            #region Query_LineNotify
            string Query_LineNotify = String.Format(@"
SELECT * FROM
(
	SELECT
    1 AS OrderNo
	,ISNULL(STUFF((
			SELECT ','+ EM.NickName
			FROM TaskOfCompany S
			OUTER APPLY (SELECT [Data] AS 'ResponsibleMan' FROM dbo.Split(S.ResponsibleMan,',')) X
			LEFT JOIN 
			(
				SELECT EM.Code, EM.Categories1 AS NickName, EM.NameThai , EM.Title , EM.RecStatus
				FROM EmployeeMaster EM
				WHERE ISNULL(EM.Categories1,'') <> ''
			) EM
			ON EM.Code = x.ResponsibleMan
			WHERE S.RowNumber = T.RowNumber
			AND EM.Code IS NOT NULL
			ORDER BY EM.NameThai
			FOR XML PATH ('')
	), 1, 1, ''),'Support') AS SupervisorName
	, CASE WHEN T.DueDate < GETDATE() THEN CONVERT(DATE,GETDATE()) ELSE CONVERT(DATE,DueDate) END AS 'Date'
	, T.TransactionDescription
	, ISNULL(T.CustomerName,'') AS CustomerName
    , CASE WHEN T.DocumentTypeRequiment = 'T' AND T.DueDate IS NULL THEN 1 ELSE 0 END AS IsTemplate
    , CASE WHEN T.DueDate < GETDATE() THEN CONVERT(DATE,GETDATE()) ELSE CONVERT(DATE,DueDate) END AS DueDate
    , CONVERT(DATE,T.DueDateBigin) AS DueDateBegin
	FROM TaskOfCompany T
	WHERE T.RecStatus = 0
	AND T.DocumentType IN ('B','N')
	AND CONVERT(DATE,T.DueDate) < '{0}'
	AND ISNULL(T.TransactionStatus,'') IN('N','T','W','P')
	
	UNION ALL
	SELECT
    1 AS OrderNo
    ,ISNULL(STUFF((
			SELECT ','+ EM.NickName
			FROM TaskOfCompany S
			OUTER APPLY (SELECT [Data] AS 'ResponsibleMan' FROM dbo.Split(S.Tester,',')) X
			LEFT JOIN 
			(
				SELECT EM.Code, EM.Categories1 AS NickName, EM.NameThai , EM.Title , EM.RecStatus
				FROM EmployeeMaster EM
				WHERE ISNULL(EM.Categories1,'') <> ''
			) EM
			ON EM.Code = x.ResponsibleMan
			WHERE S.RowNumber = T.RowNumber
			AND EM.Code IS NOT NULL
			ORDER BY EM.NameThai
			FOR XML PATH ('')
	), 1, 1, ''),'Support-Test') AS SupervisorName
	, CASE WHEN T.TestDueDate < GETDATE() THEN CONVERT(DATE,GETDATE()) ELSE CONVERT(DATE,TestDueDate) END AS 'Date'
	, T.TransactionDescription
	, ISNULL(T.CustomerName,'') AS CustomerName
    , CASE WHEN T.DocumentTypeRequiment = 'T' AND T.DueDate IS NULL THEN 1 ELSE 0 END AS IsTemplate
    , CASE WHEN T.TestDueDate < GETDATE() THEN CONVERT(DATE,GETDATE()) ELSE CONVERT(DATE,TestDueDate) END AS DueDate
    , NULL AS DueDateBegin
	FROM TaskOfCompany T
	WHERE T.RecStatus = 0
	AND T.DocumentType IN('B')
	AND ISNULL(T.TestStatus,'') IN('N','P') 
	AND CONVERT(DATE,T.TestDueDate) < '{0}'

	UNION ALL
    SELECT 
    2 AS OrderNo
    , N'To Talk ที่นัดกับลูกค้าไว้' AS SupervisorName
	, CASE WHEN T.DueDate < GETDATE() THEN CONVERT(DATE,GETDATE()) ELSE CONVERT(DATE,DueDate) END AS 'Date'
	, T.TransactionDescription
	, ISNULL(T.CustomerName,'') AS CustomerName
    , CASE WHEN T.DocumentTypeRequiment = 'T' AND T.DueDate IS NULL THEN 1 ELSE 0 END AS IsTemplate
    , CASE WHEN T.DueDate < GETDATE() THEN CONVERT(DATE,GETDATE()) ELSE CONVERT(DATE,DueDate) END AS DueDate
    , CONVERT(DATE,T.DueDateBigin) AS DueDateBegin
	FROM TaskOfCompany T
	WHERE T.RecStatus = 0
	AND T.DocumentType = 'K'
	AND CONVERT(DATE,T.DueDate) < '{0}'
	AND ISNULL(T.TransactionStatus,'') IN('N','T','W','P')
) T
WHERE T.IsTemplate = 0
GROUP BY T.SupervisorName,T.Date,T.TransactionDescription,T.CustomerName,T.IsTemplate,T.DueDate,T.DueDateBegin,T.OrderNo
ORDER BY T.OrderNo,T.SupervisorName,T.DueDate,T.CustomerName
", DateSecondtxt);
            #endregion

            TranErr += DBSimpleObj.FillData(TBTasksOfCompanyLine, Query_LineNotify, ref ErrCode, ref ErrMsg);
            string EmployeesName = string.Empty;
            if (TBTasksOfCompanyLine.Rows.Count > 0)
            {
                string Msg = string.Empty;
                string MessageALL = string.Empty;

                List<string> LineMsg = GetSplit(TBTasksOfCompanyLine);
                TranErr += SentLine(LineMsg, ref ErrMsg, Token);
                return TranErr;
            }
            return TranErr;
        }
        private List<string> GetSplit(DataTable TBTask)
        {
            List<string> ListLine = new List<string>();

            string[] ArrayColumns = { FILED_TasksOfCompany.COL12_SupervisorName };
            DataTable TBName = TBTask.DefaultView.ToTable(true, ArrayColumns);

            foreach (DataRow RowName in TBName.Rows)
            {
                string NameGet = ReceiveValue.StringReceive(FILED_TasksOfCompany.COL12_SupervisorName, RowName);
                string FormatSearch = String.Format("{0} = '{1}'", FILED_TasksOfCompany.COL12_SupervisorName, NameGet);

                string MixItem = string.Empty;
                string DateCheck = string.Empty;
                int CountRow = 1, TotalRow = TBTask.Select(FormatSearch).Count();

                foreach (DataRow RowTask in TBTask.Select(FormatSearch))
                {
                    string Date = ReceiveValue.StringReceive("Date", RowTask);
                    string DueDateBegin = ReceiveValue.StringReceive("DueDateBegin", RowTask);
                    DateTime dtDate = DateTime.Now.Date;
                    DateTime dtDateBegin = DateTime.MaxValue;
                    string[] listDate = Date.Split('-');
                    string[] listDateBegin = DueDateBegin.Split('-');
                    if (listDate.Length == 3)
                        dtDate = new DateTime(Convert.ToInt32(listDate[0]), Convert.ToInt32(listDate[1]), Convert.ToInt32(listDate[2]));
                    if (listDateBegin.Length == 3)
                        dtDateBegin = new DateTime(Convert.ToInt32(listDateBegin[0]), Convert.ToInt32(listDateBegin[1]), Convert.ToInt32(listDateBegin[2]));

                    if (Date != DateCheck)
                    {
                        MixItem += "\n" + String.Format("{0} : {1:ddd dd/MM/yyyy} \n", NameGet, dtDate);
                        DateCheck = Date;
                    }

                    string Company = ReceiveValue.StringReceive(FILED_TasksOfCompany.COL04_CustomerName, RowTask);
                    string Description = ReceiveValue.StringReceive(FILED_TasksOfCompany.COL09_TransactionDescription, RowTask);
                    string DueDate = string.Empty;
                    if (dtDate > dtDateBegin)
                        DueDate = " [กำหนดแรก " + dtDateBegin.ToString("dd/MM/yyyy") + "]";

                    string ComDesc = String.Format("[{0}] {1}{2} \n", Company, Description.Trim(), DueDate);

                    if ((MixItem.Length + ComDesc.Length) + 50 > 1000)
                    {
                        ListLine.Add(MixItem);
                        MixItem = String.Format("{0} :{1:ddd dd/MM/yyyy} (ต่อ)\n", NameGet, dtDate) + ComDesc;
                    }
                    else
                    {
                        MixItem += ComDesc;
                        if (CountRow == TotalRow)
                        {
                            ListLine.Add(MixItem);
                        }
                    }
                    CountRow++;
                }
            }

            return ListLine;
        }
        private int SentLine(List<string> LineMsg, ref string ErrMsg, string Token)
        {
            int TransErr = 0;
            if (LineMsg.Count > 0)
            {
                string Test = string.Empty;
                LineNotify LineNotifyClass = new LineNotify();
                foreach (string SentLine in LineMsg)
                {
                    string CheckText = SentLine;
                    if (CheckText.Contains("%"))
                    {
                        CheckText = CheckText.Replace("%", "เปอร์เซ็น");
                    }
                    if (CheckText.Contains("&"))
                    {
                        CheckText = CheckText.Replace("&", "and");
                    }

                    TransErr += LineNotifyClass.lineNotify("\n" + CheckText.Trim(), Token, ref ErrMsg);
                }
            }
            return TransErr;
        }
        #endregion
        private bool Flag = false;
        private void TBTasksOfCompany_ColumnChanged(object sender, DataColumnChangeEventArgs e)
        {
            e.Row.EndEdit();
            string FocusColumns = e.Column.ColumnName;
            string ErrMsg = string.Empty;
            int ErrCode = 0;
            int TransErr = 0;
            if (ModeSelect == Mode.View || ModeCopyOrNormal == ModeCopyPress.CopyPress)
                return;
            if (Flag)
                return;
            string StatusRow = ReceiveValue.StringReceive("StatusRow", e.Row);
            if (StatusRow == string.Empty)
            {
                e.Row.BeginEdit();
                e.Row["StatusRow"] = VALUE_Status.EDIT;
                e.Row.EndEdit();
            }
            #region FILED CustomerCode
            if (FocusColumns == FILED_TasksOfCompany.COL03_CustomerCode)
            {
                string CustomerCode = ReceiveValue.StringReceive(FILED_TasksOfCompany.COL03_CustomerCode, e.Row);
                if (CustomerCode != string.Empty)
                {
                    string Query_CustomerName = @"
SELECT NickName 
FROM CustomerMaster 
WHERE Code = '{0}'
AND RecStatus = 0";
                    DataTable TBCustomerName = new DataTable();
                    TransErr += DBSimpleObj.FillData(TBCustomerName, string.Format(Query_CustomerName, CustomerCode), ref ErrCode, ref ErrMsg);

                    if (TransErr == 0)
                    {
                        if (TBCustomerName.Rows.Count > 0)
                        {
                            DataRow RowCustomer = TBCustomerName.Rows[0];
                            string CustomerName = ReceiveValue.StringReceive(FILED_CustomerMaster.NickName, RowCustomer);

                            e.Row.BeginEdit();
                            e.Row[FILED_TasksOfCompany.COL04_CustomerName] = CustomerName;
                            e.Row.EndEdit();
                        }
                        else
                        {
                            e.Row.BeginEdit();
                            e.Row[FILED_TasksOfCompany.COL04_CustomerName] = string.Empty;
                            e.Row[FILED_TasksOfCompany.COL03_CustomerCode] = string.Empty;

                            e.Row.EndEdit();
                            MessageBox.Show("ไม่พบ รหัส ลูกค้า กรุณาลองใหม่");
                        }
                    }
                    else
                    {
                        MessageBox.Show(ErrMsg);
                    }
                }
            }
            #endregion
            #region FILED DocumentType
            else if (FocusColumns == FILED_TasksOfCompany.COL05_DocumentType)
            {
                string StatusTransaction = ReceiveValue.StringReceive(FILED_TasksOfCompany.COL05_DocumentType, e.Row);
                string CustomerCode = ReceiveValue.StringReceive(FILED_TasksOfCompany.COL03_CustomerCode, e.Row);
                if (CustomerCode != string.Empty)
                {
                    if (StatusTransaction == VALUE_Status.DocumenttType_Plan)
                    {
                        this.CheckEnableButtonPlan(true);
                    }
                    else
                    {
                        this.CheckEnableButtonPlan(false);
                    }
                }
                else
                {
                    this.CheckEnableButtonPlan(false);
                }
            }
            #endregion
            else if (FocusColumns == FILED_TasksOfCompany.COL39_ApprovedTest)
            {
                bool CheckApproved = ReceiveValue.BoolReceive(FILED_TasksOfCompany.COL39_ApprovedTest, e.Row, false);
                if (CheckApproved)
                {
                    if (this.QEBCenterInfo.Username != "QERP_noot"
                        && this.QEBCenterInfo.Username != "QERP_jo")
                    {
                        MessageBox.Show("....มีสิทธิ์แค่พี่นุทกับพี่โจ้นะจ๊ะ.");
                        e.Row.BeginEdit();
                        e.Row[FILED_TasksOfCompany.COL39_ApprovedTest] = false;
                        e.Row.EndEdit();
                    }
                }
            }
            else if (FocusColumns == FILED_TasksOfCompany.COL15_TransactionStatus)
            {
                string DocumentType = ReceiveValue.StringReceive("DocumentType", e.Row);
                string TransactionStatus = ReceiveValue.StringReceive("TransactionStatus", e.Row);
                if (DocumentType == VALUE_Status.DocumenttType_Bug
                    && TransactionStatus == "F")
                {
                    e.Row.BeginEdit();
                    e.Row["Tester"] = "Support-Test";
                    e.Row["TestDueDate"] = DateTime.Now.Date.AddDays(1);
                    e.Row["TestStatus"] = VALUE_Status.TransactionStatus_Non;
                    e.Row.EndEdit();
                }
                else if (DocumentType == VALUE_Status.DocumenttType_Bug
                   && TransactionStatus == "FWT")
                {
                    this.Flag = true;
                    e.Row.BeginEdit();
                    e.Row[FILED_TasksOfCompany.COL15_TransactionStatus] = "F";
                    e.Row.EndEdit();
                    this.Flag = false;
                }
            }
            else if (FocusColumns == "TestStatus")
            {
                string DocumentType = ReceiveValue.StringReceive("DocumentType", e.Row);
                string TestStatus = ReceiveValue.StringReceive("TestStatus", e.Row);
                if (DocumentType == VALUE_Status.DocumenttType_Bug
                    && TestStatus == "")
                {
                    e.Row.BeginEdit();
                    e.Row["Tester"] = DBNull.Value;
                    e.Row["TestDueDate"] = DBNull.Value;
                    e.Row.EndEdit();
                }
                else if (DocumentType == VALUE_Status.DocumenttType_Bug
                    && TestStatus == "R")
                {
                    e.Row.BeginEdit();
                    e.Row["StartDate"] = DBNull.Value;
                    e.Row["DueDate"] = DateTime.Now.Date.AddDays(1);
                    e.Row["DueDateBigin"] = DateTime.Now.Date.AddDays(1);
                    e.Row[FILED_TasksOfCompany.COL15_TransactionStatus] = "N";
                    e.Row["TestStatus"] = "";
                    e.Row.EndEdit();
                }
            }
        }
        private void UpdateCalendarRow()
        {
            string ErrMsg = string.Empty;
            int ErrCode = 0;
            int TranErr = 0;
            foreach (DataRow dr in TBTasksOfCompany.Rows)
            {
                if (dr.HasVersion(DataRowVersion.Original))
                {
                    DateTime dateNew = DateTime.Now.Date
                            , dateOld = DateTime.Now.Date;
                    DateTime.TryParse(dr["DueDate", DataRowVersion.Original].ToString(), out dateOld);
                    DateTime.TryParse(dr["DueDate"].ToString(), out dateNew);
                    if (dateNew != dateOld)
                    {
                        dr.BeginEdit();
                        dr["CalendarYesNo"] = 'N';
                        dr["DeleteCalendar"] = "N";
                        dr.EndEdit();
                    }
                }
            }
            if (TranErr == 0)
            {
                TranErr += this.DBTransObj.OleDBUpdate(this.TBTasksOfCompany, ref ErrCode, ref ErrMsg);
            }
        }
        private void EnterKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.LoadData();
            }
        }
        private void StripMenuCancel_Click(object sender, EventArgs e)
        {
            //DataRow RowTask = null;
            AdvBandedGridView advBandedGrid = new AdvBandedGridView();
            if (ModeTabSelect == TabSelected.TabBug)
                advBandedGrid = this.advBandedGridTaskOfCompany;
            //RowTask = this.advBandedGridTaskOfCompany.GetFocusedDataRow();
            if (ModeTabSelect == TabSelected.TabPlaREQ)
                advBandedGrid = this.advBandedGridPlanREQ;
            //RowTask = this.advBandedGridPlanREQ.GetFocusedDataRow();
            if (ModeTabSelect == TabSelected.TabTest)
                advBandedGrid = this.advBandedGridTest;
            //RowTask = this.advBandedGridTest.GetFocusedDataRow();
            int[] IndexRow = advBandedGrid.GetSelectedRows();
            if (IndexRow.Length > 0)
            {
                foreach (int Index in IndexRow)
                {
                    DataRow RowSelect = advBandedGrid.GetDataRow(Index);
                    if (RowSelect != null)
                    {
                        string StatusRow = ReceiveValue.StringReceive("StatusRow", RowSelect);
                        if (StatusRow == VALUE_Status.NEW)
                        {
                            DialogResult CheckButton = MessageBox.Show("คุณต้องการลบหรือไม่ ?", "ลบแน่นะ ?", MessageBoxButtons.YesNo);
                            if (CheckButton == DialogResult.Yes)
                            {
                                this.TBTasksOfCompany.Rows.Remove(RowSelect);
                            }
                        }
                        else if (StatusRow == VALUE_Status.EDIT || StatusRow == string.Empty)
                        {
                            RowSelect.BeginEdit();
                            RowSelect["StatusRow"] = VALUE_Status.DELETE;
                            RowSelect["ColorStatus"] = VALUE_Status.Color_Delete;
                            RowSelect.EndEdit();
                        }
                    }
                }
            }
        }
        private void StripMenuAdd_Click(object sender, EventArgs e)
        {
            if (this.ModeSelect == Mode.View)
            {
                return;
            }
            else
            {
                int NoLine = 1;
                string TabSelect = string.Empty;
                if (this.TabControl.SelectedTabPage == this.TabTest)
                    TabSelect = VALUE_Status.DocumenttType_Test;
                if (this.TabControl.SelectedTabPage == this.TabBacklog)
                    TabSelect = VALUE_Status.DocumenttType_Bug;
                if (this.TabControl.SelectedTabPage == this.TabPlanREQ)
                    TabSelect = VALUE_Status.DocumenttType_Requiment;


                if (this.TBTasksOfCompany.Rows.Count > 0)
                {
                    NoLine = TBTasksOfCompany.AsEnumerable().Max(r => r.Field<int>(FILED_TasksOfCompany.COL01_RowNumber)) + 1;
                }

                DataRow RowNew = this.TBTasksOfCompany.NewRow();
                RowNew[FILED_TasksOfCompany.COL01_RowNumber] = NoLine;
                RowNew[FILED_TasksOfCompany.COL02_TransactionDate] = DateTime.Now;
                RowNew[FILED_TasksOfCompany.COL05_DocumentType] = TabSelect;
                RowNew[FILED_TasksOfCompany.COL07_StatusCustomer] = "ปกติ";
                RowNew[FILED_TasksOfCompany.COL18_RecStatus] = 0;
                RowNew[FILED_TasksOfCompany.COL19_LastUpdated] = DateTime.Now;
                RowNew[FILED_TasksOfCompany.COL20_UpdatedUser] = this.QEBCenterInfo.Username;
                RowNew[FILED_TasksOfCompany.COL15_TransactionStatus] = VALUE_Status.TransactionStatus_Non;
                RowNew["ColorStatus"] = VALUE_Status.Color_Black;
                RowNew["StatusRow"] = "N";
                RowNew["NoItem"] = "NEW";
                if (TabSelect == VALUE_Status.DocumenttType_Test)
                {
                    RowNew[FILED_TasksOfCompany.COL40_StatusTest] = "N";
                }
                this.TBTasksOfCompany.Rows.Add(RowNew);
            }
        }
        private void StripMenuReCancel_Click(object sender, EventArgs e)
        {
            DataRow RowTask = this.advBandedGridTaskOfCompany.GetFocusedDataRow();
            if (RowTask == null)
            {
                return;
            }
            else
            {
                string StatusRow = ReceiveValue.StringReceive("StatusRow", RowTask);
                string ReColor = ReceiveValue.StringReceive("BackColorStatus", RowTask);
                if (StatusRow == VALUE_Status.DELETE)
                {
                    RowTask.BeginEdit();
                    RowTask["StatusRow"] = VALUE_Status.EDIT;
                    RowTask["ColorStatus"] = ReColor;
                    RowTask.EndEdit();
                }
            }
        }
        private void contextMenuStripGrit_Opening(object sender, CancelEventArgs e)
        {
            if (this.ModeSelect == Mode.View)
            {
                e.Cancel = true;
                return;
            }
            else
            {
                DataRow RowFocus = null;
                if (ModeTabSelect == TabSelected.TabBug)
                    RowFocus = this.advBandedGridTaskOfCompany.GetFocusedDataRow();
                if (ModeTabSelect == TabSelected.TabPlaREQ)
                    RowFocus = this.advBandedGridPlanREQ.GetFocusedDataRow();
                if (ModeTabSelect == TabSelected.TabTest)
                    RowFocus = this.advBandedGridTest.GetFocusedDataRow();

                if (RowFocus == null)
                {
                    this.StripMenuAdd.Enabled = true;
                    this.StripMenuCancel.Enabled = false;
                    this.StripMenuColor.Enabled = false;
                    this.StripMenuReCancel.Enabled = false;
                    this.StripMenuCopy.Enabled = false;
                    this.StripMenuPaste.Enabled = false;
                }
                else
                {
                    int RecStatus = ReceiveValue.IntReceive(FILED_TasksOfCompany.COL18_RecStatus, RowFocus, 0);
                    string StatusRow = ReceiveValue.StringReceive("StatusRow", RowFocus);
                    if (RecStatus == 0)
                    {
                        this.StripMenuAdd.Enabled = true;
                        this.StripMenuColor.Enabled = true;
                        this.StripMenuPaste.Enabled = true;
                        this.StripMenuCopy.Enabled = true;

                        if (StatusRow == VALUE_Status.DELETE)
                        {
                            this.StripMenuCancel.Enabled = false;
                            this.StripMenuReCancel.Enabled = true;
                        }
                        else
                        {
                            this.StripMenuCancel.Enabled = true;
                            this.StripMenuReCancel.Enabled = false;
                        }
                    }
                    else
                    {
                        this.StripMenuCancel.Enabled = false;
                        this.StripMenuAdd.Enabled = true;
                        this.StripMenuColor.Enabled = true;
                    }
                    if (this.TBTasksOfCompanyCopy.Rows.Count == 0)
                    {
                        this.StripMenuPaste.Enabled = false;
                    }
                }
            }
        }
        private void advBandedGridTaskOfCompany_CustomRowFilter(object sender, DevExpress.XtraGrid.Views.Base.RowFilterEventArgs e)
        {
            ColumnView view = sender as ColumnView;
            string StatusRow = view.GetListSourceRowCellValue(e.ListSourceRow, "StatusRow").ToString();
            if (StatusRow == VALUE_Status.NEW)
            {
                e.Visible = true;
                e.Handled = true;
            }
        }
        private void CheckEnableControl(Mode CurrentMode)
        {
            if (CurrentMode == Mode.View)
            {
                this.barEdit.Enabled = true;
                this.barSave.Enabled = false;
                this.BarUndo.Enabled = false;

                this.btnSearch.Enabled = true;
                this.txtSearch.Enabled = true;
                this.dateSearchTo.Enabled = true;
                this.dateSearchFrom.Enabled = true;
                this.lueDateSearch.Enabled = true;
                this.lueSerch.Enabled = true;
                this.SearchStatus_CheckCombobox.Enabled = true;
                this.CheckEnableButtonPlan(false);
            }
            else
            {
                this.barEdit.Enabled = false;
                this.barSave.Enabled = true;
                this.BarUndo.Enabled = true;

                this.btnSearch.Enabled = false;
                this.txtSearch.Enabled = false;
                this.dateSearchTo.Enabled = false;
                this.dateSearchFrom.Enabled = false;
                this.lueDateSearch.Enabled = false;
                this.lueSerch.Enabled = false;
                this.SearchStatus_CheckCombobox.Enabled = false;
            }
        }
        private void CheckEnableButtonPlan(bool Check)
        {
            this.barPlan.Enabled = Check;
        }
        private void StripMenuCopy_Click(object sender, EventArgs e)
        {
            if (ModeSelect == Mode.View)
                return;
            int[] IndexRow = null;
            AdvBandedGridView advBanGridSelect = new AdvBandedGridView();
            if (ModeTabSelect == TabSelected.TabBug)
                if (this.advBandedGridTaskOfCompany.GetSelectedRows() != null)
                {
                    advBanGridSelect = this.advBandedGridTaskOfCompany;
                    IndexRow = this.advBandedGridTaskOfCompany.GetSelectedRows();
                }
            if (ModeTabSelect == TabSelected.TabPlaREQ)
                if (this.advBandedGridPlanREQ.GetSelectedRows() != null)
                {
                    advBanGridSelect = this.advBandedGridPlanREQ;
                    IndexRow = this.advBandedGridPlanREQ.GetSelectedRows();
                }
            if (ModeTabSelect == TabSelected.TabTest)
                if (this.advBandedGridTest.GetSelectedRows() != null)
                {
                    advBanGridSelect = this.advBandedGridTest;
                    IndexRow = this.advBandedGridTest.GetSelectedRows();
                }
            if (IndexRow != null)
            {
                this.TBTasksOfCompanyCopy.Clear();
                foreach (int Index in IndexRow)
                {
                    this.TBTasksOfCompanyCopy.ImportRow(advBanGridSelect.GetDataRow(Index));
                }
            }
        }
        private void StripMenuPaste_Click(object sender, EventArgs e)
        {
            if (this.TBTasksOfCompanyCopy.Rows.Count > 0)
            {
                ModeCopyOrNormal = ModeCopyPress.CopyPress;
                foreach (DataRow RowCopy in this.TBTasksOfCompanyCopy.Rows)
                {
                    DataRow RowNew = this.TBTasksOfCompany.NewRow();
                    RowNew[FILED_TasksOfCompany.COL02_TransactionDate] = RowCopy[FILED_TasksOfCompany.COL02_TransactionDate];
                    RowNew[FILED_TasksOfCompany.COL03_CustomerCode] = RowCopy[FILED_TasksOfCompany.COL03_CustomerCode];
                    RowNew[FILED_TasksOfCompany.COL04_CustomerName] = RowCopy[FILED_TasksOfCompany.COL04_CustomerName];
                    RowNew[FILED_TasksOfCompany.COL05_DocumentType] = RowCopy[FILED_TasksOfCompany.COL05_DocumentType];
                    RowNew[FILED_TasksOfCompany.COL06_DocumentTypeRequiment] = RowCopy[FILED_TasksOfCompany.COL06_DocumentTypeRequiment];
                    RowNew[FILED_TasksOfCompany.COL24_Version] = RowCopy[FILED_TasksOfCompany.COL24_Version];
                    RowNew[FILED_TasksOfCompany.COL08_ProgramName] = RowCopy[FILED_TasksOfCompany.COL08_ProgramName];
                    RowNew[FILED_TasksOfCompany.COL09_TransactionDescription] = RowCopy[FILED_TasksOfCompany.COL09_TransactionDescription];
                    //RowNew[FILED_TasksOfCompany.COL10_AssigerName] = RowCopy[FILED_TasksOfCompany.COL10_AssigerName];
                    RowNew["AssigerCode"] = RowCopy["AssigerCode"];
                    RowNew[FILED_TasksOfCompany.COL11_TypeSupervisor] = RowCopy[FILED_TasksOfCompany.COL11_TypeSupervisor];
                    //RowNew[FILED_TasksOfCompany.COL12_SupervisorName] = RowCopy[FILED_TasksOfCompany.COL12_SupervisorName];
                    RowNew["ResponsibleMan"] = RowCopy["ResponsibleMan"];
                    RowNew[FILED_TasksOfCompany.COL13_DueDate] = RowCopy[FILED_TasksOfCompany.COL13_DueDate];
                    RowNew[FILED_TasksOfCompany.COL15_TransactionStatus] = VALUE_Status.TransactionStatus_Non;
                    RowNew[FILED_TasksOfCompany.COL23_FolderBackUP] = RowCopy[FILED_TasksOfCompany.COL23_FolderBackUP];
                    RowNew[FILED_TasksOfCompany.COL40_StatusTest] = RowCopy[FILED_TasksOfCompany.COL40_StatusTest];
                    RowNew["CustomerDueDate"] = RowCopy["CustomerDueDate"];

                    RowNew[FILED_TasksOfCompany.COL18_RecStatus] = 0;
                    RowNew[FILED_TasksOfCompany.COL19_LastUpdated] = DateTime.Now;
                    RowNew[FILED_TasksOfCompany.COL20_UpdatedUser] = this.QEBCenterInfo.Username;
                    RowNew["ColorStatus"] = VALUE_Status.Color_Black;
                    RowNew["StatusRow"] = VALUE_Status.NEW;
                    RowNew["NoItem"] = "NEW";
                    this.TBTasksOfCompany.Rows.Add(RowNew);
                }
                ModeCopyOrNormal = ModeCopyPress.Normal;
            }
        }
        private void TabControl_SelectedPageChanged(object sender, DevExpress.XtraTab.TabPageChangedEventArgs e)
        {
            if (e.Page == this.TabBacklog)
                ModeTabSelect = TabSelected.TabBug;
            if (e.Page == this.TabPlanREQ)
                ModeTabSelect = TabSelected.TabPlaREQ;
            if (e.Page == this.TabTest)
                ModeTabSelect = TabSelected.TabTest;
            this.TBTasksOfCompanyCopy.Clear();
        }
        private void CustomRowCellEditForEditing(object sender, DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventArgs e)
        {
            if (e.Column.FieldName == "AssigerCode")
            {
                RepositoryItemCheckedComboBoxEdit ctr = new RepositoryItemCheckedComboBoxEdit();
                DataView view = new DataView(this.TBEmployee, "RecStatus = 0", "NickName ASC", DataViewRowState.CurrentRows);
                ctr.DataSource = view;
                ctr.DisplayMember = "NickName";
                ctr.ValueMember = "Code";
                ctr.DropDownRows = view.Count + 1;
                e.RepositoryItem = ctr;
            }
            else if (e.Column.FieldName == "ResponsibleMan")
            {
                DataRow rowFocus = (sender as GridView).GetDataRow(e.RowHandle);
                if (rowFocus == null)
                    return;
                string TypeSupervisor = ReceiveValue.StringReceive("TypeSupervisor", rowFocus);
                RepositoryItemCheckedComboBoxEdit ctr = new RepositoryItemCheckedComboBoxEdit();
                string filter = "RecStatus = 0";
                if (TypeSupervisor != "")
                    filter += " AND Title = '" + TypeSupervisor + "'";
                DataView view = new DataView(this.TBEmployee, filter, "NickName ASC", DataViewRowState.CurrentRows);
                ctr.DataSource = view;
                ctr.DisplayMember = "NickName";
                ctr.ValueMember = "Code";
                ctr.DropDownRows = view.Count + 1;
                e.RepositoryItem = ctr;
            }
            else if (e.Column.FieldName == "Tester")
            {
                DataRow rowFocus = (sender as GridView).GetDataRow(e.RowHandle);
                if (rowFocus == null)
                    return;
                string TypeSupervisor = "S";
                RepositoryItemCheckedComboBoxEdit ctr = new RepositoryItemCheckedComboBoxEdit();
                string filter = "RecStatus = 0";
                if (TypeSupervisor != "")
                    filter += " AND Title = '" + TypeSupervisor + "'";
                DataView view = new DataView(this.TBEmployee, filter, "NickName ASC", DataViewRowState.CurrentRows);
                ctr.DataSource = view;
                ctr.DisplayMember = "NickName";
                ctr.ValueMember = "Code";
                ctr.DropDownRows = view.Count + 1;
                e.RepositoryItem = ctr;
            }
        }
        private void BarConfigPlan_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            PopUpConfigPlan CompanyConfigPlan = new PopUpConfigPlan(this.QEBCenterInfo, this.QEBds, this.DBSimpleObj, this.DBTransObj);
            CompanyConfigPlan.ShowDialog();
        }
    }
}