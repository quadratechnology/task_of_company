﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TasksOfCompany
{
    public class VALUE_Status
    {
        public const string NEW = "N";
        public const string EDIT = "E";
        public const string DELETE = "D";

        public const string Color_Red = "Red";
        public const string Color_Blue = "Blue";
        public const string Color_Delete = "Delete";
        public const string Color_Green = "Green";
        public const string Color_LightPink = "LightPink";
        public const string Color_Yellow = "Yellow";
        public const string Color_Black = "Black";

        public const string TransactionStatus_Finish = "F";
        public const string TransactionStatus_Test = "T";
        public const string TransactionStatus_Wait = "W";
        public const string TransactionStatus_Non = "N";
        public const string TransactionStatus_Processing = "P";

        public const string DocumenttType_Requiment = "R";
        public const string DocumenttType_Note = "N";
        public const string DocumenttType_Bug = "B";
        public const string DocumenttType_Plan = "P";
        public const string DocumenttType_Test = "T";
        public const string DocumenttType_ToTalk = "K";

        public const string DocumentTypeRequiment_Form = "F";
        public const string DocumentTypeRequiment_Report = "R";
        public const string DocumentTypeRequiment_Program = "P";
        public const string DocumentTypeRequiment_Config = "C";
        public const string DocumentTypeRequiment_Template = "T";
        public const string DocumentTypeRequiment_Trainning = "TR";
        public const string DocumentTypeRequiment_OpenHouse = "O";
        public const string DocumentTypeRequiment_Setup = "S";

    }
    public class VALUE_TabPage
    {
        public const string TabBug = "B";
        public const string TabPlanREQ = "P";
        public const string TabTest = "T";
    }
}
