﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TasksOfCompany
{
    public class FILED_TasksOfCompany
    {
        public const string COL01_RowNumber = "RowNumber";
        public const string COL02_TransactionDate = "TransactionDate";
        public const string COL03_CustomerCode = "CustomerCode";
        public const string COL04_CustomerName = "CustomerName";
        public const string COL05_DocumentType = "DocumentType";
        public const string COL06_DocumentTypeRequiment = "DocumentTypeRequiment";
        public const string COL07_StatusCustomer = "StatusCustomer";
        public const string COL08_ProgramName = "ProgramName";
        public const string COL09_TransactionDescription = "TransactionDescription";
        public const string COL10_AssigerName = "AssigerName";
        public const string COL11_TypeSupervisor = "TypeSupervisor";
        public const string COL12_SupervisorName = "SupervisorName";
        public const string COL13_DueDate = "DueDate";
        public const string COL14_DueDateTrue = "DueDateTrue";
        public const string COL15_TransactionStatus = "TransactionStatus";
        public const string COL16_TransactionConClude = "TransactionConClude";
        public const string COL17_StatusColor = "StatusColor";
        public const string COL18_RecStatus = "RecStatus";
        public const string COL19_LastUpdated = "LastUpdated";
        public const string COL20_UpdatedUser = "UpdatedUser";
        public const string COL21_CalendarYesNo = "CalendarYesNo";
        public const string COL22_DueDateBigin = "DueDateBigin";

        // Tab TEST
        public const string COL23_FolderBackUP = "FolderBackUP";
        public const string COL24_Version = "Version";
        public const string COL25_UserTest = "UserTest";
        public const string COL26_ProgramTest = "ProgramTest";
        public const string COL27_CreateTest = "CreateTest";
        public const string COL28_PullTest = "PullTest";
        public const string COL29_ADDTest = "ADDTest";
        public const string COL30_SaveTest = "SaveTest";
        public const string COL31_EditTest = "EditTest";
        public const string COL32_CancelTest = "CancelTest";
        public const string COL33_SearchTest = "SearchTest";
        public const string COL34_PriviewTest = "PriviewTest";
        public const string COL35_RandomTest = "RandomTest";
        public const string COL35_OpenFileVersion = "OpenFileVersion";
        public const string COL37_WebUpdate = "WebUpdate";
        public const string COL38_TestUpdate = "TestUpdate";
        public const string COL39_ApprovedTest = "ApprovedTest";
        public const string COL40_StatusTest = "StatusTest";
        public const string COL41_Timeperiod = "Timeperiod";
    }
}
