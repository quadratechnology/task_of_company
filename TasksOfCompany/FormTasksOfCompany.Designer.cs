﻿namespace TasksOfCompany
{
    partial class FormTasksOfCompany
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormTasksOfCompany));
            QEB.Center center1 = new QEB.Center();
            this.styleControllerBlue = new DevExpress.XtraEditors.StyleController(this.components);
            this.styleControllerRed = new DevExpress.XtraEditors.StyleController(this.components);
            this.contextMenuStripGrit = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.StripMenuAdd = new System.Windows.Forms.ToolStripMenuItem();
            this.StripMenuCancel = new System.Windows.Forms.ToolStripMenuItem();
            this.StripMenuReCancel = new System.Windows.Forms.ToolStripMenuItem();
            this.StripMenuColor = new System.Windows.Forms.ToolStripComboBox();
            this.StripMenuCopy = new System.Windows.Forms.ToolStripMenuItem();
            this.StripMenuPaste = new System.Windows.Forms.ToolStripMenuItem();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.barEdit = new DevExpress.XtraBars.BarButtonItem();
            this.barSave = new DevExpress.XtraBars.BarButtonItem();
            this.BarUndo = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barPlan = new DevExpress.XtraBars.BarButtonItem();
            this.barLineNotify = new DevExpress.XtraBars.BarButtonItem();
            this.barMail = new DevExpress.XtraBars.BarButtonItem();
            this.barExport = new DevExpress.XtraBars.BarButtonItem();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.barSubItem1 = new DevExpress.XtraBars.BarSubItem();
            this.btnbarEdit = new DevExpress.XtraBars.BarButtonItem();
            this.btnbarSave = new DevExpress.XtraBars.BarButtonItem();
            this.btnbarUndo = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem2 = new DevExpress.XtraBars.BarSubItem();
            this.barConfigPlan = new DevExpress.XtraBars.BarButtonItem();
            this.barAndDockingController1 = new DevExpress.XtraBars.BarAndDockingController(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.txtSearch = new DevExpress.XtraEditors.TextEdit();
            this.lueSerch = new DevExpress.XtraEditors.LookUpEdit();
            this.dateSearchTo = new DevExpress.XtraEditors.DateEdit();
            this.gridControlBackLog = new DevExpress.XtraGrid.GridControl();
            this.advBandedGridTaskOfCompany = new DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView();
            this.gridBandTask01 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.Col_NoLine = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.Col_TransactionDate = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryTransactionDate = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.Col_CustomerCode = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryCustomerCode = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.Col_CustomerName = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.Col_DocumentType = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repository_DocumentType = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.Col_DocumentTypeRequiment = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryDocumentTypeRequiment = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.Col_ProgramName = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.Col_StatusCustomer = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.Col_TransactionDescription = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryTransactionDescription = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.Col_CustomerDueDate = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryDueDate = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.Col_AssigerCode = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemEmployee = new DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit();
            this.Col_AssigerName = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.Col_SupervisorName = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.Col_TypeSupervisor = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repository_TypeSupervisor = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.Col_ResponsibleMan = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.Col_DueDateBigin = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryDueDateBigin = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.Col_DueDate = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.Col_DueDateTrue = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryDueDateTrue = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.Col_TransactionStatus = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repository_TransactionStatus = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.gridBand4 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.Col_Tester = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.Col_TestDueDate = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.Col_TestStatus = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemTestStatus = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.gridBand5 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.Col_TransactionConClude = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryTransactionConClude = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.lueDateSearch = new DevExpress.XtraEditors.LookUpEdit();
            this.dateSearchFrom = new DevExpress.XtraEditors.DateEdit();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.SearchStatus_CheckCombobox = new DevExpress.XtraEditors.CheckedComboBoxEdit();
            this.btnSearch = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.TabControl = new DevExpress.XtraTab.XtraTabControl();
            this.TabBacklog = new DevExpress.XtraTab.XtraTabPage();
            this.TabPlanREQ = new DevExpress.XtraTab.XtraTabPage();
            this.gridControlPlanREQ = new DevExpress.XtraGrid.GridControl();
            this.advBandedGridPlanREQ = new DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView();
            this.gridBand2 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.TabPlanREQNoLine = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.TabPlanREQTransactionDate = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemTabPlanREQTransactionDate = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.TabPlanREQCustomerCode = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemTabPlanREQCustomerCode = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.TabPlanREQCustomerName = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.TabPlanREQDocumentType = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemTabPlanREQDocumentType = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.TabPlanREQDocumentTypeRequiment = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemTabPlanREQDocumentTypeRequiment = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.TabPlanREQProgramName = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.TabPlanREQTransactionDescription = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemTabPlanREQTransactionDescription = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.TabPlanCol_AssigerCode = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemEmployee_Plan = new DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit();
            this.TabPlanREQAssigeName = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.TabPlanREQTypeSupervisor = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryTabPlanREQTypeSupervisor = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.TabPlanCol_ResponsibleMan = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.TabPlanREQSupervisorName = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.TabPlanREQDueDate = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemTabPlanREQDueDate = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.TabPlanREQDueDateTrue = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemTabPlanREQDueDateTrue = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.TabPlanREQDueDateBigin = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemTabPlanREQDueDateBigin = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.TabPlanREQStatusCustomer = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.TabPlanREQTransactionStatus = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemTabPlanREQTransactionStatus = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.TabPlanREQTransactionConClude = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemTabPlanREQTransactionConClude = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.TabTest = new DevExpress.XtraTab.XtraTabPage();
            this.gridControlTest = new DevExpress.XtraGrid.GridControl();
            this.advBandedGridTest = new DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView();
            this.gridBand3 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.TabTestTransactionDate = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemDateEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.TabTestCustomerCode = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemTabTestCustomerCode = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.TabTestCustomerName = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.TabTestTransactionDescription = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemTransactionDescriptionTest = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.TabTestFolderBackUP = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.TabTestProgramName = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.TabTestVersion = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.TabTestUserTest = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.TestProgramTest = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.TabTestCreateTest = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemCreateTest = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.TabTestPullTest = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemPullTest = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.TabTestADDTest = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemADDTest = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.TabTestSaveTest = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemSaveTest = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.TabTestEditTest = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemEditTest = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.TabTestCancelTest = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemCancelTest = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.TabTestSearchTest = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemSearchTest = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.TabTestPriviewTest = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemPriviewTest = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.TabTestWebUpdate = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemTabTestWebUpdate = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.TabTestUpdate = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemTestUpdate = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.TabApprovedTest = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemApprovedTest = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.TabTestSupportTest = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemTabTestSupportTest = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.TabTestStatusTest = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemStatusTest = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.TabTestOpenFileVersion = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemTabTestOpenFileVersion = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.TabTestResponsibleMan = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemEmployee_TabTest = new DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit();
            this.TabTestSupervisorName = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.TabTestRandomTest = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.TabTestTransactionConClude = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemTestTransactionConClude = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.bandedGridColumn18 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.defaultLookAndFeel1 = new DevExpress.LookAndFeel.DefaultLookAndFeel(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.styleControllerBlue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.styleControllerRed)).BeginInit();
            this.contextMenuStripGrit.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barAndDockingController1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSearch.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueSerch.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateSearchTo.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateSearchTo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlBackLog)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.advBandedGridTaskOfCompany)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryTransactionDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryTransactionDate.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryCustomerCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repository_DocumentType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryDocumentTypeRequiment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryTransactionDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryDueDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryDueDate.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemEmployee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repository_TypeSupervisor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryDueDateBigin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryDueDateBigin.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryDueDateTrue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryDueDateTrue.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repository_TransactionStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTestStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryTransactionConClude)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueDateSearch.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateSearchFrom.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateSearchFrom.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SearchStatus_CheckCombobox.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TabControl)).BeginInit();
            this.TabControl.SuspendLayout();
            this.TabBacklog.SuspendLayout();
            this.TabPlanREQ.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlPlanREQ)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.advBandedGridPlanREQ)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTabPlanREQTransactionDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTabPlanREQTransactionDate.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTabPlanREQCustomerCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTabPlanREQDocumentType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTabPlanREQDocumentTypeRequiment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTabPlanREQTransactionDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemEmployee_Plan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryTabPlanREQTypeSupervisor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTabPlanREQDueDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTabPlanREQDueDate.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTabPlanREQDueDateTrue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTabPlanREQDueDateTrue.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTabPlanREQDueDateBigin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTabPlanREQDueDateBigin.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTabPlanREQTransactionStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTabPlanREQTransactionConClude)).BeginInit();
            this.TabTest.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlTest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.advBandedGridTest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit5.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTabTestCustomerCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTransactionDescriptionTest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCreateTest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPullTest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemADDTest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSaveTest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemEditTest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCancelTest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSearchTest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPriviewTest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTabTestWebUpdate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTestUpdate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemApprovedTest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTabTestSupportTest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemStatusTest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTabTestOpenFileVersion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemEmployee_TabTest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTestTransactionConClude)).BeginInit();
            this.SuspendLayout();
            // 
            // styleControllerBlue
            // 
            this.styleControllerBlue.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.styleControllerBlue.Appearance.Options.UseForeColor = true;
            // 
            // styleControllerRed
            // 
            this.styleControllerRed.Appearance.ForeColor = System.Drawing.Color.Red;
            this.styleControllerRed.Appearance.Options.UseForeColor = true;
            // 
            // contextMenuStripGrit
            // 
            this.contextMenuStripGrit.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.contextMenuStripGrit.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.StripMenuAdd,
            this.StripMenuCancel,
            this.StripMenuReCancel,
            this.StripMenuColor,
            this.StripMenuCopy,
            this.StripMenuPaste});
            this.contextMenuStripGrit.Name = "contextMenuStripGrit";
            this.contextMenuStripGrit.Size = new System.Drawing.Size(182, 141);
            // 
            // StripMenuAdd
            // 
            this.StripMenuAdd.Name = "StripMenuAdd";
            this.StripMenuAdd.Size = new System.Drawing.Size(181, 22);
            this.StripMenuAdd.Text = "เพิ่ม";
            // 
            // StripMenuCancel
            // 
            this.StripMenuCancel.Name = "StripMenuCancel";
            this.StripMenuCancel.Size = new System.Drawing.Size(181, 22);
            this.StripMenuCancel.Text = "ลบ";
            // 
            // StripMenuReCancel
            // 
            this.StripMenuReCancel.Name = "StripMenuReCancel";
            this.StripMenuReCancel.Size = new System.Drawing.Size(181, 22);
            this.StripMenuReCancel.Text = "ยกเลิก ลบ";
            // 
            // StripMenuColor
            // 
            this.StripMenuColor.Items.AddRange(new object[] {
            "A",
            "B",
            "C"});
            this.StripMenuColor.Name = "StripMenuColor";
            this.StripMenuColor.Size = new System.Drawing.Size(121, 23);
            this.StripMenuColor.Text = "เปลี่ยนสี";
            this.StripMenuColor.Visible = false;
            // 
            // StripMenuCopy
            // 
            this.StripMenuCopy.Name = "StripMenuCopy";
            this.StripMenuCopy.Size = new System.Drawing.Size(181, 22);
            this.StripMenuCopy.Text = "คัดลอก";
            // 
            // StripMenuPaste
            // 
            this.StripMenuPaste.Name = "StripMenuPaste";
            this.StripMenuPaste.Size = new System.Drawing.Size(181, 22);
            this.StripMenuPaste.Text = "วาง";
            this.StripMenuPaste.Click += new System.EventHandler(this.StripMenuPaste_Click);
            // 
            // barManager1
            // 
            this.barManager1.AllowMoveBarOnToolbar = false;
            this.barManager1.AllowQuickCustomization = false;
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar2});
            this.barManager1.Controller = this.barAndDockingController1;
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Images = this.imageCollection1;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barSubItem1,
            this.btnbarEdit,
            this.btnbarSave,
            this.btnbarUndo,
            this.barEdit,
            this.BarUndo,
            this.barSave,
            this.barButtonItem1,
            this.barPlan,
            this.barLineNotify,
            this.barExport,
            this.barMail,
            this.barButtonItem2,
            this.barSubItem2,
            this.barConfigPlan});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 17;
            // 
            // bar1
            // 
            this.bar1.BarName = "Tools";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 1;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barEdit),
            new DevExpress.XtraBars.LinkPersistInfo(this.barSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.BarUndo),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.barPlan),
            new DevExpress.XtraBars.LinkPersistInfo(this.barLineNotify),
            new DevExpress.XtraBars.LinkPersistInfo(this.barMail),
            new DevExpress.XtraBars.LinkPersistInfo(this.barExport)});
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Tools";
            // 
            // barEdit
            // 
            this.barEdit.Caption = "Edit";
            this.barEdit.Id = 6;
            this.barEdit.ImageOptions.ImageIndex = 0;
            this.barEdit.Name = "barEdit";
            // 
            // barSave
            // 
            this.barSave.Caption = "Save";
            this.barSave.Id = 8;
            this.barSave.ImageOptions.ImageIndex = 1;
            this.barSave.Name = "barSave";
            // 
            // BarUndo
            // 
            this.BarUndo.Caption = "Undo";
            this.BarUndo.Id = 7;
            this.BarUndo.ImageOptions.ImageIndex = 2;
            this.BarUndo.Name = "BarUndo";
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "Preview";
            this.barButtonItem1.Id = 9;
            this.barButtonItem1.ImageOptions.ImageIndex = 3;
            this.barButtonItem1.Name = "barButtonItem1";
            // 
            // barPlan
            // 
            this.barPlan.Caption = "Plan";
            this.barPlan.Id = 10;
            this.barPlan.ImageOptions.ImageIndex = 4;
            this.barPlan.Name = "barPlan";
            // 
            // barLineNotify
            // 
            this.barLineNotify.Caption = "LineNotify";
            this.barLineNotify.Id = 11;
            this.barLineNotify.ImageOptions.ImageIndex = 6;
            this.barLineNotify.Name = "barLineNotify";
            // 
            // barMail
            // 
            this.barMail.Caption = "Mail";
            this.barMail.Id = 13;
            this.barMail.ImageIndexDisabled = 9;
            this.barMail.ImageOptions.DisabledImageIndex = 9;
            this.barMail.ImageOptions.ImageIndex = 10;
            this.barMail.Name = "barMail";
            toolTipTitleItem1.Text = "แจ้งเตือน Mail";
            superToolTip1.Items.Add(toolTipTitleItem1);
            this.barMail.SuperTip = superToolTip1;
            // 
            // barExport
            // 
            this.barExport.Caption = "ExportExcel";
            this.barExport.Id = 12;
            this.barExport.ImageOptions.ImageIndex = 8;
            this.barExport.Name = "barExport";
            toolTipTitleItem2.Text = "Export Excel";
            superToolTip2.Items.Add(toolTipTitleItem2);
            this.barExport.SuperTip = superToolTip2;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem2)});
            this.bar2.OptionsBar.AllowQuickCustomization = false;
            this.bar2.OptionsBar.DrawDragBorder = false;
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // barSubItem1
            // 
            this.barSubItem1.Caption = "Menu";
            this.barSubItem1.Id = 0;
            this.barSubItem1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnbarEdit),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnbarSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnbarUndo)});
            this.barSubItem1.Name = "barSubItem1";
            // 
            // btnbarEdit
            // 
            this.btnbarEdit.Caption = "Edit";
            this.btnbarEdit.Id = 2;
            this.btnbarEdit.Name = "btnbarEdit";
            // 
            // btnbarSave
            // 
            this.btnbarSave.Caption = "Save";
            this.btnbarSave.Id = 3;
            this.btnbarSave.Name = "btnbarSave";
            // 
            // btnbarUndo
            // 
            this.btnbarUndo.Caption = "Undo";
            this.btnbarUndo.Id = 4;
            this.btnbarUndo.Name = "btnbarUndo";
            // 
            // barSubItem2
            // 
            this.barSubItem2.Caption = "Option";
            this.barSubItem2.Id = 15;
            this.barSubItem2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barConfigPlan)});
            this.barSubItem2.Name = "barSubItem2";
            // 
            // barConfigPlan
            // 
            this.barConfigPlan.Caption = "Config Plan";
            this.barConfigPlan.Id = 16;
            this.barConfigPlan.Name = "barConfigPlan";
            // 
            // barAndDockingController1
            // 
            this.barAndDockingController1.PropertiesBar.AllowLinkLighting = false;
            this.barAndDockingController1.PropertiesDocking.ViewStyle = DevExpress.XtraBars.Docking2010.Views.DockingViewStyle.Classic;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(1284, 62);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 661);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(1284, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 62);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 599);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1284, 62);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 599);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageSize = new System.Drawing.Size(25, 25);
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "ConfigPlan";
            this.barButtonItem2.Id = 14;
            this.barButtonItem2.Name = "barButtonItem2";
            // 
            // txtSearch
            // 
            this.txtSearch.Location = new System.Drawing.Point(170, 7);
            this.txtSearch.MenuManager = this.barManager1;
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(200, 20);
            this.txtSearch.TabIndex = 0;
            // 
            // lueSerch
            // 
            this.lueSerch.Location = new System.Drawing.Point(45, 7);
            this.lueSerch.MenuManager = this.barManager1;
            this.lueSerch.Name = "lueSerch";
            this.lueSerch.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueSerch.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Desc", "Desc")});
            this.lueSerch.Properties.NullText = "";
            this.lueSerch.Size = new System.Drawing.Size(120, 20);
            this.lueSerch.TabIndex = 1;
            // 
            // dateSearchTo
            // 
            this.dateSearchTo.EditValue = null;
            this.dateSearchTo.Location = new System.Drawing.Point(827, 7);
            this.dateSearchTo.MenuManager = this.barManager1;
            this.dateSearchTo.Name = "dateSearchTo";
            this.dateSearchTo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateSearchTo.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateSearchTo.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dateSearchTo.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateSearchTo.Size = new System.Drawing.Size(100, 20);
            this.dateSearchTo.TabIndex = 4;
            // 
            // gridControlBackLog
            // 
            this.gridControlBackLog.ContextMenuStrip = this.contextMenuStripGrit;
            this.gridControlBackLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlBackLog.Location = new System.Drawing.Point(0, 0);
            this.gridControlBackLog.MainView = this.advBandedGridTaskOfCompany;
            this.gridControlBackLog.Name = "gridControlBackLog";
            this.gridControlBackLog.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryTransactionDate,
            this.repositoryCustomerCode,
            this.repository_DocumentType,
            this.repositoryDocumentTypeRequiment,
            this.repositoryTransactionDescription,
            this.repositoryDueDate,
            this.repositoryDueDateTrue,
            this.repositoryTransactionConClude,
            this.repository_TypeSupervisor,
            this.repository_TransactionStatus,
            this.repositoryDueDateBigin,
            this.repositoryItemEmployee,
            this.repositoryItemTestStatus});
            this.gridControlBackLog.Size = new System.Drawing.Size(1272, 527);
            this.gridControlBackLog.TabIndex = 0;
            this.gridControlBackLog.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.advBandedGridTaskOfCompany});
            // 
            // advBandedGridTaskOfCompany
            // 
            this.advBandedGridTaskOfCompany.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.advBandedGridTaskOfCompany.Appearance.FixedLine.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.advBandedGridTaskOfCompany.Appearance.FixedLine.Options.UseBackColor = true;
            this.advBandedGridTaskOfCompany.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Transparent;
            this.advBandedGridTaskOfCompany.Appearance.FocusedRow.Options.UseForeColor = true;
            this.advBandedGridTaskOfCompany.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.advBandedGridTaskOfCompany.Appearance.HeaderPanel.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.advBandedGridTaskOfCompany.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.advBandedGridTaskOfCompany.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.advBandedGridTaskOfCompany.Appearance.HeaderPanel.Options.UseFont = true;
            this.advBandedGridTaskOfCompany.Appearance.HeaderPanelBackground.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.advBandedGridTaskOfCompany.Appearance.HeaderPanelBackground.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.advBandedGridTaskOfCompany.Appearance.HeaderPanelBackground.Options.UseBackColor = true;
            this.advBandedGridTaskOfCompany.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.advBandedGridTaskOfCompany.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.advBandedGridTaskOfCompany.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.advBandedGridTaskOfCompany.Appearance.SelectedRow.Options.UseForeColor = true;
            this.advBandedGridTaskOfCompany.AppearancePrint.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.advBandedGridTaskOfCompany.AppearancePrint.HeaderPanel.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.advBandedGridTaskOfCompany.AppearancePrint.HeaderPanel.Options.UseBackColor = true;
            this.advBandedGridTaskOfCompany.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBandTask01,
            this.gridBand1,
            this.gridBand4,
            this.gridBand5});
            this.advBandedGridTaskOfCompany.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.advBandedGridTaskOfCompany.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.Col_NoLine,
            this.Col_TransactionDate,
            this.Col_CustomerCode,
            this.Col_CustomerName,
            this.Col_StatusCustomer,
            this.Col_DocumentType,
            this.Col_DocumentTypeRequiment,
            this.Col_ProgramName,
            this.Col_TransactionDescription,
            this.Col_CustomerDueDate,
            this.Col_AssigerCode,
            this.Col_TypeSupervisor,
            this.Col_ResponsibleMan,
            this.Col_DueDateBigin,
            this.Col_DueDate,
            this.Col_DueDateTrue,
            this.Col_TransactionStatus,
            this.Col_Tester,
            this.Col_TestDueDate,
            this.Col_TestStatus,
            this.Col_TransactionConClude,
            this.Col_AssigerName,
            this.Col_SupervisorName});
            this.advBandedGridTaskOfCompany.CustomizationFormBounds = new System.Drawing.Rectangle(630, 286, 295, 307);
            this.advBandedGridTaskOfCompany.DetailHeight = 227;
            this.advBandedGridTaskOfCompany.FixedLineWidth = 1;
            this.advBandedGridTaskOfCompany.GridControl = this.gridControlBackLog;
            this.advBandedGridTaskOfCompany.Name = "advBandedGridTaskOfCompany";
            this.advBandedGridTaskOfCompany.OptionsSelection.MultiSelect = true;
            this.advBandedGridTaskOfCompany.OptionsView.ShowGroupPanel = false;
            this.advBandedGridTaskOfCompany.CustomRowFilter += new DevExpress.XtraGrid.Views.Base.RowFilterEventHandler(this.advBandedGridTaskOfCompany_CustomRowFilter);
            // 
            // gridBandTask01
            // 
            this.gridBandTask01.AppearanceHeader.ForeColor = System.Drawing.Color.Blue;
            this.gridBandTask01.AppearanceHeader.Options.UseForeColor = true;
            this.gridBandTask01.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBandTask01.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBandTask01.Caption = "Task";
            this.gridBandTask01.Columns.Add(this.Col_NoLine);
            this.gridBandTask01.Columns.Add(this.Col_TransactionDate);
            this.gridBandTask01.Columns.Add(this.Col_CustomerCode);
            this.gridBandTask01.Columns.Add(this.Col_CustomerName);
            this.gridBandTask01.Columns.Add(this.Col_DocumentType);
            this.gridBandTask01.Columns.Add(this.Col_DocumentTypeRequiment);
            this.gridBandTask01.Columns.Add(this.Col_ProgramName);
            this.gridBandTask01.Columns.Add(this.Col_StatusCustomer);
            this.gridBandTask01.Columns.Add(this.Col_TransactionDescription);
            this.gridBandTask01.Columns.Add(this.Col_CustomerDueDate);
            this.gridBandTask01.Columns.Add(this.Col_AssigerCode);
            this.gridBandTask01.Columns.Add(this.Col_AssigerName);
            this.gridBandTask01.Columns.Add(this.Col_SupervisorName);
            this.gridBandTask01.Name = "gridBandTask01";
            this.gridBandTask01.VisibleIndex = 0;
            this.gridBandTask01.Width = 1130;
            // 
            // Col_NoLine
            // 
            this.Col_NoLine.AppearanceHeader.Options.UseTextOptions = true;
            this.Col_NoLine.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Col_NoLine.Caption = "Case ID";
            this.Col_NoLine.FieldName = "NoItem";
            this.Col_NoLine.Name = "Col_NoLine";
            this.Col_NoLine.OptionsColumn.AllowEdit = false;
            this.Col_NoLine.OptionsFilter.ImmediateUpdatePopupDateFilterOnCheck = DevExpress.Utils.DefaultBoolean.True;
            this.Col_NoLine.Visible = true;
            this.Col_NoLine.Width = 50;
            // 
            // Col_TransactionDate
            // 
            this.Col_TransactionDate.AppearanceCell.Options.UseTextOptions = true;
            this.Col_TransactionDate.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Col_TransactionDate.AppearanceHeader.Options.UseTextOptions = true;
            this.Col_TransactionDate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Col_TransactionDate.Caption = "วันที่";
            this.Col_TransactionDate.ColumnEdit = this.repositoryTransactionDate;
            this.Col_TransactionDate.FieldName = "TransactionDate";
            this.Col_TransactionDate.Name = "Col_TransactionDate";
            this.Col_TransactionDate.Visible = true;
            this.Col_TransactionDate.Width = 80;
            // 
            // repositoryTransactionDate
            // 
            this.repositoryTransactionDate.AutoHeight = false;
            this.repositoryTransactionDate.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryTransactionDate.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryTransactionDate.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.repositoryTransactionDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryTransactionDate.EditFormat.FormatString = "dd/MM/yyyy";
            this.repositoryTransactionDate.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryTransactionDate.Mask.EditMask = "dd/MM/yyyy";
            this.repositoryTransactionDate.Name = "repositoryTransactionDate";
            // 
            // Col_CustomerCode
            // 
            this.Col_CustomerCode.AppearanceHeader.Options.UseTextOptions = true;
            this.Col_CustomerCode.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Col_CustomerCode.Caption = "รหัสลูกค้า";
            this.Col_CustomerCode.ColumnEdit = this.repositoryCustomerCode;
            this.Col_CustomerCode.FieldName = "CustomerCode";
            this.Col_CustomerCode.Name = "Col_CustomerCode";
            this.Col_CustomerCode.Visible = true;
            this.Col_CustomerCode.Width = 90;
            // 
            // repositoryCustomerCode
            // 
            this.repositoryCustomerCode.AutoHeight = false;
            this.repositoryCustomerCode.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryCustomerCode.Name = "repositoryCustomerCode";
            // 
            // Col_CustomerName
            // 
            this.Col_CustomerName.AppearanceHeader.Options.UseTextOptions = true;
            this.Col_CustomerName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Col_CustomerName.Caption = "ชื่อลูกค้า";
            this.Col_CustomerName.FieldName = "CustomerName";
            this.Col_CustomerName.Name = "Col_CustomerName";
            this.Col_CustomerName.Visible = true;
            this.Col_CustomerName.Width = 90;
            // 
            // Col_DocumentType
            // 
            this.Col_DocumentType.AppearanceHeader.Options.UseTextOptions = true;
            this.Col_DocumentType.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Col_DocumentType.Caption = "ประเภทรายการ";
            this.Col_DocumentType.ColumnEdit = this.repository_DocumentType;
            this.Col_DocumentType.FieldName = "DocumentType";
            this.Col_DocumentType.Name = "Col_DocumentType";
            this.Col_DocumentType.Visible = true;
            this.Col_DocumentType.Width = 80;
            // 
            // repository_DocumentType
            // 
            this.repository_DocumentType.AutoHeight = false;
            this.repository_DocumentType.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repository_DocumentType.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Desc", "Desc")});
            this.repository_DocumentType.Name = "repository_DocumentType";
            this.repository_DocumentType.NullText = "";
            // 
            // Col_DocumentTypeRequiment
            // 
            this.Col_DocumentTypeRequiment.AppearanceHeader.Options.UseTextOptions = true;
            this.Col_DocumentTypeRequiment.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Col_DocumentTypeRequiment.Caption = "ประเภท Requiment";
            this.Col_DocumentTypeRequiment.ColumnEdit = this.repositoryDocumentTypeRequiment;
            this.Col_DocumentTypeRequiment.FieldName = "DocumentTypeRequiment";
            this.Col_DocumentTypeRequiment.Name = "Col_DocumentTypeRequiment";
            this.Col_DocumentTypeRequiment.Visible = true;
            this.Col_DocumentTypeRequiment.Width = 100;
            // 
            // repositoryDocumentTypeRequiment
            // 
            this.repositoryDocumentTypeRequiment.AutoHeight = false;
            this.repositoryDocumentTypeRequiment.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryDocumentTypeRequiment.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Desc", "Desc")});
            this.repositoryDocumentTypeRequiment.Name = "repositoryDocumentTypeRequiment";
            this.repositoryDocumentTypeRequiment.NullText = "";
            // 
            // Col_ProgramName
            // 
            this.Col_ProgramName.AppearanceHeader.Options.UseTextOptions = true;
            this.Col_ProgramName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Col_ProgramName.Caption = "โปรแกรม";
            this.Col_ProgramName.FieldName = "ProgramName";
            this.Col_ProgramName.Name = "Col_ProgramName";
            this.Col_ProgramName.Visible = true;
            this.Col_ProgramName.Width = 80;
            // 
            // Col_StatusCustomer
            // 
            this.Col_StatusCustomer.AppearanceHeader.ForeColor = System.Drawing.Color.Red;
            this.Col_StatusCustomer.AppearanceHeader.Options.UseForeColor = true;
            this.Col_StatusCustomer.AppearanceHeader.Options.UseTextOptions = true;
            this.Col_StatusCustomer.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Col_StatusCustomer.Caption = "ประเภทงาน";
            this.Col_StatusCustomer.FieldName = "StatusCustomer";
            this.Col_StatusCustomer.Name = "Col_StatusCustomer";
            this.Col_StatusCustomer.Visible = true;
            this.Col_StatusCustomer.Width = 80;
            // 
            // Col_TransactionDescription
            // 
            this.Col_TransactionDescription.AppearanceHeader.ForeColor = System.Drawing.Color.Red;
            this.Col_TransactionDescription.AppearanceHeader.Options.UseForeColor = true;
            this.Col_TransactionDescription.AppearanceHeader.Options.UseTextOptions = true;
            this.Col_TransactionDescription.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Col_TransactionDescription.Caption = "รายละเอียด";
            this.Col_TransactionDescription.ColumnEdit = this.repositoryTransactionDescription;
            this.Col_TransactionDescription.FieldName = "TransactionDescription";
            this.Col_TransactionDescription.MinWidth = 300;
            this.Col_TransactionDescription.Name = "Col_TransactionDescription";
            this.Col_TransactionDescription.Visible = true;
            this.Col_TransactionDescription.Width = 300;
            // 
            // repositoryTransactionDescription
            // 
            this.repositoryTransactionDescription.AutoHeight = false;
            this.repositoryTransactionDescription.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryTransactionDescription.Name = "repositoryTransactionDescription";
            this.repositoryTransactionDescription.ShowIcon = false;
            // 
            // Col_CustomerDueDate
            // 
            this.Col_CustomerDueDate.AppearanceCell.Options.UseTextOptions = true;
            this.Col_CustomerDueDate.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Col_CustomerDueDate.AppearanceHeader.ForeColor = System.Drawing.Color.Red;
            this.Col_CustomerDueDate.AppearanceHeader.Options.UseForeColor = true;
            this.Col_CustomerDueDate.AppearanceHeader.Options.UseTextOptions = true;
            this.Col_CustomerDueDate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Col_CustomerDueDate.Caption = "กำหนดส่งลูกค้า";
            this.Col_CustomerDueDate.ColumnEdit = this.repositoryDueDate;
            this.Col_CustomerDueDate.FieldName = "CustomerDueDate";
            this.Col_CustomerDueDate.Name = "Col_CustomerDueDate";
            this.Col_CustomerDueDate.Visible = true;
            this.Col_CustomerDueDate.Width = 80;
            // 
            // repositoryDueDate
            // 
            this.repositoryDueDate.AutoHeight = false;
            this.repositoryDueDate.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryDueDate.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryDueDate.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.repositoryDueDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryDueDate.EditFormat.FormatString = "dd/MM/yyyy";
            this.repositoryDueDate.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryDueDate.Mask.EditMask = "dd/MM/yyyy";
            this.repositoryDueDate.Name = "repositoryDueDate";
            // 
            // Col_AssigerCode
            // 
            this.Col_AssigerCode.AppearanceHeader.Options.UseTextOptions = true;
            this.Col_AssigerCode.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Col_AssigerCode.Caption = "ผู้มอบหมาย";
            this.Col_AssigerCode.ColumnEdit = this.repositoryItemEmployee;
            this.Col_AssigerCode.FieldName = "AssigerCode";
            this.Col_AssigerCode.Name = "Col_AssigerCode";
            this.Col_AssigerCode.Visible = true;
            this.Col_AssigerCode.Width = 100;
            // 
            // repositoryItemEmployee
            // 
            this.repositoryItemEmployee.AutoHeight = false;
            this.repositoryItemEmployee.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemEmployee.Name = "repositoryItemEmployee";
            // 
            // Col_AssigerName
            // 
            this.Col_AssigerName.AppearanceHeader.Options.UseTextOptions = true;
            this.Col_AssigerName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Col_AssigerName.Caption = "ผู้มอบหมาย";
            this.Col_AssigerName.FieldName = "AssigerName";
            this.Col_AssigerName.Name = "Col_AssigerName";
            this.Col_AssigerName.Width = 100;
            // 
            // Col_SupervisorName
            // 
            this.Col_SupervisorName.AppearanceHeader.Options.UseTextOptions = true;
            this.Col_SupervisorName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Col_SupervisorName.Caption = "ชื่อผู้รับผิดชอบ";
            this.Col_SupervisorName.FieldName = "SupervisorName";
            this.Col_SupervisorName.Name = "Col_SupervisorName";
            this.Col_SupervisorName.Width = 100;
            // 
            // gridBand1
            // 
            this.gridBand1.AppearanceHeader.ForeColor = System.Drawing.Color.Blue;
            this.gridBand1.AppearanceHeader.Options.UseForeColor = true;
            this.gridBand1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand1.Caption = "Programmer/Support/Sales";
            this.gridBand1.Columns.Add(this.Col_TypeSupervisor);
            this.gridBand1.Columns.Add(this.Col_ResponsibleMan);
            this.gridBand1.Columns.Add(this.Col_DueDateBigin);
            this.gridBand1.Columns.Add(this.Col_DueDate);
            this.gridBand1.Columns.Add(this.Col_DueDateTrue);
            this.gridBand1.Columns.Add(this.Col_TransactionStatus);
            this.gridBand1.Name = "gridBand1";
            this.gridBand1.VisibleIndex = 1;
            this.gridBand1.Width = 565;
            // 
            // Col_TypeSupervisor
            // 
            this.Col_TypeSupervisor.AppearanceHeader.Options.UseTextOptions = true;
            this.Col_TypeSupervisor.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Col_TypeSupervisor.Caption = "แผนกที่รับผิดชอบ";
            this.Col_TypeSupervisor.ColumnEdit = this.repository_TypeSupervisor;
            this.Col_TypeSupervisor.FieldName = "TypeSupervisor";
            this.Col_TypeSupervisor.Name = "Col_TypeSupervisor";
            this.Col_TypeSupervisor.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.Col_TypeSupervisor.Visible = true;
            this.Col_TypeSupervisor.Width = 100;
            // 
            // repository_TypeSupervisor
            // 
            this.repository_TypeSupervisor.AutoHeight = false;
            this.repository_TypeSupervisor.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repository_TypeSupervisor.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Desc", "Desc")});
            this.repository_TypeSupervisor.Name = "repository_TypeSupervisor";
            this.repository_TypeSupervisor.NullText = "";
            // 
            // Col_ResponsibleMan
            // 
            this.Col_ResponsibleMan.AppearanceHeader.Options.UseTextOptions = true;
            this.Col_ResponsibleMan.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Col_ResponsibleMan.Caption = "ผู้รับผิดชอบ";
            this.Col_ResponsibleMan.ColumnEdit = this.repositoryItemEmployee;
            this.Col_ResponsibleMan.FieldName = "ResponsibleMan";
            this.Col_ResponsibleMan.Name = "Col_ResponsibleMan";
            this.Col_ResponsibleMan.Visible = true;
            this.Col_ResponsibleMan.Width = 100;
            // 
            // Col_DueDateBigin
            // 
            this.Col_DueDateBigin.AppearanceCell.Options.UseTextOptions = true;
            this.Col_DueDateBigin.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Col_DueDateBigin.AppearanceHeader.Options.UseTextOptions = true;
            this.Col_DueDateBigin.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Col_DueDateBigin.Caption = "วันที่กำหนดส่งแรก";
            this.Col_DueDateBigin.ColumnEdit = this.repositoryDueDateBigin;
            this.Col_DueDateBigin.FieldName = "DueDateBigin";
            this.Col_DueDateBigin.Name = "Col_DueDateBigin";
            this.Col_DueDateBigin.Visible = true;
            this.Col_DueDateBigin.Width = 95;
            // 
            // repositoryDueDateBigin
            // 
            this.repositoryDueDateBigin.AutoHeight = false;
            this.repositoryDueDateBigin.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryDueDateBigin.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryDueDateBigin.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.repositoryDueDateBigin.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryDueDateBigin.EditFormat.FormatString = "dd/MM/yyyy";
            this.repositoryDueDateBigin.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryDueDateBigin.Mask.EditMask = "dd/MM/yyyy";
            this.repositoryDueDateBigin.Name = "repositoryDueDateBigin";
            // 
            // Col_DueDate
            // 
            this.Col_DueDate.AppearanceCell.Options.UseTextOptions = true;
            this.Col_DueDate.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Col_DueDate.AppearanceHeader.ForeColor = System.Drawing.Color.Red;
            this.Col_DueDate.AppearanceHeader.Options.UseForeColor = true;
            this.Col_DueDate.AppearanceHeader.Options.UseTextOptions = true;
            this.Col_DueDate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Col_DueDate.Caption = "วันที่กำหนดส่ง";
            this.Col_DueDate.ColumnEdit = this.repositoryDueDate;
            this.Col_DueDate.FieldName = "DueDate";
            this.Col_DueDate.Name = "Col_DueDate";
            this.Col_DueDate.Visible = true;
            // 
            // Col_DueDateTrue
            // 
            this.Col_DueDateTrue.AppearanceCell.Options.UseTextOptions = true;
            this.Col_DueDateTrue.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Col_DueDateTrue.AppearanceHeader.Options.UseTextOptions = true;
            this.Col_DueDateTrue.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Col_DueDateTrue.Caption = "วันที่ส่งจริง";
            this.Col_DueDateTrue.ColumnEdit = this.repositoryDueDateTrue;
            this.Col_DueDateTrue.FieldName = "DueDateTrue";
            this.Col_DueDateTrue.Name = "Col_DueDateTrue";
            this.Col_DueDateTrue.Visible = true;
            // 
            // repositoryDueDateTrue
            // 
            this.repositoryDueDateTrue.AutoHeight = false;
            this.repositoryDueDateTrue.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryDueDateTrue.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryDueDateTrue.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.repositoryDueDateTrue.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryDueDateTrue.EditFormat.FormatString = "dd/MM/yyyy";
            this.repositoryDueDateTrue.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryDueDateTrue.Mask.EditMask = "dd/MM/yyyy";
            this.repositoryDueDateTrue.Name = "repositoryDueDateTrue";
            // 
            // Col_TransactionStatus
            // 
            this.Col_TransactionStatus.AppearanceHeader.ForeColor = System.Drawing.Color.Red;
            this.Col_TransactionStatus.AppearanceHeader.Options.UseForeColor = true;
            this.Col_TransactionStatus.AppearanceHeader.Options.UseTextOptions = true;
            this.Col_TransactionStatus.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Col_TransactionStatus.Caption = "สถานะงาน";
            this.Col_TransactionStatus.ColumnEdit = this.repository_TransactionStatus;
            this.Col_TransactionStatus.FieldName = "TransactionStatus";
            this.Col_TransactionStatus.Name = "Col_TransactionStatus";
            this.Col_TransactionStatus.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.Col_TransactionStatus.Visible = true;
            this.Col_TransactionStatus.Width = 120;
            // 
            // repository_TransactionStatus
            // 
            this.repository_TransactionStatus.AutoHeight = false;
            this.repository_TransactionStatus.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repository_TransactionStatus.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Desc", "Desc")});
            this.repository_TransactionStatus.Name = "repository_TransactionStatus";
            this.repository_TransactionStatus.NullText = "";
            // 
            // gridBand4
            // 
            this.gridBand4.AppearanceHeader.ForeColor = System.Drawing.Color.Blue;
            this.gridBand4.AppearanceHeader.Options.UseForeColor = true;
            this.gridBand4.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand4.Caption = "Tester";
            this.gridBand4.Columns.Add(this.Col_Tester);
            this.gridBand4.Columns.Add(this.Col_TestDueDate);
            this.gridBand4.Columns.Add(this.Col_TestStatus);
            this.gridBand4.Name = "gridBand4";
            this.gridBand4.VisibleIndex = 2;
            this.gridBand4.Width = 280;
            // 
            // Col_Tester
            // 
            this.Col_Tester.AppearanceHeader.Options.UseTextOptions = true;
            this.Col_Tester.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Col_Tester.Caption = "ผู้ทดสอบ";
            this.Col_Tester.ColumnEdit = this.repositoryItemEmployee;
            this.Col_Tester.FieldName = "Tester";
            this.Col_Tester.Name = "Col_Tester";
            this.Col_Tester.Visible = true;
            this.Col_Tester.Width = 100;
            // 
            // Col_TestDueDate
            // 
            this.Col_TestDueDate.AppearanceCell.Options.UseTextOptions = true;
            this.Col_TestDueDate.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Col_TestDueDate.AppearanceHeader.ForeColor = System.Drawing.Color.Red;
            this.Col_TestDueDate.AppearanceHeader.Options.UseForeColor = true;
            this.Col_TestDueDate.AppearanceHeader.Options.UseTextOptions = true;
            this.Col_TestDueDate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Col_TestDueDate.Caption = "กำหนดทดสอบ";
            this.Col_TestDueDate.ColumnEdit = this.repositoryDueDate;
            this.Col_TestDueDate.FieldName = "TestDueDate";
            this.Col_TestDueDate.Name = "Col_TestDueDate";
            this.Col_TestDueDate.Visible = true;
            this.Col_TestDueDate.Width = 80;
            // 
            // Col_TestStatus
            // 
            this.Col_TestStatus.AppearanceHeader.ForeColor = System.Drawing.Color.Red;
            this.Col_TestStatus.AppearanceHeader.Options.UseForeColor = true;
            this.Col_TestStatus.AppearanceHeader.Options.UseTextOptions = true;
            this.Col_TestStatus.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Col_TestStatus.Caption = "สถานะทดสอบ";
            this.Col_TestStatus.ColumnEdit = this.repositoryItemTestStatus;
            this.Col_TestStatus.FieldName = "TestStatus";
            this.Col_TestStatus.Name = "Col_TestStatus";
            this.Col_TestStatus.Visible = true;
            this.Col_TestStatus.Width = 100;
            // 
            // repositoryItemTestStatus
            // 
            this.repositoryItemTestStatus.AutoHeight = false;
            this.repositoryItemTestStatus.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemTestStatus.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Desc", "Desc")});
            this.repositoryItemTestStatus.Name = "repositoryItemTestStatus";
            this.repositoryItemTestStatus.NullText = "";
            // 
            // gridBand5
            // 
            this.gridBand5.Columns.Add(this.Col_TransactionConClude);
            this.gridBand5.Name = "gridBand5";
            this.gridBand5.VisibleIndex = 3;
            this.gridBand5.Width = 300;
            // 
            // Col_TransactionConClude
            // 
            this.Col_TransactionConClude.AppearanceHeader.Options.UseTextOptions = true;
            this.Col_TransactionConClude.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Col_TransactionConClude.Caption = "หมายเหตุ";
            this.Col_TransactionConClude.ColumnEdit = this.repositoryTransactionConClude;
            this.Col_TransactionConClude.FieldName = "TransactionConClude";
            this.Col_TransactionConClude.MinWidth = 300;
            this.Col_TransactionConClude.Name = "Col_TransactionConClude";
            this.Col_TransactionConClude.Visible = true;
            this.Col_TransactionConClude.Width = 300;
            // 
            // repositoryTransactionConClude
            // 
            this.repositoryTransactionConClude.AutoHeight = false;
            this.repositoryTransactionConClude.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryTransactionConClude.Name = "repositoryTransactionConClude";
            this.repositoryTransactionConClude.ShowIcon = false;
            // 
            // lueDateSearch
            // 
            this.lueDateSearch.Location = new System.Drawing.Point(570, 7);
            this.lueDateSearch.Name = "lueDateSearch";
            this.lueDateSearch.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueDateSearch.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Desc", "Desc")});
            this.lueDateSearch.Properties.NullText = "";
            this.lueDateSearch.Size = new System.Drawing.Size(100, 20);
            this.lueDateSearch.TabIndex = 1;
            // 
            // dateSearchFrom
            // 
            this.dateSearchFrom.EditValue = null;
            this.dateSearchFrom.Location = new System.Drawing.Point(701, 7);
            this.dateSearchFrom.Name = "dateSearchFrom";
            this.dateSearchFrom.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateSearchFrom.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateSearchFrom.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dateSearchFrom.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateSearchFrom.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dateSearchFrom.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateSearchFrom.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dateSearchFrom.Size = new System.Drawing.Size(100, 20);
            this.dateSearchFrom.TabIndex = 4;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.labelControl3);
            this.panelControl1.Controls.Add(this.SearchStatus_CheckCombobox);
            this.panelControl1.Controls.Add(this.dateSearchTo);
            this.panelControl1.Controls.Add(this.dateSearchFrom);
            this.panelControl1.Controls.Add(this.lueDateSearch);
            this.panelControl1.Controls.Add(this.lueSerch);
            this.panelControl1.Controls.Add(this.btnSearch);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.labelControl4);
            this.panelControl1.Controls.Add(this.TabControl);
            this.panelControl1.Controls.Add(this.txtSearch);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 62);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1284, 599);
            this.panelControl1.TabIndex = 5;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(380, 10);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(30, 13);
            this.labelControl3.TabIndex = 7;
            this.labelControl3.Text = "สถานะ";
            // 
            // SearchStatus_CheckCombobox
            // 
            this.SearchStatus_CheckCombobox.EditValue = "";
            this.SearchStatus_CheckCombobox.Location = new System.Drawing.Point(419, 7);
            this.SearchStatus_CheckCombobox.MenuManager = this.barManager1;
            this.SearchStatus_CheckCombobox.Name = "SearchStatus_CheckCombobox";
            this.SearchStatus_CheckCombobox.Properties.AllowMultiSelect = true;
            this.SearchStatus_CheckCombobox.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SearchStatus_CheckCombobox.Size = new System.Drawing.Size(120, 20);
            this.SearchStatus_CheckCombobox.TabIndex = 6;
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(944, 5);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(75, 23);
            this.btnSearch.TabIndex = 5;
            this.btnSearch.Text = "ค้นหา";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(810, 10);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(11, 13);
            this.labelControl2.TabIndex = 4;
            this.labelControl2.Text = "ถึง";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(675, 10);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(20, 13);
            this.labelControl1.TabIndex = 3;
            this.labelControl1.Text = "วันที่";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(10, 10);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(26, 13);
            this.labelControl4.TabIndex = 2;
            this.labelControl4.Text = "ค้นหา";
            // 
            // TabControl
            // 
            this.TabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TabControl.Location = new System.Drawing.Point(5, 34);
            this.TabControl.Name = "TabControl";
            this.TabControl.SelectedTabPage = this.TabBacklog;
            this.TabControl.Size = new System.Drawing.Size(1274, 560);
            this.TabControl.TabIndex = 1;
            this.TabControl.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.TabBacklog,
            this.TabPlanREQ,
            this.TabTest});
            // 
            // TabBacklog
            // 
            this.TabBacklog.Controls.Add(this.gridControlBackLog);
            this.TabBacklog.Name = "TabBacklog";
            this.TabBacklog.Size = new System.Drawing.Size(1272, 527);
            this.TabBacklog.Text = "งานค้าง";
            // 
            // TabPlanREQ
            // 
            this.TabPlanREQ.Controls.Add(this.gridControlPlanREQ);
            this.TabPlanREQ.Name = "TabPlanREQ";
            this.TabPlanREQ.Size = new System.Drawing.Size(1272, 527);
            this.TabPlanREQ.Text = "Plan/REQ";
            // 
            // gridControlPlanREQ
            // 
            this.gridControlPlanREQ.ContextMenuStrip = this.contextMenuStripGrit;
            this.gridControlPlanREQ.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlPlanREQ.Location = new System.Drawing.Point(0, 0);
            this.gridControlPlanREQ.MainView = this.advBandedGridPlanREQ;
            this.gridControlPlanREQ.Name = "gridControlPlanREQ";
            this.gridControlPlanREQ.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTabPlanREQTransactionDate,
            this.repositoryItemTabPlanREQCustomerCode,
            this.repositoryItemTabPlanREQDocumentType,
            this.repositoryItemTabPlanREQDocumentTypeRequiment,
            this.repositoryItemTabPlanREQTransactionDescription,
            this.repositoryItemTabPlanREQDueDate,
            this.repositoryItemTabPlanREQDueDateTrue,
            this.repositoryItemTabPlanREQTransactionConClude,
            this.repositoryTabPlanREQTypeSupervisor,
            this.repositoryItemTabPlanREQTransactionStatus,
            this.repositoryItemTabPlanREQDueDateBigin,
            this.repositoryItemEmployee_Plan});
            this.gridControlPlanREQ.Size = new System.Drawing.Size(1272, 527);
            this.gridControlPlanREQ.TabIndex = 1;
            this.gridControlPlanREQ.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.advBandedGridPlanREQ});
            // 
            // advBandedGridPlanREQ
            // 
            this.advBandedGridPlanREQ.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.advBandedGridPlanREQ.Appearance.FocusedRow.Options.UseForeColor = true;
            this.advBandedGridPlanREQ.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.advBandedGridPlanREQ.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.advBandedGridPlanREQ.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.advBandedGridPlanREQ.Appearance.SelectedRow.Options.UseForeColor = true;
            this.advBandedGridPlanREQ.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand2});
            this.advBandedGridPlanREQ.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.TabPlanREQNoLine,
            this.TabPlanREQTransactionDate,
            this.TabPlanREQCustomerCode,
            this.TabPlanREQCustomerName,
            this.TabPlanREQStatusCustomer,
            this.TabPlanREQDocumentType,
            this.TabPlanREQDocumentTypeRequiment,
            this.TabPlanREQProgramName,
            this.TabPlanREQTransactionDescription,
            this.TabPlanREQAssigeName,
            this.TabPlanREQSupervisorName,
            this.TabPlanREQDueDate,
            this.TabPlanREQDueDateTrue,
            this.TabPlanREQTransactionStatus,
            this.TabPlanREQTransactionConClude,
            this.TabPlanREQTypeSupervisor,
            this.TabPlanREQDueDateBigin,
            this.TabPlanCol_AssigerCode,
            this.TabPlanCol_ResponsibleMan});
            this.advBandedGridPlanREQ.CustomizationFormBounds = new System.Drawing.Rectangle(630, 286, 295, 307);
            this.advBandedGridPlanREQ.DetailHeight = 227;
            this.advBandedGridPlanREQ.FixedLineWidth = 1;
            this.advBandedGridPlanREQ.GridControl = this.gridControlPlanREQ;
            this.advBandedGridPlanREQ.Name = "advBandedGridPlanREQ";
            this.advBandedGridPlanREQ.OptionsSelection.MultiSelect = true;
            this.advBandedGridPlanREQ.OptionsView.ShowBands = false;
            this.advBandedGridPlanREQ.OptionsView.ShowGroupPanel = false;
            // 
            // gridBand2
            // 
            this.gridBand2.Caption = "gridBand1";
            this.gridBand2.Columns.Add(this.TabPlanREQNoLine);
            this.gridBand2.Columns.Add(this.TabPlanREQTransactionDate);
            this.gridBand2.Columns.Add(this.TabPlanREQCustomerCode);
            this.gridBand2.Columns.Add(this.TabPlanREQCustomerName);
            this.gridBand2.Columns.Add(this.TabPlanREQDocumentType);
            this.gridBand2.Columns.Add(this.TabPlanREQDocumentTypeRequiment);
            this.gridBand2.Columns.Add(this.TabPlanREQProgramName);
            this.gridBand2.Columns.Add(this.TabPlanREQTransactionDescription);
            this.gridBand2.Columns.Add(this.TabPlanCol_AssigerCode);
            this.gridBand2.Columns.Add(this.TabPlanREQAssigeName);
            this.gridBand2.Columns.Add(this.TabPlanREQTypeSupervisor);
            this.gridBand2.Columns.Add(this.TabPlanCol_ResponsibleMan);
            this.gridBand2.Columns.Add(this.TabPlanREQSupervisorName);
            this.gridBand2.Columns.Add(this.TabPlanREQDueDate);
            this.gridBand2.Columns.Add(this.TabPlanREQDueDateTrue);
            this.gridBand2.Columns.Add(this.TabPlanREQDueDateBigin);
            this.gridBand2.Columns.Add(this.TabPlanREQStatusCustomer);
            this.gridBand2.Columns.Add(this.TabPlanREQTransactionStatus);
            this.gridBand2.Columns.Add(this.TabPlanREQTransactionConClude);
            this.gridBand2.MinWidth = 13;
            this.gridBand2.Name = "gridBand2";
            this.gridBand2.VisibleIndex = 0;
            this.gridBand2.Width = 1940;
            // 
            // TabPlanREQNoLine
            // 
            this.TabPlanREQNoLine.AppearanceHeader.Options.UseTextOptions = true;
            this.TabPlanREQNoLine.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TabPlanREQNoLine.Caption = "ลำดับ";
            this.TabPlanREQNoLine.FieldName = "NoLine";
            this.TabPlanREQNoLine.Name = "TabPlanREQNoLine";
            this.TabPlanREQNoLine.OptionsColumn.ReadOnly = true;
            this.TabPlanREQNoLine.OptionsFilter.ImmediateUpdatePopupDateFilterOnCheck = DevExpress.Utils.DefaultBoolean.True;
            this.TabPlanREQNoLine.Width = 27;
            // 
            // TabPlanREQTransactionDate
            // 
            this.TabPlanREQTransactionDate.AppearanceCell.Options.UseTextOptions = true;
            this.TabPlanREQTransactionDate.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TabPlanREQTransactionDate.AppearanceHeader.Options.UseTextOptions = true;
            this.TabPlanREQTransactionDate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TabPlanREQTransactionDate.Caption = "วันที่";
            this.TabPlanREQTransactionDate.ColumnEdit = this.repositoryItemTabPlanREQTransactionDate;
            this.TabPlanREQTransactionDate.FieldName = "TransactionDate";
            this.TabPlanREQTransactionDate.Name = "TabPlanREQTransactionDate";
            this.TabPlanREQTransactionDate.Visible = true;
            this.TabPlanREQTransactionDate.Width = 80;
            // 
            // repositoryItemTabPlanREQTransactionDate
            // 
            this.repositoryItemTabPlanREQTransactionDate.AutoHeight = false;
            this.repositoryItemTabPlanREQTransactionDate.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemTabPlanREQTransactionDate.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemTabPlanREQTransactionDate.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.repositoryItemTabPlanREQTransactionDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemTabPlanREQTransactionDate.EditFormat.FormatString = "dd/MM/yyyy";
            this.repositoryItemTabPlanREQTransactionDate.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemTabPlanREQTransactionDate.Mask.EditMask = "dd/MM/yyyy";
            this.repositoryItemTabPlanREQTransactionDate.Name = "repositoryItemTabPlanREQTransactionDate";
            // 
            // TabPlanREQCustomerCode
            // 
            this.TabPlanREQCustomerCode.AppearanceHeader.Options.UseTextOptions = true;
            this.TabPlanREQCustomerCode.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TabPlanREQCustomerCode.Caption = "รหัสลูกค้า";
            this.TabPlanREQCustomerCode.ColumnEdit = this.repositoryItemTabPlanREQCustomerCode;
            this.TabPlanREQCustomerCode.FieldName = "CustomerCode";
            this.TabPlanREQCustomerCode.Name = "TabPlanREQCustomerCode";
            this.TabPlanREQCustomerCode.Visible = true;
            this.TabPlanREQCustomerCode.Width = 100;
            // 
            // repositoryItemTabPlanREQCustomerCode
            // 
            this.repositoryItemTabPlanREQCustomerCode.AutoHeight = false;
            this.repositoryItemTabPlanREQCustomerCode.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemTabPlanREQCustomerCode.Name = "repositoryItemTabPlanREQCustomerCode";
            // 
            // TabPlanREQCustomerName
            // 
            this.TabPlanREQCustomerName.AppearanceHeader.Options.UseTextOptions = true;
            this.TabPlanREQCustomerName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TabPlanREQCustomerName.Caption = "ชื่อลูกค้า";
            this.TabPlanREQCustomerName.FieldName = "CustomerName";
            this.TabPlanREQCustomerName.Name = "TabPlanREQCustomerName";
            this.TabPlanREQCustomerName.Visible = true;
            this.TabPlanREQCustomerName.Width = 100;
            // 
            // TabPlanREQDocumentType
            // 
            this.TabPlanREQDocumentType.AppearanceHeader.Options.UseTextOptions = true;
            this.TabPlanREQDocumentType.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TabPlanREQDocumentType.Caption = "ประเภทรายการ";
            this.TabPlanREQDocumentType.ColumnEdit = this.repositoryItemTabPlanREQDocumentType;
            this.TabPlanREQDocumentType.FieldName = "DocumentType";
            this.TabPlanREQDocumentType.Name = "TabPlanREQDocumentType";
            this.TabPlanREQDocumentType.Visible = true;
            this.TabPlanREQDocumentType.Width = 100;
            // 
            // repositoryItemTabPlanREQDocumentType
            // 
            this.repositoryItemTabPlanREQDocumentType.AutoHeight = false;
            this.repositoryItemTabPlanREQDocumentType.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemTabPlanREQDocumentType.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Desc", "Desc")});
            this.repositoryItemTabPlanREQDocumentType.Name = "repositoryItemTabPlanREQDocumentType";
            this.repositoryItemTabPlanREQDocumentType.NullText = "";
            // 
            // TabPlanREQDocumentTypeRequiment
            // 
            this.TabPlanREQDocumentTypeRequiment.AppearanceHeader.Options.UseTextOptions = true;
            this.TabPlanREQDocumentTypeRequiment.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TabPlanREQDocumentTypeRequiment.Caption = "ประเภท Requiment";
            this.TabPlanREQDocumentTypeRequiment.ColumnEdit = this.repositoryItemTabPlanREQDocumentTypeRequiment;
            this.TabPlanREQDocumentTypeRequiment.FieldName = "DocumentTypeRequiment";
            this.TabPlanREQDocumentTypeRequiment.Name = "TabPlanREQDocumentTypeRequiment";
            this.TabPlanREQDocumentTypeRequiment.Visible = true;
            this.TabPlanREQDocumentTypeRequiment.Width = 100;
            // 
            // repositoryItemTabPlanREQDocumentTypeRequiment
            // 
            this.repositoryItemTabPlanREQDocumentTypeRequiment.AutoHeight = false;
            this.repositoryItemTabPlanREQDocumentTypeRequiment.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemTabPlanREQDocumentTypeRequiment.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Desc", "Desc")});
            this.repositoryItemTabPlanREQDocumentTypeRequiment.Name = "repositoryItemTabPlanREQDocumentTypeRequiment";
            this.repositoryItemTabPlanREQDocumentTypeRequiment.NullText = "";
            // 
            // TabPlanREQProgramName
            // 
            this.TabPlanREQProgramName.AppearanceHeader.Options.UseTextOptions = true;
            this.TabPlanREQProgramName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TabPlanREQProgramName.Caption = "โปรแกรม";
            this.TabPlanREQProgramName.FieldName = "ProgramName";
            this.TabPlanREQProgramName.Name = "TabPlanREQProgramName";
            this.TabPlanREQProgramName.Visible = true;
            this.TabPlanREQProgramName.Width = 100;
            // 
            // TabPlanREQTransactionDescription
            // 
            this.TabPlanREQTransactionDescription.AppearanceHeader.Options.UseTextOptions = true;
            this.TabPlanREQTransactionDescription.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TabPlanREQTransactionDescription.Caption = "รายละเอียด";
            this.TabPlanREQTransactionDescription.ColumnEdit = this.repositoryItemTabPlanREQTransactionDescription;
            this.TabPlanREQTransactionDescription.FieldName = "TransactionDescription";
            this.TabPlanREQTransactionDescription.MinWidth = 300;
            this.TabPlanREQTransactionDescription.Name = "TabPlanREQTransactionDescription";
            this.TabPlanREQTransactionDescription.Visible = true;
            this.TabPlanREQTransactionDescription.Width = 300;
            // 
            // repositoryItemTabPlanREQTransactionDescription
            // 
            this.repositoryItemTabPlanREQTransactionDescription.AutoHeight = false;
            this.repositoryItemTabPlanREQTransactionDescription.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemTabPlanREQTransactionDescription.Name = "repositoryItemTabPlanREQTransactionDescription";
            this.repositoryItemTabPlanREQTransactionDescription.ShowIcon = false;
            // 
            // TabPlanCol_AssigerCode
            // 
            this.TabPlanCol_AssigerCode.AppearanceHeader.Options.UseTextOptions = true;
            this.TabPlanCol_AssigerCode.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TabPlanCol_AssigerCode.Caption = "ผู้มอบหมาย";
            this.TabPlanCol_AssigerCode.ColumnEdit = this.repositoryItemEmployee_Plan;
            this.TabPlanCol_AssigerCode.FieldName = "AssigerCode";
            this.TabPlanCol_AssigerCode.Name = "TabPlanCol_AssigerCode";
            this.TabPlanCol_AssigerCode.Visible = true;
            this.TabPlanCol_AssigerCode.Width = 100;
            // 
            // repositoryItemEmployee_Plan
            // 
            this.repositoryItemEmployee_Plan.AutoHeight = false;
            this.repositoryItemEmployee_Plan.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemEmployee_Plan.Name = "repositoryItemEmployee_Plan";
            // 
            // TabPlanREQAssigeName
            // 
            this.TabPlanREQAssigeName.AppearanceHeader.Options.UseTextOptions = true;
            this.TabPlanREQAssigeName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TabPlanREQAssigeName.Caption = "ผู้มอบหมาย-";
            this.TabPlanREQAssigeName.FieldName = "AssigerName";
            this.TabPlanREQAssigeName.Name = "TabPlanREQAssigeName";
            this.TabPlanREQAssigeName.Width = 100;
            // 
            // TabPlanREQTypeSupervisor
            // 
            this.TabPlanREQTypeSupervisor.AppearanceHeader.Options.UseTextOptions = true;
            this.TabPlanREQTypeSupervisor.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TabPlanREQTypeSupervisor.Caption = "แผนกที่รับผิดชอบ";
            this.TabPlanREQTypeSupervisor.ColumnEdit = this.repositoryTabPlanREQTypeSupervisor;
            this.TabPlanREQTypeSupervisor.FieldName = "TypeSupervisor";
            this.TabPlanREQTypeSupervisor.Name = "TabPlanREQTypeSupervisor";
            this.TabPlanREQTypeSupervisor.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.TabPlanREQTypeSupervisor.Visible = true;
            this.TabPlanREQTypeSupervisor.Width = 100;
            // 
            // repositoryTabPlanREQTypeSupervisor
            // 
            this.repositoryTabPlanREQTypeSupervisor.AutoHeight = false;
            this.repositoryTabPlanREQTypeSupervisor.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryTabPlanREQTypeSupervisor.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Desc", "Desc")});
            this.repositoryTabPlanREQTypeSupervisor.Name = "repositoryTabPlanREQTypeSupervisor";
            this.repositoryTabPlanREQTypeSupervisor.NullText = "";
            // 
            // TabPlanCol_ResponsibleMan
            // 
            this.TabPlanCol_ResponsibleMan.AppearanceHeader.Options.UseTextOptions = true;
            this.TabPlanCol_ResponsibleMan.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TabPlanCol_ResponsibleMan.Caption = "ผู้รับผิดชอบ";
            this.TabPlanCol_ResponsibleMan.ColumnEdit = this.repositoryItemEmployee_Plan;
            this.TabPlanCol_ResponsibleMan.FieldName = "ResponsibleMan";
            this.TabPlanCol_ResponsibleMan.Name = "TabPlanCol_ResponsibleMan";
            this.TabPlanCol_ResponsibleMan.Visible = true;
            this.TabPlanCol_ResponsibleMan.Width = 100;
            // 
            // TabPlanREQSupervisorName
            // 
            this.TabPlanREQSupervisorName.AppearanceHeader.Options.UseTextOptions = true;
            this.TabPlanREQSupervisorName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TabPlanREQSupervisorName.Caption = "ชื่อผู้รับผิดชอบ-";
            this.TabPlanREQSupervisorName.FieldName = "SupervisorName";
            this.TabPlanREQSupervisorName.Name = "TabPlanREQSupervisorName";
            this.TabPlanREQSupervisorName.Width = 100;
            // 
            // TabPlanREQDueDate
            // 
            this.TabPlanREQDueDate.AppearanceCell.Options.UseTextOptions = true;
            this.TabPlanREQDueDate.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TabPlanREQDueDate.AppearanceHeader.Options.UseTextOptions = true;
            this.TabPlanREQDueDate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TabPlanREQDueDate.Caption = "วันที่กำหนดส่ง";
            this.TabPlanREQDueDate.ColumnEdit = this.repositoryItemTabPlanREQDueDate;
            this.TabPlanREQDueDate.FieldName = "DueDate";
            this.TabPlanREQDueDate.Name = "TabPlanREQDueDate";
            this.TabPlanREQDueDate.Visible = true;
            this.TabPlanREQDueDate.Width = 80;
            // 
            // repositoryItemTabPlanREQDueDate
            // 
            this.repositoryItemTabPlanREQDueDate.AutoHeight = false;
            this.repositoryItemTabPlanREQDueDate.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemTabPlanREQDueDate.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemTabPlanREQDueDate.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.repositoryItemTabPlanREQDueDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemTabPlanREQDueDate.EditFormat.FormatString = "dd/MM/yyyy";
            this.repositoryItemTabPlanREQDueDate.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemTabPlanREQDueDate.Mask.EditMask = "dd/MM/yyyy";
            this.repositoryItemTabPlanREQDueDate.Name = "repositoryItemTabPlanREQDueDate";
            // 
            // TabPlanREQDueDateTrue
            // 
            this.TabPlanREQDueDateTrue.AppearanceCell.Options.UseTextOptions = true;
            this.TabPlanREQDueDateTrue.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TabPlanREQDueDateTrue.AppearanceHeader.Options.UseTextOptions = true;
            this.TabPlanREQDueDateTrue.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TabPlanREQDueDateTrue.Caption = "วันที่ส่งจริง";
            this.TabPlanREQDueDateTrue.ColumnEdit = this.repositoryItemTabPlanREQDueDateTrue;
            this.TabPlanREQDueDateTrue.FieldName = "DueDateTrue";
            this.TabPlanREQDueDateTrue.Name = "TabPlanREQDueDateTrue";
            this.TabPlanREQDueDateTrue.Visible = true;
            this.TabPlanREQDueDateTrue.Width = 80;
            // 
            // repositoryItemTabPlanREQDueDateTrue
            // 
            this.repositoryItemTabPlanREQDueDateTrue.AutoHeight = false;
            this.repositoryItemTabPlanREQDueDateTrue.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemTabPlanREQDueDateTrue.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemTabPlanREQDueDateTrue.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.repositoryItemTabPlanREQDueDateTrue.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemTabPlanREQDueDateTrue.EditFormat.FormatString = "dd/MM/yyyy";
            this.repositoryItemTabPlanREQDueDateTrue.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemTabPlanREQDueDateTrue.Mask.EditMask = "dd/MM/yyyy";
            this.repositoryItemTabPlanREQDueDateTrue.Name = "repositoryItemTabPlanREQDueDateTrue";
            // 
            // TabPlanREQDueDateBigin
            // 
            this.TabPlanREQDueDateBigin.AppearanceCell.Options.UseTextOptions = true;
            this.TabPlanREQDueDateBigin.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TabPlanREQDueDateBigin.AppearanceHeader.Options.UseTextOptions = true;
            this.TabPlanREQDueDateBigin.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TabPlanREQDueDateBigin.Caption = "วันที่กำหนดส่งแรก";
            this.TabPlanREQDueDateBigin.ColumnEdit = this.repositoryItemTabPlanREQDueDateBigin;
            this.TabPlanREQDueDateBigin.FieldName = "DueDateBigin";
            this.TabPlanREQDueDateBigin.Name = "TabPlanREQDueDateBigin";
            this.TabPlanREQDueDateBigin.Visible = true;
            this.TabPlanREQDueDateBigin.Width = 100;
            // 
            // repositoryItemTabPlanREQDueDateBigin
            // 
            this.repositoryItemTabPlanREQDueDateBigin.AutoHeight = false;
            this.repositoryItemTabPlanREQDueDateBigin.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemTabPlanREQDueDateBigin.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemTabPlanREQDueDateBigin.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.repositoryItemTabPlanREQDueDateBigin.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemTabPlanREQDueDateBigin.EditFormat.FormatString = "dd/MM/yyyy";
            this.repositoryItemTabPlanREQDueDateBigin.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemTabPlanREQDueDateBigin.Mask.EditMask = "dd/MM/yyyy";
            this.repositoryItemTabPlanREQDueDateBigin.Name = "repositoryItemTabPlanREQDueDateBigin";
            // 
            // TabPlanREQStatusCustomer
            // 
            this.TabPlanREQStatusCustomer.AppearanceHeader.Options.UseTextOptions = true;
            this.TabPlanREQStatusCustomer.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TabPlanREQStatusCustomer.Caption = "สถานะลูกค้า";
            this.TabPlanREQStatusCustomer.FieldName = "StatusCustomer";
            this.TabPlanREQStatusCustomer.Name = "TabPlanREQStatusCustomer";
            this.TabPlanREQStatusCustomer.Visible = true;
            this.TabPlanREQStatusCustomer.Width = 100;
            // 
            // TabPlanREQTransactionStatus
            // 
            this.TabPlanREQTransactionStatus.AppearanceHeader.Options.UseTextOptions = true;
            this.TabPlanREQTransactionStatus.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TabPlanREQTransactionStatus.Caption = "สถานะงาน";
            this.TabPlanREQTransactionStatus.ColumnEdit = this.repositoryItemTabPlanREQTransactionStatus;
            this.TabPlanREQTransactionStatus.FieldName = "TransactionStatus";
            this.TabPlanREQTransactionStatus.Name = "TabPlanREQTransactionStatus";
            this.TabPlanREQTransactionStatus.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.TabPlanREQTransactionStatus.Visible = true;
            this.TabPlanREQTransactionStatus.Width = 100;
            // 
            // repositoryItemTabPlanREQTransactionStatus
            // 
            this.repositoryItemTabPlanREQTransactionStatus.AutoHeight = false;
            this.repositoryItemTabPlanREQTransactionStatus.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemTabPlanREQTransactionStatus.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Desc", "Desc")});
            this.repositoryItemTabPlanREQTransactionStatus.Name = "repositoryItemTabPlanREQTransactionStatus";
            this.repositoryItemTabPlanREQTransactionStatus.NullText = "";
            // 
            // TabPlanREQTransactionConClude
            // 
            this.TabPlanREQTransactionConClude.AppearanceHeader.Options.UseTextOptions = true;
            this.TabPlanREQTransactionConClude.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TabPlanREQTransactionConClude.Caption = "หมายเหตุ";
            this.TabPlanREQTransactionConClude.ColumnEdit = this.repositoryItemTabPlanREQTransactionConClude;
            this.TabPlanREQTransactionConClude.FieldName = "TransactionConClude";
            this.TabPlanREQTransactionConClude.MinWidth = 300;
            this.TabPlanREQTransactionConClude.Name = "TabPlanREQTransactionConClude";
            this.TabPlanREQTransactionConClude.Visible = true;
            this.TabPlanREQTransactionConClude.Width = 300;
            // 
            // repositoryItemTabPlanREQTransactionConClude
            // 
            this.repositoryItemTabPlanREQTransactionConClude.AutoHeight = false;
            this.repositoryItemTabPlanREQTransactionConClude.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemTabPlanREQTransactionConClude.Name = "repositoryItemTabPlanREQTransactionConClude";
            this.repositoryItemTabPlanREQTransactionConClude.ShowIcon = false;
            // 
            // TabTest
            // 
            this.TabTest.Controls.Add(this.gridControlTest);
            this.TabTest.Name = "TabTest";
            this.TabTest.Size = new System.Drawing.Size(1272, 527);
            this.TabTest.Text = "Test";
            // 
            // gridControlTest
            // 
            this.gridControlTest.ContextMenuStrip = this.contextMenuStripGrit;
            this.gridControlTest.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlTest.Location = new System.Drawing.Point(0, 0);
            this.gridControlTest.MainView = this.advBandedGridTest;
            this.gridControlTest.Name = "gridControlTest";
            this.gridControlTest.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemDateEdit5,
            this.repositoryItemTabTestCustomerCode,
            this.repositoryItemTransactionDescriptionTest,
            this.repositoryItemTestTransactionConClude,
            this.repositoryItemCreateTest,
            this.repositoryItemPullTest,
            this.repositoryItemADDTest,
            this.repositoryItemSaveTest,
            this.repositoryItemEditTest,
            this.repositoryItemCancelTest,
            this.repositoryItemSearchTest,
            this.repositoryItemPriviewTest,
            this.repositoryItemStatusTest,
            this.repositoryItemTabTestSupportTest,
            this.repositoryItemTabTestOpenFileVersion,
            this.repositoryItemTabTestWebUpdate,
            this.repositoryItemApprovedTest,
            this.repositoryItemTestUpdate,
            this.repositoryItemEmployee_TabTest});
            this.gridControlTest.Size = new System.Drawing.Size(1272, 527);
            this.gridControlTest.TabIndex = 1;
            this.gridControlTest.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.advBandedGridTest});
            // 
            // advBandedGridTest
            // 
            this.advBandedGridTest.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.advBandedGridTest.Appearance.FocusedRow.Options.UseForeColor = true;
            this.advBandedGridTest.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.advBandedGridTest.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.advBandedGridTest.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.advBandedGridTest.Appearance.SelectedRow.Options.UseForeColor = true;
            this.advBandedGridTest.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand3});
            this.advBandedGridTest.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.bandedGridColumn18,
            this.TabTestTransactionDate,
            this.TabTestCustomerCode,
            this.TabTestCustomerName,
            this.TabTestTransactionDescription,
            this.TabTestFolderBackUP,
            this.TabTestProgramName,
            this.TabTestVersion,
            this.TestProgramTest,
            this.TabTestUserTest,
            this.TabTestCreateTest,
            this.TabTestPullTest,
            this.TabTestADDTest,
            this.TabTestSaveTest,
            this.TabTestEditTest,
            this.TabTestCancelTest,
            this.TabTestSearchTest,
            this.TabTestPriviewTest,
            this.TabTestWebUpdate,
            this.TabTestStatusTest,
            this.TabTestSupportTest,
            this.TabTestTransactionConClude,
            this.TabTestRandomTest,
            this.TabTestOpenFileVersion,
            this.TabApprovedTest,
            this.TabTestUpdate,
            this.TabTestSupervisorName,
            this.TabTestResponsibleMan});
            this.advBandedGridTest.CustomizationFormBounds = new System.Drawing.Rectangle(630, 286, 295, 307);
            this.advBandedGridTest.DetailHeight = 227;
            this.advBandedGridTest.FixedLineWidth = 1;
            this.advBandedGridTest.GridControl = this.gridControlTest;
            this.advBandedGridTest.Name = "advBandedGridTest";
            this.advBandedGridTest.OptionsSelection.MultiSelect = true;
            this.advBandedGridTest.OptionsView.ShowBands = false;
            this.advBandedGridTest.OptionsView.ShowGroupPanel = false;
            // 
            // gridBand3
            // 
            this.gridBand3.Caption = "gridBand1";
            this.gridBand3.Columns.Add(this.TabTestTransactionDate);
            this.gridBand3.Columns.Add(this.TabTestCustomerCode);
            this.gridBand3.Columns.Add(this.TabTestCustomerName);
            this.gridBand3.Columns.Add(this.TabTestTransactionDescription);
            this.gridBand3.Columns.Add(this.TabTestFolderBackUP);
            this.gridBand3.Columns.Add(this.TabTestProgramName);
            this.gridBand3.Columns.Add(this.TabTestVersion);
            this.gridBand3.Columns.Add(this.TabTestUserTest);
            this.gridBand3.Columns.Add(this.TestProgramTest);
            this.gridBand3.Columns.Add(this.TabTestCreateTest);
            this.gridBand3.Columns.Add(this.TabTestPullTest);
            this.gridBand3.Columns.Add(this.TabTestADDTest);
            this.gridBand3.Columns.Add(this.TabTestSaveTest);
            this.gridBand3.Columns.Add(this.TabTestEditTest);
            this.gridBand3.Columns.Add(this.TabTestCancelTest);
            this.gridBand3.Columns.Add(this.TabTestSearchTest);
            this.gridBand3.Columns.Add(this.TabTestPriviewTest);
            this.gridBand3.Columns.Add(this.TabTestWebUpdate);
            this.gridBand3.Columns.Add(this.TabTestUpdate);
            this.gridBand3.Columns.Add(this.TabApprovedTest);
            this.gridBand3.Columns.Add(this.TabTestSupportTest);
            this.gridBand3.Columns.Add(this.TabTestStatusTest);
            this.gridBand3.Columns.Add(this.TabTestOpenFileVersion);
            this.gridBand3.Columns.Add(this.TabTestResponsibleMan);
            this.gridBand3.Columns.Add(this.TabTestSupervisorName);
            this.gridBand3.Columns.Add(this.TabTestRandomTest);
            this.gridBand3.Columns.Add(this.TabTestTransactionConClude);
            this.gridBand3.Columns.Add(this.bandedGridColumn18);
            this.gridBand3.MinWidth = 13;
            this.gridBand3.Name = "gridBand3";
            this.gridBand3.VisibleIndex = 0;
            this.gridBand3.Width = 2620;
            // 
            // TabTestTransactionDate
            // 
            this.TabTestTransactionDate.AppearanceCell.Options.UseTextOptions = true;
            this.TabTestTransactionDate.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TabTestTransactionDate.AppearanceHeader.Options.UseTextOptions = true;
            this.TabTestTransactionDate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TabTestTransactionDate.Caption = "วันที่";
            this.TabTestTransactionDate.ColumnEdit = this.repositoryItemDateEdit5;
            this.TabTestTransactionDate.FieldName = "TransactionDate";
            this.TabTestTransactionDate.Name = "TabTestTransactionDate";
            this.TabTestTransactionDate.Visible = true;
            this.TabTestTransactionDate.Width = 80;
            // 
            // repositoryItemDateEdit5
            // 
            this.repositoryItemDateEdit5.AutoHeight = false;
            this.repositoryItemDateEdit5.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit5.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemDateEdit5.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.repositoryItemDateEdit5.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit5.EditFormat.FormatString = "dd/MM/yyyy";
            this.repositoryItemDateEdit5.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit5.Mask.EditMask = "dd/MM/yyyy";
            this.repositoryItemDateEdit5.Name = "repositoryItemDateEdit5";
            // 
            // TabTestCustomerCode
            // 
            this.TabTestCustomerCode.AppearanceHeader.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.TabTestCustomerCode.AppearanceHeader.Options.UseBackColor = true;
            this.TabTestCustomerCode.AppearanceHeader.Options.UseTextOptions = true;
            this.TabTestCustomerCode.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TabTestCustomerCode.Caption = "รหัสลูกค้า";
            this.TabTestCustomerCode.ColumnEdit = this.repositoryItemTabTestCustomerCode;
            this.TabTestCustomerCode.FieldName = "CustomerCode";
            this.TabTestCustomerCode.Name = "TabTestCustomerCode";
            this.TabTestCustomerCode.Visible = true;
            this.TabTestCustomerCode.Width = 100;
            // 
            // repositoryItemTabTestCustomerCode
            // 
            this.repositoryItemTabTestCustomerCode.AutoHeight = false;
            this.repositoryItemTabTestCustomerCode.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemTabTestCustomerCode.Name = "repositoryItemTabTestCustomerCode";
            // 
            // TabTestCustomerName
            // 
            this.TabTestCustomerName.AppearanceHeader.Options.UseTextOptions = true;
            this.TabTestCustomerName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TabTestCustomerName.Caption = "ชื่อลูกค้า";
            this.TabTestCustomerName.FieldName = "CustomerName";
            this.TabTestCustomerName.Name = "TabTestCustomerName";
            this.TabTestCustomerName.Visible = true;
            this.TabTestCustomerName.Width = 100;
            // 
            // TabTestTransactionDescription
            // 
            this.TabTestTransactionDescription.AppearanceHeader.Options.UseTextOptions = true;
            this.TabTestTransactionDescription.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TabTestTransactionDescription.Caption = "เรื่อง";
            this.TabTestTransactionDescription.ColumnEdit = this.repositoryItemTransactionDescriptionTest;
            this.TabTestTransactionDescription.FieldName = "TransactionDescription";
            this.TabTestTransactionDescription.MinWidth = 300;
            this.TabTestTransactionDescription.Name = "TabTestTransactionDescription";
            this.TabTestTransactionDescription.Visible = true;
            this.TabTestTransactionDescription.Width = 300;
            // 
            // repositoryItemTransactionDescriptionTest
            // 
            this.repositoryItemTransactionDescriptionTest.AutoHeight = false;
            this.repositoryItemTransactionDescriptionTest.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemTransactionDescriptionTest.Name = "repositoryItemTransactionDescriptionTest";
            this.repositoryItemTransactionDescriptionTest.ShowIcon = false;
            // 
            // TabTestFolderBackUP
            // 
            this.TabTestFolderBackUP.AppearanceHeader.Options.UseTextOptions = true;
            this.TabTestFolderBackUP.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TabTestFolderBackUP.Caption = "Folder BackUP";
            this.TabTestFolderBackUP.FieldName = "FolderBackUP";
            this.TabTestFolderBackUP.Name = "TabTestFolderBackUP";
            this.TabTestFolderBackUP.Visible = true;
            this.TabTestFolderBackUP.Width = 120;
            // 
            // TabTestProgramName
            // 
            this.TabTestProgramName.AppearanceHeader.Options.UseTextOptions = true;
            this.TabTestProgramName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TabTestProgramName.Caption = "โปรแกรมที่ Update";
            this.TabTestProgramName.FieldName = "ProgramName";
            this.TabTestProgramName.Name = "TabTestProgramName";
            this.TabTestProgramName.Visible = true;
            this.TabTestProgramName.Width = 120;
            // 
            // TabTestVersion
            // 
            this.TabTestVersion.AppearanceHeader.Options.UseTextOptions = true;
            this.TabTestVersion.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TabTestVersion.Caption = "Version";
            this.TabTestVersion.FieldName = "Version";
            this.TabTestVersion.Name = "TabTestVersion";
            this.TabTestVersion.Visible = true;
            this.TabTestVersion.Width = 80;
            // 
            // TabTestUserTest
            // 
            this.TabTestUserTest.AppearanceHeader.Options.UseTextOptions = true;
            this.TabTestUserTest.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TabTestUserTest.Caption = "User Test";
            this.TabTestUserTest.FieldName = "UserTest";
            this.TabTestUserTest.Name = "TabTestUserTest";
            this.TabTestUserTest.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.TabTestUserTest.Visible = true;
            this.TabTestUserTest.Width = 80;
            // 
            // TestProgramTest
            // 
            this.TestProgramTest.AppearanceHeader.Options.UseTextOptions = true;
            this.TestProgramTest.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TestProgramTest.Caption = "โปรแกรม Test";
            this.TestProgramTest.FieldName = "ProgramTest";
            this.TestProgramTest.Name = "TestProgramTest";
            this.TestProgramTest.Visible = true;
            this.TestProgramTest.Width = 100;
            // 
            // TabTestCreateTest
            // 
            this.TabTestCreateTest.AppearanceHeader.Options.UseTextOptions = true;
            this.TabTestCreateTest.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TabTestCreateTest.Caption = "สร้าง";
            this.TabTestCreateTest.ColumnEdit = this.repositoryItemCreateTest;
            this.TabTestCreateTest.FieldName = "CreateTest";
            this.TabTestCreateTest.Name = "TabTestCreateTest";
            this.TabTestCreateTest.Visible = true;
            this.TabTestCreateTest.Width = 60;
            // 
            // repositoryItemCreateTest
            // 
            this.repositoryItemCreateTest.AutoHeight = false;
            this.repositoryItemCreateTest.Name = "repositoryItemCreateTest";
            this.repositoryItemCreateTest.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            // 
            // TabTestPullTest
            // 
            this.TabTestPullTest.AppearanceHeader.Options.UseTextOptions = true;
            this.TabTestPullTest.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TabTestPullTest.Caption = "ดึง";
            this.TabTestPullTest.ColumnEdit = this.repositoryItemPullTest;
            this.TabTestPullTest.FieldName = "PullTest";
            this.TabTestPullTest.Name = "TabTestPullTest";
            this.TabTestPullTest.Visible = true;
            this.TabTestPullTest.Width = 60;
            // 
            // repositoryItemPullTest
            // 
            this.repositoryItemPullTest.AutoHeight = false;
            this.repositoryItemPullTest.Name = "repositoryItemPullTest";
            this.repositoryItemPullTest.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            // 
            // TabTestADDTest
            // 
            this.TabTestADDTest.AppearanceHeader.Options.UseTextOptions = true;
            this.TabTestADDTest.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TabTestADDTest.Caption = "เพิ่ม";
            this.TabTestADDTest.ColumnEdit = this.repositoryItemADDTest;
            this.TabTestADDTest.FieldName = "ADDTest";
            this.TabTestADDTest.Name = "TabTestADDTest";
            this.TabTestADDTest.Visible = true;
            this.TabTestADDTest.Width = 60;
            // 
            // repositoryItemADDTest
            // 
            this.repositoryItemADDTest.AutoHeight = false;
            this.repositoryItemADDTest.Name = "repositoryItemADDTest";
            this.repositoryItemADDTest.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            // 
            // TabTestSaveTest
            // 
            this.TabTestSaveTest.AppearanceHeader.Options.UseTextOptions = true;
            this.TabTestSaveTest.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TabTestSaveTest.Caption = "Save";
            this.TabTestSaveTest.ColumnEdit = this.repositoryItemSaveTest;
            this.TabTestSaveTest.FieldName = "SaveTest";
            this.TabTestSaveTest.Name = "TabTestSaveTest";
            this.TabTestSaveTest.Visible = true;
            this.TabTestSaveTest.Width = 60;
            // 
            // repositoryItemSaveTest
            // 
            this.repositoryItemSaveTest.AutoHeight = false;
            this.repositoryItemSaveTest.Name = "repositoryItemSaveTest";
            this.repositoryItemSaveTest.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            // 
            // TabTestEditTest
            // 
            this.TabTestEditTest.AppearanceHeader.Options.UseTextOptions = true;
            this.TabTestEditTest.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TabTestEditTest.Caption = "Edit";
            this.TabTestEditTest.ColumnEdit = this.repositoryItemEditTest;
            this.TabTestEditTest.FieldName = "EditTest";
            this.TabTestEditTest.Name = "TabTestEditTest";
            this.TabTestEditTest.Visible = true;
            this.TabTestEditTest.Width = 60;
            // 
            // repositoryItemEditTest
            // 
            this.repositoryItemEditTest.AutoHeight = false;
            this.repositoryItemEditTest.Name = "repositoryItemEditTest";
            this.repositoryItemEditTest.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            // 
            // TabTestCancelTest
            // 
            this.TabTestCancelTest.AppearanceHeader.Options.UseTextOptions = true;
            this.TabTestCancelTest.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TabTestCancelTest.Caption = "ยกเลิก";
            this.TabTestCancelTest.ColumnEdit = this.repositoryItemCancelTest;
            this.TabTestCancelTest.FieldName = "CancelTest";
            this.TabTestCancelTest.Name = "TabTestCancelTest";
            this.TabTestCancelTest.Visible = true;
            this.TabTestCancelTest.Width = 60;
            // 
            // repositoryItemCancelTest
            // 
            this.repositoryItemCancelTest.AutoHeight = false;
            this.repositoryItemCancelTest.Name = "repositoryItemCancelTest";
            this.repositoryItemCancelTest.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            // 
            // TabTestSearchTest
            // 
            this.TabTestSearchTest.AppearanceHeader.Options.UseTextOptions = true;
            this.TabTestSearchTest.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TabTestSearchTest.Caption = "ค้นหา";
            this.TabTestSearchTest.ColumnEdit = this.repositoryItemSearchTest;
            this.TabTestSearchTest.FieldName = "SearchTest";
            this.TabTestSearchTest.Name = "TabTestSearchTest";
            this.TabTestSearchTest.Visible = true;
            this.TabTestSearchTest.Width = 60;
            // 
            // repositoryItemSearchTest
            // 
            this.repositoryItemSearchTest.AutoHeight = false;
            this.repositoryItemSearchTest.Name = "repositoryItemSearchTest";
            this.repositoryItemSearchTest.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            // 
            // TabTestPriviewTest
            // 
            this.TabTestPriviewTest.AppearanceHeader.Options.UseTextOptions = true;
            this.TabTestPriviewTest.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TabTestPriviewTest.Caption = "Priview";
            this.TabTestPriviewTest.ColumnEdit = this.repositoryItemPriviewTest;
            this.TabTestPriviewTest.FieldName = "PriviewTest";
            this.TabTestPriviewTest.Name = "TabTestPriviewTest";
            this.TabTestPriviewTest.Visible = true;
            this.TabTestPriviewTest.Width = 60;
            // 
            // repositoryItemPriviewTest
            // 
            this.repositoryItemPriviewTest.AutoHeight = false;
            this.repositoryItemPriviewTest.Name = "repositoryItemPriviewTest";
            this.repositoryItemPriviewTest.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            // 
            // TabTestWebUpdate
            // 
            this.TabTestWebUpdate.AppearanceHeader.Options.UseTextOptions = true;
            this.TabTestWebUpdate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TabTestWebUpdate.Caption = "WebUpdate";
            this.TabTestWebUpdate.ColumnEdit = this.repositoryItemTabTestWebUpdate;
            this.TabTestWebUpdate.FieldName = "WebUpdate";
            this.TabTestWebUpdate.Name = "TabTestWebUpdate";
            this.TabTestWebUpdate.Visible = true;
            this.TabTestWebUpdate.Width = 70;
            // 
            // repositoryItemTabTestWebUpdate
            // 
            this.repositoryItemTabTestWebUpdate.AutoHeight = false;
            this.repositoryItemTabTestWebUpdate.Name = "repositoryItemTabTestWebUpdate";
            this.repositoryItemTabTestWebUpdate.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            // 
            // TabTestUpdate
            // 
            this.TabTestUpdate.AppearanceHeader.Options.UseTextOptions = true;
            this.TabTestUpdate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TabTestUpdate.Caption = "TestUpdate";
            this.TabTestUpdate.ColumnEdit = this.repositoryItemTestUpdate;
            this.TabTestUpdate.FieldName = "TestUpdate";
            this.TabTestUpdate.Name = "TabTestUpdate";
            this.TabTestUpdate.Visible = true;
            this.TabTestUpdate.Width = 70;
            // 
            // repositoryItemTestUpdate
            // 
            this.repositoryItemTestUpdate.AutoHeight = false;
            this.repositoryItemTestUpdate.Name = "repositoryItemTestUpdate";
            this.repositoryItemTestUpdate.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            // 
            // TabApprovedTest
            // 
            this.TabApprovedTest.AppearanceHeader.Options.UseTextOptions = true;
            this.TabApprovedTest.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TabApprovedTest.Caption = "Approved";
            this.TabApprovedTest.ColumnEdit = this.repositoryItemApprovedTest;
            this.TabApprovedTest.FieldName = "ApprovedTest";
            this.TabApprovedTest.Name = "TabApprovedTest";
            this.TabApprovedTest.Visible = true;
            this.TabApprovedTest.Width = 70;
            // 
            // repositoryItemApprovedTest
            // 
            this.repositoryItemApprovedTest.AutoHeight = false;
            this.repositoryItemApprovedTest.Name = "repositoryItemApprovedTest";
            this.repositoryItemApprovedTest.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            // 
            // TabTestSupportTest
            // 
            this.TabTestSupportTest.AppearanceHeader.Options.UseTextOptions = true;
            this.TabTestSupportTest.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TabTestSupportTest.Caption = "Support Test";
            this.TabTestSupportTest.ColumnEdit = this.repositoryItemTabTestSupportTest;
            this.TabTestSupportTest.FieldName = "SupportTest";
            this.TabTestSupportTest.Name = "TabTestSupportTest";
            this.TabTestSupportTest.Visible = true;
            this.TabTestSupportTest.Width = 100;
            // 
            // repositoryItemTabTestSupportTest
            // 
            this.repositoryItemTabTestSupportTest.AutoHeight = false;
            this.repositoryItemTabTestSupportTest.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemTabTestSupportTest.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Desc", "Desc")});
            this.repositoryItemTabTestSupportTest.Name = "repositoryItemTabTestSupportTest";
            this.repositoryItemTabTestSupportTest.NullText = "";
            // 
            // TabTestStatusTest
            // 
            this.TabTestStatusTest.AppearanceHeader.Options.UseTextOptions = true;
            this.TabTestStatusTest.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TabTestStatusTest.Caption = "สถานะ Test";
            this.TabTestStatusTest.ColumnEdit = this.repositoryItemStatusTest;
            this.TabTestStatusTest.FieldName = "StatusTest";
            this.TabTestStatusTest.Name = "TabTestStatusTest";
            this.TabTestStatusTest.Visible = true;
            this.TabTestStatusTest.Width = 100;
            // 
            // repositoryItemStatusTest
            // 
            this.repositoryItemStatusTest.AutoHeight = false;
            this.repositoryItemStatusTest.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemStatusTest.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Desc", "Desc")});
            this.repositoryItemStatusTest.Name = "repositoryItemStatusTest";
            this.repositoryItemStatusTest.NullText = "";
            // 
            // TabTestOpenFileVersion
            // 
            this.TabTestOpenFileVersion.AppearanceHeader.Options.UseTextOptions = true;
            this.TabTestOpenFileVersion.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TabTestOpenFileVersion.Caption = "เวอร์ชั่นโปรแกรมOpenfile/WebUpdate ";
            this.TabTestOpenFileVersion.ColumnEdit = this.repositoryItemTabTestOpenFileVersion;
            this.TabTestOpenFileVersion.FieldName = "OpenFileVersion";
            this.TabTestOpenFileVersion.Name = "TabTestOpenFileVersion";
            this.TabTestOpenFileVersion.Visible = true;
            this.TabTestOpenFileVersion.Width = 150;
            // 
            // repositoryItemTabTestOpenFileVersion
            // 
            this.repositoryItemTabTestOpenFileVersion.AutoHeight = false;
            this.repositoryItemTabTestOpenFileVersion.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemTabTestOpenFileVersion.Name = "repositoryItemTabTestOpenFileVersion";
            this.repositoryItemTabTestOpenFileVersion.ShowIcon = false;
            // 
            // TabTestResponsibleMan
            // 
            this.TabTestResponsibleMan.AppearanceHeader.Options.UseTextOptions = true;
            this.TabTestResponsibleMan.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TabTestResponsibleMan.Caption = "ผู้รับผิดชอบ";
            this.TabTestResponsibleMan.ColumnEdit = this.repositoryItemEmployee_TabTest;
            this.TabTestResponsibleMan.FieldName = "ResponsibleMan";
            this.TabTestResponsibleMan.Name = "TabTestResponsibleMan";
            this.TabTestResponsibleMan.Visible = true;
            this.TabTestResponsibleMan.Width = 100;
            // 
            // repositoryItemEmployee_TabTest
            // 
            this.repositoryItemEmployee_TabTest.AutoHeight = false;
            this.repositoryItemEmployee_TabTest.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemEmployee_TabTest.Name = "repositoryItemEmployee_TabTest";
            // 
            // TabTestSupervisorName
            // 
            this.TabTestSupervisorName.AppearanceHeader.Options.UseTextOptions = true;
            this.TabTestSupervisorName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TabTestSupervisorName.Caption = "ชื่อผู้รับผิดชอบ";
            this.TabTestSupervisorName.FieldName = "SupervisorName";
            this.TabTestSupervisorName.Name = "TabTestSupervisorName";
            this.TabTestSupervisorName.Width = 100;
            // 
            // TabTestRandomTest
            // 
            this.TabTestRandomTest.AppearanceHeader.Options.UseTextOptions = true;
            this.TabTestRandomTest.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TabTestRandomTest.Caption = "สุ่ม Test";
            this.TabTestRandomTest.FieldName = "RandomTest";
            this.TabTestRandomTest.Name = "TabTestRandomTest";
            this.TabTestRandomTest.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.TabTestRandomTest.Visible = true;
            this.TabTestRandomTest.Width = 100;
            // 
            // TabTestTransactionConClude
            // 
            this.TabTestTransactionConClude.AppearanceHeader.Options.UseTextOptions = true;
            this.TabTestTransactionConClude.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TabTestTransactionConClude.Caption = "หมายเหตุ";
            this.TabTestTransactionConClude.ColumnEdit = this.repositoryItemTestTransactionConClude;
            this.TabTestTransactionConClude.FieldName = "TransactionConClude";
            this.TabTestTransactionConClude.MinWidth = 300;
            this.TabTestTransactionConClude.Name = "TabTestTransactionConClude";
            this.TabTestTransactionConClude.Visible = true;
            this.TabTestTransactionConClude.Width = 300;
            // 
            // repositoryItemTestTransactionConClude
            // 
            this.repositoryItemTestTransactionConClude.AutoHeight = false;
            this.repositoryItemTestTransactionConClude.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemTestTransactionConClude.Name = "repositoryItemTestTransactionConClude";
            this.repositoryItemTestTransactionConClude.ShowIcon = false;
            // 
            // bandedGridColumn18
            // 
            this.bandedGridColumn18.AppearanceHeader.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.bandedGridColumn18.AppearanceHeader.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.bandedGridColumn18.AppearanceHeader.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.bandedGridColumn18.AppearanceHeader.Options.UseBackColor = true;
            this.bandedGridColumn18.AppearanceHeader.Options.UseBorderColor = true;
            this.bandedGridColumn18.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn18.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn18.Caption = "ลำดับ";
            this.bandedGridColumn18.FieldName = "NoLine";
            this.bandedGridColumn18.Name = "bandedGridColumn18";
            this.bandedGridColumn18.OptionsColumn.ReadOnly = true;
            this.bandedGridColumn18.OptionsFilter.ImmediateUpdatePopupDateFilterOnCheck = DevExpress.Utils.DefaultBoolean.True;
            this.bandedGridColumn18.RowIndex = 1;
            this.bandedGridColumn18.Width = 27;
            // 
            // defaultLookAndFeel1
            // 
            this.defaultLookAndFeel1.LookAndFeel.SkinName = "VS2010";
            // 
            // FormTasksOfCompany
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1284, 661);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormTasksOfCompany";
            center1.Company = "";
            center1.ErrorOn = true;
            center1.Password = null;
            center1.Server = null;
            center1.Username = null;
            this.QEBCenterInfo = center1;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Tasks Of Company";
            this.Load += new System.EventHandler(this.FormTasksOfCompany_Load);
            ((System.ComponentModel.ISupportInitialize)(this.styleControllerBlue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.styleControllerRed)).EndInit();
            this.contextMenuStripGrit.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barAndDockingController1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSearch.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueSerch.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateSearchTo.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateSearchTo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlBackLog)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.advBandedGridTaskOfCompany)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryTransactionDate.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryTransactionDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryCustomerCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repository_DocumentType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryDocumentTypeRequiment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryTransactionDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryDueDate.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryDueDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemEmployee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repository_TypeSupervisor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryDueDateBigin.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryDueDateBigin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryDueDateTrue.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryDueDateTrue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repository_TransactionStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTestStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryTransactionConClude)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueDateSearch.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateSearchFrom.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateSearchFrom.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SearchStatus_CheckCombobox.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TabControl)).EndInit();
            this.TabControl.ResumeLayout(false);
            this.TabBacklog.ResumeLayout(false);
            this.TabPlanREQ.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlPlanREQ)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.advBandedGridPlanREQ)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTabPlanREQTransactionDate.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTabPlanREQTransactionDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTabPlanREQCustomerCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTabPlanREQDocumentType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTabPlanREQDocumentTypeRequiment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTabPlanREQTransactionDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemEmployee_Plan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryTabPlanREQTypeSupervisor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTabPlanREQDueDate.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTabPlanREQDueDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTabPlanREQDueDateTrue.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTabPlanREQDueDateTrue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTabPlanREQDueDateBigin.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTabPlanREQDueDateBigin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTabPlanREQTransactionStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTabPlanREQTransactionConClude)).EndInit();
            this.TabTest.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlTest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.advBandedGridTest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit5.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTabTestCustomerCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTransactionDescriptionTest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCreateTest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPullTest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemADDTest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSaveTest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemEditTest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCancelTest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSearchTest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPriviewTest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTabTestWebUpdate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTestUpdate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemApprovedTest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTabTestSupportTest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemStatusTest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTabTestOpenFileVersion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemEmployee_TabTest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTestTransactionConClude)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.StyleController styleControllerBlue;
        private DevExpress.XtraEditors.StyleController styleControllerRed;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarAndDockingController barAndDockingController1;
        private DevExpress.XtraBars.BarButtonItem barEdit;
        private DevExpress.XtraBars.BarSubItem barSubItem1;
        private DevExpress.XtraBars.BarButtonItem btnbarEdit;
        private DevExpress.XtraBars.BarButtonItem btnbarSave;
        private DevExpress.XtraBars.BarButtonItem btnbarUndo;
        private DevExpress.XtraBars.BarButtonItem BarUndo;
        private DevExpress.XtraBars.BarButtonItem barSave;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripGrit;
        private System.Windows.Forms.ToolStripMenuItem StripMenuAdd;
        private System.Windows.Forms.ToolStripMenuItem StripMenuCancel;
        private System.Windows.Forms.ToolStripComboBox StripMenuColor;
        private DevExpress.XtraGrid.GridControl gridControlBackLog;
        private DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView advBandedGridTaskOfCompany;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn Col_NoLine;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn Col_TransactionDate;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryTransactionDate;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn Col_CustomerCode;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryCustomerCode;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn Col_CustomerName;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn Col_StatusCustomer;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn Col_DocumentType;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repository_DocumentType;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn Col_DocumentTypeRequiment;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryDocumentTypeRequiment;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn Col_ProgramName;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn Col_TransactionDescription;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryTransactionDescription;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn Col_AssigerName;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn Col_SupervisorName;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn Col_DueDate;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryDueDate;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn Col_DueDateTrue;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryDueDateTrue;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn Col_TransactionStatus;
        private DevExpress.XtraEditors.DateEdit dateSearchTo;
        private DevExpress.XtraEditors.LookUpEdit lueSerch;
        private DevExpress.XtraEditors.TextEdit txtSearch;
        private DevExpress.XtraEditors.LookUpEdit lueDateSearch;
        private DevExpress.XtraEditors.DateEdit dateSearchFrom;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn Col_TransactionConClude;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryTransactionConClude;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn Col_TypeSupervisor;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repository_TypeSupervisor;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private System.Windows.Forms.ToolStripMenuItem StripMenuReCancel;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repository_TransactionStatus;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarButtonItem barPlan;
        private DevExpress.XtraBars.BarButtonItem barLineNotify;
        private DevExpress.XtraBars.BarButtonItem barExport;
        private DevExpress.LookAndFeel.DefaultLookAndFeel defaultLookAndFeel1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn Col_DueDateBigin;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryDueDateBigin;
        private DevExpress.XtraTab.XtraTabControl TabControl;
        private DevExpress.XtraTab.XtraTabPage TabTest;
        private DevExpress.XtraGrid.GridControl gridControlTest;
        private DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView advBandedGridTest;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn18;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn TabTestTransactionDate;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit5;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn TabTestCustomerCode;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemTabTestCustomerCode;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn TabTestCustomerName;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn TabTestProgramName;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn TabTestTransactionDescription;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemTransactionDescriptionTest;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn TabTestFolderBackUP;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn TabTestRandomTest;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn TabTestVersion;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn TabTestUserTest;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn TabTestTransactionConClude;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemTestTransactionConClude;
        private DevExpress.XtraTab.XtraTabPage TabBacklog;
        private DevExpress.XtraTab.XtraTabPage TabPlanREQ;
        private DevExpress.XtraGrid.GridControl gridControlPlanREQ;
        private DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView advBandedGridPlanREQ;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn TabPlanREQNoLine;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn TabPlanREQTransactionDate;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemTabPlanREQTransactionDate;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn TabPlanREQCustomerCode;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemTabPlanREQCustomerCode;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn TabPlanREQCustomerName;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn TabPlanREQDocumentType;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemTabPlanREQDocumentType;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn TabPlanREQDocumentTypeRequiment;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemTabPlanREQDocumentTypeRequiment;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn TabPlanREQProgramName;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn TabPlanREQTransactionDescription;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemTabPlanREQTransactionDescription;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn TabPlanREQAssigeName;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn TabPlanREQTypeSupervisor;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryTabPlanREQTypeSupervisor;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn TabPlanREQSupervisorName;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn TabPlanREQDueDate;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemTabPlanREQDueDate;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn TabPlanREQDueDateTrue;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemTabPlanREQDueDateTrue;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn TabPlanREQDueDateBigin;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemTabPlanREQDueDateBigin;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn TabPlanREQStatusCustomer;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn TabPlanREQTransactionStatus;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemTabPlanREQTransactionStatus;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn TabPlanREQTransactionConClude;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemTabPlanREQTransactionConClude;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn TestProgramTest;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn TabTestCreateTest;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCreateTest;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn TabTestPullTest;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemPullTest;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn TabTestADDTest;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemADDTest;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn TabTestSaveTest;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemSaveTest;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn TabTestEditTest;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemEditTest;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn TabTestCancelTest;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCancelTest;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn TabTestSearchTest;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemSearchTest;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn TabTestPriviewTest;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemPriviewTest;
        private System.Windows.Forms.ToolStripMenuItem StripMenuPaste;
        private System.Windows.Forms.ToolStripMenuItem StripMenuCopy;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn TabTestSupportTest;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemTabTestSupportTest;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn TabTestStatusTest;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemStatusTest;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn TabTestOpenFileVersion;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemTabTestOpenFileVersion;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn TabTestWebUpdate;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemTabTestWebUpdate;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn TabTestUpdate;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemTestUpdate;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn TabApprovedTest;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemApprovedTest;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn TabTestSupervisorName;
        private DevExpress.XtraBars.BarButtonItem barMail;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarSubItem barSubItem2;
        private DevExpress.XtraBars.BarButtonItem barConfigPlan;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.SimpleButton btnSearch;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand3;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn TabTestResponsibleMan;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit repositoryItemEmployee_TabTest;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn Col_AssigerCode;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit repositoryItemEmployee;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn Col_ResponsibleMan;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn TabPlanCol_AssigerCode;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit repositoryItemEmployee_Plan;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn TabPlanCol_ResponsibleMan;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn Col_CustomerDueDate;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn Col_Tester;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn Col_TestDueDate;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn Col_TestStatus;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemTestStatus;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBandTask01;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand4;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand5;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.CheckedComboBoxEdit SearchStatus_CheckCombobox;
    }
}

