﻿namespace TasksOfCompany
{
    partial class PopUpConfigPlan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PopUpConfigPlan));
            this.GridConfigPlan = new DevExpress.XtraGrid.GridControl();
            this.contextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.StripNew = new System.Windows.Forms.ToolStripMenuItem();
            this.StripCopy = new System.Windows.Forms.ToolStripMenuItem();
            this.StripPaste = new System.Windows.Forms.ToolStripMenuItem();
            this.StripDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.advBandedGridCompanyConfigPlan = new DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView();
            this.gridBand1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.ConfigPlan_RowNumber = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.ConfigPlan_DocumentTypeRequiment = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemConfigPlan_DocumentTypeRequiment = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.ConfigPlan_TransactionDescription = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemConfigPlan_TransactionDescription = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.barEdit = new DevExpress.XtraBars.BarButtonItem();
            this.barSave = new DevExpress.XtraBars.BarButtonItem();
            this.barUndo = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.imageCollectionControl = new DevExpress.Utils.ImageCollection(this.components);
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            ((System.ComponentModel.ISupportInitialize)(this.GridConfigPlan)).BeginInit();
            this.contextMenuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.advBandedGridCompanyConfigPlan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemConfigPlan_DocumentTypeRequiment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemConfigPlan_TransactionDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollectionControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.SuspendLayout();
            // 
            // GridConfigPlan
            // 
            this.GridConfigPlan.ContextMenuStrip = this.contextMenuStrip;
            this.GridConfigPlan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridConfigPlan.Location = new System.Drawing.Point(0, 35);
            this.GridConfigPlan.MainView = this.advBandedGridCompanyConfigPlan;
            this.GridConfigPlan.Name = "GridConfigPlan";
            this.GridConfigPlan.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemConfigPlan_TransactionDescription,
            this.repositoryItemConfigPlan_DocumentTypeRequiment});
            this.GridConfigPlan.Size = new System.Drawing.Size(554, 261);
            this.GridConfigPlan.TabIndex = 0;
            this.GridConfigPlan.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.advBandedGridCompanyConfigPlan});
            // 
            // contextMenuStrip
            // 
            this.contextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.StripNew,
            this.StripCopy,
            this.StripPaste,
            this.StripDelete});
            this.contextMenuStrip.Name = "contextMenuStrip";
            this.contextMenuStrip.Size = new System.Drawing.Size(108, 92);
            // 
            // StripNew
            // 
            this.StripNew.Name = "StripNew";
            this.StripNew.Size = new System.Drawing.Size(107, 22);
            this.StripNew.Text = "สร้าง";
            // 
            // StripCopy
            // 
            this.StripCopy.Name = "StripCopy";
            this.StripCopy.Size = new System.Drawing.Size(107, 22);
            this.StripCopy.Text = "คัดลอก";
            // 
            // StripPaste
            // 
            this.StripPaste.Name = "StripPaste";
            this.StripPaste.Size = new System.Drawing.Size(107, 22);
            this.StripPaste.Text = "วาง";
            // 
            // StripDelete
            // 
            this.StripDelete.Name = "StripDelete";
            this.StripDelete.Size = new System.Drawing.Size(107, 22);
            this.StripDelete.Text = "ลบ";
            // 
            // advBandedGridCompanyConfigPlan
            // 
            this.advBandedGridCompanyConfigPlan.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand1});
            this.advBandedGridCompanyConfigPlan.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.ConfigPlan_RowNumber,
            this.ConfigPlan_DocumentTypeRequiment,
            this.ConfigPlan_TransactionDescription});
            this.advBandedGridCompanyConfigPlan.GridControl = this.GridConfigPlan;
            this.advBandedGridCompanyConfigPlan.Name = "advBandedGridCompanyConfigPlan";
            this.advBandedGridCompanyConfigPlan.OptionsView.ColumnAutoWidth = true;
            this.advBandedGridCompanyConfigPlan.OptionsView.ShowBands = false;
            this.advBandedGridCompanyConfigPlan.OptionsView.ShowGroupPanel = false;
            // 
            // gridBand1
            // 
            this.gridBand1.Caption = "gridBand1";
            this.gridBand1.Columns.Add(this.ConfigPlan_RowNumber);
            this.gridBand1.Columns.Add(this.ConfigPlan_DocumentTypeRequiment);
            this.gridBand1.Columns.Add(this.ConfigPlan_TransactionDescription);
            this.gridBand1.Name = "gridBand1";
            this.gridBand1.VisibleIndex = 0;
            this.gridBand1.Width = 536;
            // 
            // ConfigPlan_RowNumber
            // 
            this.ConfigPlan_RowNumber.AppearanceHeader.Options.UseTextOptions = true;
            this.ConfigPlan_RowNumber.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ConfigPlan_RowNumber.Caption = "ลำดับ";
            this.ConfigPlan_RowNumber.FieldName = "RowNumber";
            this.ConfigPlan_RowNumber.Name = "ConfigPlan_RowNumber";
            this.ConfigPlan_RowNumber.OptionsColumn.AllowMove = false;
            this.ConfigPlan_RowNumber.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.ConfigPlan_RowNumber.OptionsFilter.AllowAutoFilter = false;
            this.ConfigPlan_RowNumber.OptionsFilter.AllowFilter = false;
            this.ConfigPlan_RowNumber.Visible = true;
            this.ConfigPlan_RowNumber.Width = 48;
            // 
            // ConfigPlan_DocumentTypeRequiment
            // 
            this.ConfigPlan_DocumentTypeRequiment.AppearanceHeader.Options.UseTextOptions = true;
            this.ConfigPlan_DocumentTypeRequiment.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ConfigPlan_DocumentTypeRequiment.Caption = "ประเภท Requiment";
            this.ConfigPlan_DocumentTypeRequiment.ColumnEdit = this.repositoryItemConfigPlan_DocumentTypeRequiment;
            this.ConfigPlan_DocumentTypeRequiment.FieldName = "DocumentTypeRequiment";
            this.ConfigPlan_DocumentTypeRequiment.Name = "ConfigPlan_DocumentTypeRequiment";
            this.ConfigPlan_DocumentTypeRequiment.OptionsColumn.AllowMove = false;
            this.ConfigPlan_DocumentTypeRequiment.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.ConfigPlan_DocumentTypeRequiment.OptionsFilter.AllowAutoFilter = false;
            this.ConfigPlan_DocumentTypeRequiment.OptionsFilter.AllowFilter = false;
            this.ConfigPlan_DocumentTypeRequiment.Visible = true;
            this.ConfigPlan_DocumentTypeRequiment.Width = 113;
            // 
            // repositoryItemConfigPlan_DocumentTypeRequiment
            // 
            this.repositoryItemConfigPlan_DocumentTypeRequiment.AutoHeight = false;
            this.repositoryItemConfigPlan_DocumentTypeRequiment.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemConfigPlan_DocumentTypeRequiment.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Desc", "Desc")});
            this.repositoryItemConfigPlan_DocumentTypeRequiment.Name = "repositoryItemConfigPlan_DocumentTypeRequiment";
            this.repositoryItemConfigPlan_DocumentTypeRequiment.NullText = "";
            // 
            // ConfigPlan_TransactionDescription
            // 
            this.ConfigPlan_TransactionDescription.AppearanceHeader.Options.UseTextOptions = true;
            this.ConfigPlan_TransactionDescription.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ConfigPlan_TransactionDescription.Caption = "รายละเอียด";
            this.ConfigPlan_TransactionDescription.ColumnEdit = this.repositoryItemConfigPlan_TransactionDescription;
            this.ConfigPlan_TransactionDescription.FieldName = "TransactionDescription";
            this.ConfigPlan_TransactionDescription.Name = "ConfigPlan_TransactionDescription";
            this.ConfigPlan_TransactionDescription.OptionsColumn.AllowMove = false;
            this.ConfigPlan_TransactionDescription.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.ConfigPlan_TransactionDescription.OptionsFilter.AllowAutoFilter = false;
            this.ConfigPlan_TransactionDescription.OptionsFilter.AllowFilter = false;
            this.ConfigPlan_TransactionDescription.Visible = true;
            this.ConfigPlan_TransactionDescription.Width = 375;
            // 
            // repositoryItemConfigPlan_TransactionDescription
            // 
            this.repositoryItemConfigPlan_TransactionDescription.AutoHeight = false;
            this.repositoryItemConfigPlan_TransactionDescription.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemConfigPlan_TransactionDescription.Name = "repositoryItemConfigPlan_TransactionDescription";
            this.repositoryItemConfigPlan_TransactionDescription.ShowIcon = false;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Images = this.imageCollectionControl;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barEdit,
            this.barSave,
            this.barUndo});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 3;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barEdit),
            new DevExpress.XtraBars.LinkPersistInfo(this.barSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.barUndo)});
            this.bar2.OptionsBar.AllowQuickCustomization = false;
            this.bar2.OptionsBar.DrawDragBorder = false;
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // barEdit
            // 
            this.barEdit.Caption = "Edit";
            this.barEdit.Id = 0;
            this.barEdit.ImageOptions.ImageIndex = 0;
            this.barEdit.Name = "barEdit";
            // 
            // barSave
            // 
            this.barSave.Caption = "Save";
            this.barSave.Id = 1;
            this.barSave.ImageOptions.ImageIndex = 1;
            this.barSave.Name = "barSave";
            // 
            // barUndo
            // 
            this.barUndo.Caption = "Undo";
            this.barUndo.Id = 2;
            this.barUndo.ImageOptions.ImageIndex = 2;
            this.barUndo.Name = "barUndo";
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(554, 35);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 296);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(554, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 35);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 261);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(554, 35);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 261);
            // 
            // imageCollectionControl
            // 
            this.imageCollectionControl.ImageSize = new System.Drawing.Size(25, 25);
            this.imageCollectionControl.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollectionControl.ImageStream")));
            // 
            // panelControl1
            // 
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 35);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(554, 261);
            this.panelControl1.TabIndex = 5;
            // 
            // PopUpConfigPlan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(554, 296);
            this.Controls.Add(this.GridConfigPlan);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "PopUpConfigPlan";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "แผนรายการบริษัท";
            this.Load += new System.EventHandler(this.PopUpConfigPlan_Load);
            ((System.ComponentModel.ISupportInitialize)(this.GridConfigPlan)).EndInit();
            this.contextMenuStrip.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.advBandedGridCompanyConfigPlan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemConfigPlan_DocumentTypeRequiment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemConfigPlan_TransactionDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollectionControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.GridControl GridConfigPlan;
        private DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView advBandedGridCompanyConfigPlan;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand1;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarButtonItem barEdit;
        private DevExpress.XtraBars.BarButtonItem barSave;
        private DevExpress.XtraBars.BarButtonItem barUndo;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn ConfigPlan_RowNumber;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn ConfigPlan_TransactionDescription;
        private DevExpress.Utils.ImageCollection imageCollectionControl;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem StripNew;
        private System.Windows.Forms.ToolStripMenuItem StripCopy;
        private System.Windows.Forms.ToolStripMenuItem StripPaste;
        private System.Windows.Forms.ToolStripMenuItem StripDelete;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemConfigPlan_TransactionDescription;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn ConfigPlan_DocumentTypeRequiment;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemConfigPlan_DocumentTypeRequiment;
    }
}