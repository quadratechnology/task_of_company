﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TasksOfCompany
{
    public class FILED_CustomerMaster
    {
        public const string RecStatus = "RecStatus";
        public const string Code = "Code";

        public const string PrefixThai = "PrefixThai";
        public const string PrefixEng = "PrefixEng";
        public const string NameThai = "NameThai";
        public const string NameEng = "NameEng";
        public const string SuffixThai = "SuffixThai";
        public const string SuffixEng = "SuffixEng";
        public const string AddressThai = "AddressThai";
        public const string AddressEng = "AddressEng";
        public const string Country = "Country";
        public const string ProvinceState = "ProvinceState";
        public const string KhetAmporCity = "KhetAmporCity";
        public const string KwaengTambon = "KwaengTambon";
        public const string PostalCode = "PostalCode";

        public const string NoCustomerTaxInvoice = "NoCustomerTaxInvoice";
        public const string LastUpdatedNoSalesInvoice = "LastUpdatedNoSalesInvoice";
        public const string PrintLanguages = "PrintLanguages";
        public const string CurrencyCode = "CurrencyCode";

        public const string PaymentTermCode = "PaymentTermCode";
        public const string ReceiptFromCode = "ReceiptFromCode";
        public const string NoSaleOrder = "NoSaleOrder";
        public const string Phone1 = "Phone1";
        public const string Phone2 = "Phone2";
        public const string Fax1 = "Fax1";
        public const string Fax2 = "Fax2";
        public const string Email = "Email";
        public const string WebSite = "WebSite";
        public const string Category1 = "Category1";
        public const string Category2 = "Category2";
        public const string Category3 = "Category3";
        public const string Category4 = "Category4";
        public const string Category5 = "Category5";
        public const string Category6 = "Category6";
        public const string SalesmanCode = "SalesmanCode";
        public const string SalesmanName = "SalesmanName";
        public const string AccountCode = "AccountCode";
        public const string TaxID = "TaxID";
        public const string HeadQuaterOrBranch = "HeadQuaterOrBranch";
        public const string HeadQuaterOrBranchCode = "HeadQuaterOrBranchCode";
        public const string HeadQuaterOrBranchNameT = "HeadQuaterOrBranchNameT";
        public const string HeadQuaterOrBranchNameE = "HeadQuaterOrBranchNameE";
        public const string InVATExVATTrueFalse = "InVATExVATTrueFalse";
        public const string Welfare = "Welfare";
        public const string NickName = "NickName";
        public const string Remark = "Remark";
        public const string RemarkOption = "RemarkOption";
        public const string ShippedBy = "ShippedBy";
        public const string ShippedDetails = "ShippedDetails";
        public const string ForwarderAgent = "ForworderAgent";
        public const string ForwarderContact = "ForworderContact";
        public const string DocumentAgent = "DocumentAgent";
        public const string DocumentContact = "DocumentContact";
        public const string Freight = "Freight";
        public const string ShippedFrom = "ShippedFrom";
        public const string ShippedTo = "ShippedTo";
        public const string ShippingPacking = "ShippingPacking";
        public const string ShippingMarks = "ShippingMark";
        public const string ShippingDetails = "ShippingDetails";
        public const string PortOfLoading = "PortOfLoading";
        public const string CountryOfOrigin = "CountryOfOrigin";
        public const string Vessel = "Vessel";
        public const string Incoterm = "Incoterm";
        public const string IncotermDescription = "IncotermDescription";
        public const string RequiredDocument = "RequiredDocument";
    }
}
