﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using DBInfo;
using ModuleUtil;

namespace TasksOfCompany
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string [] args)
        {
            TBRequiredCheck[] TableCheckExisted = new TBRequiredCheck[]
			{

            };
            args[0] = args[0].Replace("&lt;", "<");
            string LoginInfo = ModuleLoad.LoadMainOK(args, TableCheckExisted, "Task Of Company");



            if (LoginInfo != string.Empty)
            {
                FormTasksOfCompany TasksOfCompanyObj = new FormTasksOfCompany();
                TasksOfCompanyObj.LoginInfo = LoginInfo;
                TasksOfCompanyObj.PopMessageLoadingObj = ModuleLoad.PopMessageLoadingObj;
                Application.Run(TasksOfCompanyObj);
            }
            else
            {
                Application.Exit();
            }
        }
    }
}
