﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TasksOfCompany
{
    public class TaskMessageGet
    {
        // ชื่อผู้รับผิดชอบงาน
        public string SupervisorName { get; set; }
        // รายการ Msg
        public List<DescriptionMsg> ListTaskDescritpion { get; set; }

        public TaskMessageGet()
        {
            ListTaskDescritpion = new List<DescriptionMsg>();
        }

        public string CreateMessageLine()
        {
            string Msg = SupervisorName;
            foreach (var ListMsg in ListTaskDescritpion)
            {
                if (Msg != string.Empty)
                {
                    Msg += Environment.NewLine;
                }
                Msg += " -[" + ListMsg.CompanyName + "]" + ListMsg.Description.Replace("\n", "");
            }

            return Msg ;
        }
    }
    public class DescriptionMsg
    {
        public string CompanyName { get; set; }
        public string Description { get; set; }
        public DescriptionMsg()
        {
 
        }
    }

}
