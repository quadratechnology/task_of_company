﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TasksOfCompany
{
    public class FIELD_CompanyConfigPlan
    {
        public const string NoLine = "NoLine";
        public const string RowNumber = "RowNumber";
        public const string TransactionDescription = "TransactionDescription";
        public const string RecStatus = "RecStatus";
        public const string LastUpdated = "LastUpdated";
        public const string UpdatedUser = "UpdatedUser";
        public const string StatusRow = "StatusRow";
        public const string DocumentTypeRequiment = "DocumentTypeRequiment";
    }
}
