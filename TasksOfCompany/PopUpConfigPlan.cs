﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using QERPUtill;
using DBUtil;
using ModuleUtil;
using System.Globalization;
using ControlCenter;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.BandedGrid;
using DevExpress.XtraGrid;
using DevExpress.XtraEditors.Repository;

namespace TasksOfCompany
{
    public partial class PopUpConfigPlan : Form
    {
        private enum Mode
        {
            View,Edit
        }
        private Mode ModeSelect;

        private QEB.Center CenterInfo;
        private DataSet QEBds;
        private DBSimple SimpleOjb;
        private DBTransaction TransactionOjb;

        private DataTable TBCompanyConfigPlan;
        private DataTable TBCompanyConfigPlanCopy;
        private DataTable TBCompanyConfigPlanSave;
        
        public PopUpConfigPlan(QEB.Center Center, DataSet QEBds,DBSimple Simple,DBTransaction Transaction)
        {
            InitializeComponent();
            this.CenterInfo = Center;
            this.QEBds = QEBds;
            this.SimpleOjb = Simple;
            this.TransactionOjb = Transaction;
        }

        private void PopUpConfigPlan_Load(object sender, EventArgs e)
        {
            this.InitTable();
            this.InitControl();
            this.LoadData();
            this.InitEvent();
            this.EnableDisableControl(ModeSelect);
        }

        private void InitTable()
        {
            this.TBCompanyConfigPlan = this.QEBds.Tables[TableName.CompanyConfigPlan];
            if(!TBCompanyConfigPlan.Columns.Contains(FIELD_CompanyConfigPlan.StatusRow))
                this.TBCompanyConfigPlan.Columns.Add(FIELD_CompanyConfigPlan.StatusRow,typeof(String));
            this.TBCompanyConfigPlanSave = TBCompanyConfigPlan.Clone();
            this.TBCompanyConfigPlanCopy = TBCompanyConfigPlan.Clone();
        }

        private void LoadData()
        {
            int TransErr = 0;
            string ErrMsg = string.Empty;
            int ErrCode = 0;

            string Query = @"SELECT * FROM CompanyConfigPlan";
            TransErr += this.SimpleOjb.FillData(this.TBCompanyConfigPlan,Query,ref ErrCode,ref ErrMsg);
            if (TransErr != 0)
            {
                MessageBox.Show(ErrMsg);
            }
        }
        private void InitControl()
        {
            this.GridConfigPlan.DataSource = this.TBCompanyConfigPlan;
            this.advBandedGridCompanyConfigPlan.RowCellStyle += AdvBandedGridCompanyConfigPlan_RowCellStyle;

            string[,] ArrayDocumentTypeRequiment = new string[,]
            {
              {VALUE_Status.DocumentTypeRequiment_Form,"Form"},
              {VALUE_Status.DocumentTypeRequiment_Report,"Report"},
              {VALUE_Status.DocumentTypeRequiment_Program,"Program"},
              {VALUE_Status.DocumentTypeRequiment_Config,"Config/File,FormatNo"},
              {VALUE_Status.DocumentTypeRequiment_Template,"Template"},
              {VALUE_Status.DocumentTypeRequiment_Trainning,"Tranning"},
              {VALUE_Status.DocumentTypeRequiment_OpenHouse,"OpenHouse"},
              {VALUE_Status.DocumentTypeRequiment_Setup,"Setup"}
            };
            DataView ViewDocumentTypeRequiment = this.GetDataView(ArrayDocumentTypeRequiment);
            this.SetupColLookUpEdit(ViewDocumentTypeRequiment, "Code", "Desc", this.repositoryItemConfigPlan_DocumentTypeRequiment);
        }

        private void AdvBandedGridCompanyConfigPlan_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            DataRow RowFocus = this.advBandedGridCompanyConfigPlan.GetDataRow(e.RowHandle);
            if (RowFocus != null)
            {
                if (ReceiveValue.StringReceive(FIELD_CompanyConfigPlan.StatusRow, RowFocus) == VALUE_Status.DELETE)
                {
                    e.Appearance.ForeColor = Color.Red;
                }
                else
                {
                    e.Appearance.ForeColor = Color.Black;
                }
            }
        }

        private void InitEvent()
        {
            this.barEdit.ItemClick += BarEdit_ItemClick;
            this.barUndo.ItemClick += BarUndo_ItemClick;
            this.barSave.ItemClick += BarSave_ItemClick;

            this.StripNew.Click += StripNew_Click;
            this.StripCopy.Click += StripCopy_Click;
            this.StripPaste.Click += StripPaste_Click;
            this.StripDelete.Click += StripDelete_Click;

            this.advBandedGridCompanyConfigPlan.ShowingEditor += AdvBandedGridCompanyConfigPlan_ShowingEditor;
            this.TBCompanyConfigPlan.ColumnChanged += TBCompanyConfigPlan_ColumnChanged;
            this.contextMenuStrip.Opening += ContextMenuStrip_Opening;
        }
        public DataView GetDataView(string[,] Str)
        {
            DataTable TB = new DataTable();
            TB.Columns.Add("Code", System.Type.GetType("System.String"));
            TB.Columns.Add("Desc", System.Type.GetType("System.String"));
            TB.Columns.Add("NoLine", System.Type.GetType("System.Int32"));
            for (int i = 0; i < Str.GetLength(0); i++)
            {
                DataRow dr = TB.NewRow();
                dr["NoLine"] = i + 1;
                dr["Code"] = Str[i, 0];
                dr["Desc"] = Str[i, 1];
                TB.Rows.Add(dr);
            }
            return new DataView(TB, "", "NoLine ASC", DataViewRowState.CurrentRows);
        }
        private void SetupColLookUpEdit(DataView view, string Code, string Desc, RepositoryItemLookUpEdit ctr)
        {
            ctr.DataSource = view;
            ctr.ValueMember = Code;
            ctr.DisplayMember = Desc;
            ctr.NullText = string.Empty;
            ctr.ShowHeader = false;
            ctr.ShowFooter = false;
            ctr.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFit;
            ctr.PopupFormMinSize = new System.Drawing.Size(10, 10);
            ctr.DropDownRows = view.Count > 10 ? 10 : view.Count;
        }
        private void SetupLookupEdit(DataView view, string Code, string Desc, DevExpress.XtraEditors.LookUpEdit ctr)
        {
            ctr.Properties.DataSource = view;
            ctr.Properties.ValueMember = Code;
            ctr.Properties.DisplayMember = Desc;
            ctr.Properties.NullText = string.Empty;
            ctr.Properties.ShowHeader = false;
            ctr.Properties.ShowFooter = false;
            ctr.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFit;
            ctr.Properties.PopupFormMinSize = new System.Drawing.Size(10, 10);
            ctr.Properties.DropDownRows = view.Count > 10 ? 10 : view.Count;
        }
        private void TBCompanyConfigPlan_ColumnChanged(object sender, DataColumnChangeEventArgs e)
        {
            if (ModeSelect == Mode.View)
                return;

            string StatusRow = ReceiveValue.StringReceive(FIELD_CompanyConfigPlan.StatusRow, e.Row);
            if (StatusRow == string.Empty)
            {
                e.Row.BeginEdit();
                e.Row[FIELD_CompanyConfigPlan.StatusRow] = VALUE_Status.EDIT;
                e.Row.EndEdit();
            }
        }

        private void StripNew_Click(object sender, EventArgs e)
        {
            int CountRow = 0;
            #region GetCountRow
            if (this.TBCompanyConfigPlan.Rows.Count > 0)
            {
                CountRow = this.TBCompanyConfigPlan.AsEnumerable().Max(r => r.Field<int>(FIELD_CompanyConfigPlan.RowNumber));
            }
            #endregion

            DataRow NewRow = this.TBCompanyConfigPlan.NewRow();
            NewRow[FIELD_CompanyConfigPlan.RowNumber] = CountRow+1;
            NewRow[FIELD_CompanyConfigPlan.StatusRow] = VALUE_Status.NEW;
            NewRow[FIELD_CompanyConfigPlan.RecStatus] = 0;
            NewRow[FIELD_CompanyConfigPlan.LastUpdated] = DateTime.Now.Date;
            NewRow[FIELD_CompanyConfigPlan.UpdatedUser] = this.CenterInfo.Username; 
            this.TBCompanyConfigPlan.Rows.Add(NewRow);
        }
        private void StripDelete_Click(object sender, EventArgs e)
        {
            DataRow RowFocus = this.advBandedGridCompanyConfigPlan.GetFocusedDataRow();
            if (RowFocus != null)
            {
                string StatusRow = ReceiveValue.StringReceive(FIELD_CompanyConfigPlan.StatusRow, RowFocus);
                if (StatusRow == VALUE_Status.EDIT || StatusRow == string.Empty)
                {
                    RowFocus.BeginEdit();
                    RowFocus[FIELD_CompanyConfigPlan.StatusRow] = VALUE_Status.DELETE;
                    RowFocus.EndEdit();
                }
                else if (StatusRow == VALUE_Status.NEW)
                {
                    this.TBCompanyConfigPlan.Rows.Remove(RowFocus);
                }
            }
        }
        private void StripPaste_Click(object sender, EventArgs e)
        {
            if (this.TBCompanyConfigPlanCopy.Rows.Count > 0)
            {
                DataRow RowCopy = TBCompanyConfigPlanCopy.Rows[0];
                int CountRow = 0;
                #region GetCountRow
                if (this.TBCompanyConfigPlan.Rows.Count > 0)
                {
                    CountRow = this.TBCompanyConfigPlan.AsEnumerable().Max(r => r.Field<int>(FIELD_CompanyConfigPlan.RowNumber));
                }
                #endregion

                DataRow NewRow = this.TBCompanyConfigPlan.NewRow();
                NewRow[FIELD_CompanyConfigPlan.RowNumber] = CountRow + 1;
                NewRow[FIELD_CompanyConfigPlan.TransactionDescription] = RowCopy[FIELD_CompanyConfigPlan.TransactionDescription];
                NewRow[FIELD_CompanyConfigPlan.StatusRow] = VALUE_Status.NEW;
                NewRow[FIELD_CompanyConfigPlan.RecStatus] = 0;
                NewRow[FIELD_CompanyConfigPlan.LastUpdated] = DateTime.Now.Date;
                NewRow[FIELD_CompanyConfigPlan.UpdatedUser] = this.CenterInfo.Username;
                this.TBCompanyConfigPlan.Rows.Add(NewRow);
            }
        }
        private void StripCopy_Click(object sender, EventArgs e)
        {
            DataRow RowFocus = this.advBandedGridCompanyConfigPlan.GetFocusedDataRow();
            if (RowFocus != null)
            {
                this.TBCompanyConfigPlanCopy.Clear();
                this.TBCompanyConfigPlanCopy.ImportRow(RowFocus);
            }
        }
        private void BarSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.panelControl1.Focus();
            DialogResult YNSave = MessageBox.Show("คุณต้องการบันทึกรายการหรือไม่ ?", "บันทึก", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            if (YNSave == DialogResult.Yes)
            {
                int TransErr = 0;
                string ErrMsg = string.Empty;
                string FormatSearch = String.Format("ISNULL({0},'') <> ''", FIELD_CompanyConfigPlan.StatusRow);
                foreach (DataRow RowSet in this.TBCompanyConfigPlan.Rows)
                {
                    string StatusRow = ReceiveValue.StringReceive(FIELD_CompanyConfigPlan.StatusRow, RowSet);
                    if (StatusRow == VALUE_Status.EDIT)
                    {
                        RowSet.BeginEdit();
                        RowSet[FIELD_CompanyConfigPlan.UpdatedUser] = this.CenterInfo.Username;
                        RowSet[FIELD_CompanyConfigPlan.LastUpdated] = DateTime.Now.Date;
                        RowSet.EndEdit();
                    }
                    else if (StatusRow == VALUE_Status.NEW)
                    {
                        RowSet.AcceptChanges();
                        RowSet.SetAdded();
                    }
                }
                foreach (DataRow RowSave in this.TBCompanyConfigPlan.Select(FormatSearch))
                {
                    int TransSave = 0;
                    int ErrCodeS = 0;
                    string ErrMsgS = string.Empty;

                    string StatusRow = ReceiveValue.StringReceive(FIELD_CompanyConfigPlan.StatusRow, RowSave);
                    if (StatusRow == VALUE_Status.DELETE)
                    {
                        RowSave.Delete();
                    }
                    this.TBCompanyConfigPlanSave.ImportRow(RowSave);
                    TransSave += this.TransactionOjb.StartTransaction(ref ErrMsgS);
                    if (TransSave == 0)
                        TransSave += this.TransactionOjb.OleDBUpdate(TBCompanyConfigPlanSave, ref ErrCodeS, ref ErrMsgS);
                    if (TransSave == 0)
                    {
                        this.TransactionOjb.TransCommit();
                    }
                    else
                    {
                        this.TransactionOjb.TransRollBack();
                        ErrMsg += String.Format(@"{1} รายการ {0} มีคนบันทึกไปแล้ว"
                                    , ReceiveValue.StringReceive(FIELD_CompanyConfigPlan.TransactionDescription, RowSave), ErrMsgS) + "\n";
                        TransErr++;
                    }
                }
                if (TransErr != 0)
                {
                    MessageBox.Show(ErrMsg);
                }
                ModeSelect = Mode.View;
                EnableDisableControl(ModeSelect);
            }
        }
        private void BarUndo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DialogResult YNUndo = MessageBox.Show("คุณต้องการเลิกทำหรือไม่ ?","เลิกทำ",MessageBoxButtons.YesNo,MessageBoxIcon.Information);
            if(YNUndo == DialogResult.Yes)
            {
                this.LoadData();
                ModeSelect = Mode.View;
                EnableDisableControl(ModeSelect);
            }
        }
        private void BarEdit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.LoadData();
            ModeSelect = Mode.Edit;
            EnableDisableControl(ModeSelect);
        }
        private void ContextMenuStrip_Opening(object sender, CancelEventArgs e)
        {
            if (ModeSelect == Mode.View)
            {
                e.Cancel = true;
            }
            else
            {
                DataRow RowFocus = this.advBandedGridCompanyConfigPlan.GetFocusedDataRow();

                this.StripNew.Enabled = true;
                this.StripCopy.Enabled = true;
                this.StripPaste.Enabled = true;
                this.StripDelete.Enabled = true;
                string StatusRow = string.Empty;

                if (RowFocus != null)
                    StatusRow = ReceiveValue.StringReceive(FIELD_CompanyConfigPlan.StatusRow, RowFocus);

                if (this.TBCompanyConfigPlanCopy.Rows.Count == 0)
                    this.StripPaste.Enabled = false;
                if (RowFocus == null || StatusRow == VALUE_Status.DELETE)
                {
                    this.StripCopy.Enabled = false;
                    this.StripDelete.Enabled = false;
                }
            }
        }
        private void AdvBandedGridCompanyConfigPlan_ShowingEditor(object sender, CancelEventArgs e)
        {
            if (ModeSelect == Mode.View)
            {
                e.Cancel = true;
            }
            else
            {
                e.Cancel = false;
            }
        }
        private void EnableDisableControl(Mode CheckMode)
        {
            if (CheckMode == Mode.View)
            {
                this.barEdit.Enabled = true;
                this.barSave.Enabled = false;
                this.barUndo.Enabled = false;
            }
            else
            {
                this.barEdit.Enabled = false;
                this.barSave.Enabled = true;
                this.barUndo.Enabled = true;
            }
        }
    }
}
