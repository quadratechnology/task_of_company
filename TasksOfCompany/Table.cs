﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TasksOfCompany
{
    public class TableName
    {
        public const string CustomerMaster = "CustomerMaster";
        public const string TasksOfCompany = "TaskOfCompany";
        public const string EmployeeMaster = "EmployeeMaster";
        public const string CompanyConfigPlan = "CompanyConfigPlan";
    }
}
